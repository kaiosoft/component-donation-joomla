<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */


defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

require_once('components/com_invest/helpers/navigation.php');

$pathXML = "components/com_invest/models/forms/bank.xml";
$form =& JForm::getInstance('bank', $pathXML);

$form->setFieldAttribute("bank","default",$this->data['bank'][0]->bank,"main");
$form->setFieldAttribute("rekening","default",$this->data['bank'][0]->rekening,"main");
$form->setFieldAttribute("an","default",$this->data['bank'][0]->an,"main");

?>
<script type="text/javascript">
Joomla.submitbutton = function(task)
{
	if(task == 'bank.save')
	{
       if((document.getElementById('main_bank').value == "") || (document.getElementById('main_an').value == "") || (document.getElementById('main_rekening').value == ""))
		{
			alert('Semua fields bertanda (*) bintang harus terisi');
		}
		else
		{
			document.getElementById('task').value="bank.save";
			document.adminForm.submit();
		}
	}
	else
	{
		window.open('index.php?option=com_invest&c=memberbank','_parent');
	}
}
</script>
<script language="javascript" src="components/com_invest/assets/js/ajaxscript.js"></script>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
<!-- normal fieldsets -->
	<div class="width-60 fltlft">
		<?php
		// Iterate through the normal form fieldsets and display each one.
		foreach ($form->getFieldsets('main') as $fieldsets => $fieldset):
		?>
		<fieldset class="adminform">
			<legend>
				<?php echo JText::_($fieldset->name); ?>
			</legend>
			<!-- Fields go here -->
				<dl>
				<?php
				// Iterate through the fields and display them.
				foreach($form->getFieldset($fieldset->name) as $field):
					// If the field is hidden, only use the input.
					if ($field->hidden):
						echo $field->input;
					else:
					?>
					<dt>
						<?php echo $field->label; ?>
					</dt>
					<dd>
						<?php if($field->name == 'main[bank_id]') { echo $lists['bank']; } else if($field->name == 'main[member_id]') { echo $lists['member']; } else { echo $field->input; } ?>
					</dd>
					<?php
					endif;
				endforeach;
				?>
				</dl>
		</fieldset>
		<?php
		endforeach;
		?>
	</div>
	</div>  
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="bank" />
<input type="hidden" id="id" name="id" value="<?php echo $this->data['bank'][0]->id; ?>" />
<input type="hidden" id="task" name="task" value="" />
  <?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>