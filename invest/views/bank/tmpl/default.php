<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
require_once('components/com_invest/helpers/navigation.php');
?>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
	<table width="100%" class="adminlist">
        <thead>
            <tr>
            <th width="2%"><label>
              <input type="checkbox" name="toggle" onclick="checkAll(<?php echo count( $this->data['bank'] ); ?>);" />
            </label></th>
            <th><?php echo JText::_('COM_INVEST_BANK_HEADER_BANK'); ?></th>
			<th><?php echo JText::_('COM_INVEST_BANK_HEADER_REKENING');?></th>
			<th><?php echo JText::_('COM_INVEST_BANK_HEADER_AN'); ?></th>
			</tr>
        </thead>
  
  		
	  	<?php 
	  	for($i=0;$i<count($this->data['bank']);$i++) {
		$row =& $this->data['bank'][$i];
 	  	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$link = JRoute::_('index.php?option=com_invest&c=bank&task=bank.edit&cid[]='.$row->id);
	  	?>
      	<tr class="row<?= $i % 2; ?>">
        <td class="center"><?php echo $checked; ?></td>
        <td><?php echo "<a href=".$link.">".$row->bank."</a>"; ?></td>
		<td><?php echo implode(str_split($row->rekening,4),"-"); ?></td>
		<td><?php echo $row->an; ?></td>
      	</tr>
      	<?php
	  	} 
	  	?>
		<tr><td colspan="15">&nbsp;</td></tr>
		<tr><td colspan="15">(*) <?php echo JText::_('COM_INVEST_BANK_BOTTOM_INFO'); ?></td></tr>
        <tr>
        <td colspan="9">
			<?php echo $this->data['pageNav']->getListFooter(); ?>		</td>
		</tr>
	</table>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="bank" />
<input type="hidden" name="boxchecked" value="" />
<input type="hidden" name="task" value="" />    
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>
