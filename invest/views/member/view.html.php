<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewMember extends JView
{

	function display($tpl = null)
	{
  
  	$app =& JFactory::getApplication();
    $task = JRequest::getVar('task');
	$investHelper = new investHelper;
	
	$id = JRequest::getVar('cid');
	$nama_member = 	JRequest::getVar('namaMember');
	$status = JRequest::getVar('status');
    
	//get model
    $modelMember =& JModel::getInstance('member','InvestModel');
	$modelPlan =& JModel::getInstance('plan','InvestModel');
	$modelBank =& JModel::getInstance('bank','InvestModel');
	
	//set filter
	$paramFilter = array("a.nama","a.status");
	$valueFilter = array("='".$nama_member."'","='".$status."'");
	$filter = $investHelper->setFilter($paramFilter,$valueFilter);
	
	//limit for page
	$limit		= $app->getUserStateFromRequest( 'global.list.limit', 'limit', $app->getCfg('list_limit'), 'int' );
	$limitstart = $app->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
	
	// get the total number of records
	$total = $modelMember->getTotal($filter);
		
	jimport('joomla.html.pagination');
	$pageNav = new JPagination( $total, $limitstart, $limit );
	
	if(($task == "member.add") || ($task == "member.edit"))
	{
    	$member = $modelMember->getMemberById($id[0]);
	}
	else
	{
		$member = $modelMember->getAllMember($filter,$pageNav);
	}
	
	$listPlan = $modelPlan->getListPlan();
	$listBank = $modelBank->getListBank();
	$listMember = $modelMember->getListMember();
	JRequest::setVar('listMember',$listMember);
	
	//preparing for data
	$data = array();
	$data['member'] = $member;
	$data['listPlan'] = $listPlan;
	$data['listBank'] = $listBank;
	$data['pageNav'] = $pageNav;
    
    $this->addToolBar($task);

	$this->assignRef('data',$data);
	parent::display($tpl);
		
	}
	
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar($task) 
	{
		if($task=="member.add" || $task=="member.edit"){
			JRequest::setVar('hidemainmenu', true);
			JToolBarHelper::title(JText::_('COM_INVEST_MEMBER_TITLE_ADD'));
			JToolBarHelper::save('member.save');
			JToolBarHelper::cancel('member.cancel');
		} 
		else {
			JToolBarHelper::title(JText::_('COM_INVEST_MEMBER_TITLE_MANAGER'));
			JToolBarHelper::deleteList('', 'member.delete');
			JToolBarHelper::editList('member.edit');
			JToolBarHelper::addNew('member.add');
		}
	}
	
	
		
}	

?>
