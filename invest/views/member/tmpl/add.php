<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
$helper = new investHelper;
require_once('components/com_invest/helpers/navigation.php');

require_once('components/com_invest/helpers/regional.php');
$regional = new regionalHelper;

$data = array();
$data['country'] = $this->data['member'][0]->negara;
$data['state'] = $this->data['member'][0]->provinsi;
$data['city'] = $this->data['member'][0]->kota;

$regionalKey = $regional->getKeyRegional($data);

$listCountry = $regional->getCountry();
$listState = $regional->getState($regionalKey['country']);
$listCity = $regional->getCity($regionalKey['country'],$regionalKey['state']);

$countrylist[]	= JHTML::_('select.option',  '', JText::_( 'Pilih Negara' ), 'value', 'text' );
$countrylist	= array_merge($countrylist, $listCountry);
$lists['country'] = JHTML::_('select.genericlist', $countrylist, 'country', 'class="inputbox" size="1"', 'value', 'text' , $regionalKey['country']  );

$statelist		= $listState;
$lists['state'] = JHTML::_('select.genericlist', $statelist, 'state', 'class="inputbox" size="1"', 'value', 'text', $regionalKey['state'] );

$citylist		= $listCity;
$lists['city'] 	= JHTML::_('select.genericlist', $citylist, 'city', 'class="inputbox" size="1"', 'value', 'text', $regionalKey['city'] );
?>
<script type="text/javascript">
Joomla.submitbutton = function(task)
{
	if(task == 'member.save')
	{
        if((document.getElementById('main_nama').value == "") || (document.getElementById('main_type').value == "") || 
		|| (document.getElementById('main_hp').value == "") || (document.getElementById('extra_status').value == ""))
		{
			alert('Semua fields bertanda (*) bintang harus terisi');
		}
		else
		{
			document.getElementById('task').value="member.save";
			document.adminForm.submit();
		}
	}
	else
	{
		window.open('index.php?option=com_invest&c=member','_parent');
	}
}
</script>
<script language="javascript" src="components/com_properti/assets/js/ajaxscript.js"></script>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
  
<?php 
$pathToMyXMLFile = "components/com_invest/models/forms/member.xml";
$form =& JForm::getInstance('member', $pathToMyXMLFile);
//setUp default value
$form->setFieldAttribute("nama","default",$this->data['member'][0]->nama,"main");
$form->setFieldAttribute("identitas","default",$this->data['member'][0]->identitas,"main");
$form->setFieldAttribute("no_identitas","default",$this->data['member'][0]->no_identitas,"main");
$form->setFieldAttribute("alamat","default",$this->data['member'][0]->alamat,"main");
$form->setFieldAttribute("kota","default",$this->data['member'][0]->kota,"main");
$form->setFieldAttribute("provinsi","default",$this->data['member'][0]->provinsi,"main");
$form->setFieldAttribute("negara","default",$this->data['member'][0]->negara,"main");
$form->setFieldAttribute("kode_pos","default",$this->data['member'][0]->kode_pos,"main");
$form->setFieldAttribute("hp","default",$this->data['member'][0]->hp,"main");
$form->setFieldAttribute("email","default",$this->data['member'][0]->email,"main");
$form->setFieldAttribute("type","default",$this->data['member'][0]->type,"main");
$form->setFieldAttribute("penanggung_jawab","default",$this->data['member'][0]->penanggung_jawab,"main");
$form->setFieldAttribute("status","default",$this->data['member'][0]->status,"extra");
?>
	<!-- normal fieldsets -->
	<div class="width-60 fltlft">
		<?php
		// Iterate through the normal form fieldsets and display each one.
		foreach($form->getFieldsets('main') as $fieldsets => $fieldset):
		?>
		<fieldset class="adminform">
			<legend>
				<?php echo JText::_($fieldset->name); ?>
			</legend>
			<!-- Fields go here -->
				<dl>
				<?php
				// Iterate through the fields and display them.
				foreach($form->getFieldset($fieldset->name) as $field):
					// If the field is hidden, only use the input.
					if ($field->hidden):
						echo $field->input;
					else:			
					?>
					<dt>
						<?php 
						if((!empty($this->data['member'][0]->id)) && ($field->name == "main[email]"))
						{
						
						}
						else
						{
							echo $field->label; 
						}
						?>
					</dt>
					<dd<?php echo ($field->type == 'Editor' || $field->type == 'Textarea') ? ' style="clear: both; margin: 0;"' : ''?>>
						<?php 
						if((!empty($this->data['member'][0]->id)) && ($field->name == "main[email]"))
						{
						
						}
						else
						{
							if($field->name == "main[negara]")
							{
								echo $lists['country'];
							}
							else if($field->name == "main[provinsi]")
							{
								echo "<div class='showState'>";
								if(count($listState) > 1) { echo $lists['state']; } else { echo "<input type='text' name='state' id='provinsi' placeHolder='Provinsi' value='".$regionalKey['state']."' />"; }
							}
							else if($field->name == "main[kota]")
							{
								echo "<div class='showCity'>";
								if(count($listCity) > 1) { echo $lists['city']; } else { echo "<input type='text' name='city' id='kota' placeHolder='Kota' value='".$regionalKey['city']."' />"; }
							}
							else
							{
								echo $field->input;
							}
						}
						?>
					</dd>
					<?php
					endif;
				endforeach;
				?>
				</dl>
		</fieldset>
		<?php
		endforeach;
		?>
	</div>
	<div class="width-40 fltrt">
    <?php 
	jimport('joomla.html.pane');
	//sliders | tabs
	$pane =& JPane::getInstance('tabs', array('startOffset'=>2, 'allowAllClose'=>true, 'opacityTransition'=>true, 'duration'=>600));
	echo $pane->startPane("extra"); ?>
        <?php
        // Iterate through the extra form fieldsets and display each one.
        foreach ($form->getFieldsets("extra") as $fieldsets => $fieldset):
        ?>
        <?php echo $pane->startPanel(JText::_($fieldset->name), $fieldsets); ?>
        <fieldset class="panelform">
            <dl>
            <?php
            // Iterate through the fields and display them.
            foreach($form->getFieldset($fieldset->name) as $field):
                // If the field is hidden, just display the input.
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <dt>
                    <?php echo $field->label; ?>
                </dt>
                <dd>
                    <?php if($field->name == $field->group."[properti_id]") { echo $lists['properti']; } else { echo $field->input; } ?>
                </dd>
                <?php
                endif;
            endforeach;
            ?>
            </dl>
        </fieldset>
        <?php echo $pane->endPanel(); ?>
        <?php
        endforeach;
        ?>
    <?php echo $pane->endPane(); ?>
	</div>
	</div>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="member" />
<input type="hidden" id="id" name="id" value="<?php echo $this->data['member'][0]->id; ?>" />
<input type="hidden" id="user_id" name="user_id" value="<?php echo $this->data['member'][0]->user_id; ?>" />
<input type="hidden" id="balance" name="balance" value="<?php echo $this->data['member'][0]->balance; ?>" />
<input type="hidden" id="legalitas" name="legalitas" value="<?php echo $this->data['member'][0]->legalitas; ?>" /> 	
<input type="hidden" id="date" name="date" value="<?php echo $this->data['member'][0]->date; ?>" /> 	
<input type="hidden" id="task" name="task" value="" />
<?php 
if(($this->data['member'][0]->id != 0) && ($this->data['member'][0]->status == "InActive"))
{
	echo "<input type='hidden' id='sendEmail' name='sendEmail' value='sending' />";
}
?>
  <?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>
<script>
var jQuery = jQuery.noConflict();
jQuery(document).ready(function()
{
	jQuery('#country').change(function()
	{
		var country = jQuery(this).val();
		jQuery('.showState').append('<img src="<?php echo JURI::Base(); ?>administrator/components/com_invest/assets/images/mini_process.gif" />');
		jQuery.ajax({
		type : "POST",
		url : "<?php JURI::Base().'index.php'; ?>",
		data : {option:"com_invest",c:"member",task:"getRegional",regional:"state",country:country},
		}).done(function(show)
		{
			jQuery(".showState").html(show);
			jQuery('#state').change(function()
			{
				jQuery('.showCity').append('<img src="<?php echo JURI::Base(); ?>administrator/components/com_invest/assets/images/mini_process.gif" />');
				var state = jQuery(this).val();
				jQuery.ajax({
				type : "POST",
				url : "<?php JURI::Base().'index.php'; ?>",
				data : {option:"com_invest",view:"plan",task:"getRegional",regional:"city",country:country,state:state},
				}).done(function(show)
				{
					jQuery(".showCity").html(show);
				});
			});
		});
	});
});
</script>