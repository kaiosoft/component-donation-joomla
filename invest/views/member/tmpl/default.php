<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;

$lists = array();
$statuslist[]	= JHTML::_('select.option',  '', JText::_( 'Filter berdasarkan status' ), 'status', 'value' );
$statuslist[]	= JHTML::_('select.option',  'Active', JText::_( 'Active' ), 'status', 'value' );
$statuslist[]	= JHTML::_('select.option',  'InActive', JText::_( 'InActive' ), 'status', 'value' );
$lists['status']	= JHTML::_('select.genericlist', $statuslist, 'status', 'class="inputbox" size="1"', 'status', 'value',JRequest::getVar('status'));

$helper = new investHelper;
$nama_member = JRequest::getVar('namaMember');

require_once('components/com_invest/helpers/navigation.php');
?>
<link rel="stylesheet" href="components/com_invest/assets/js/autocomplete/autocomplete.css" />
<script src="components/com_invest/assets/js/autocomplete/jquery-1.7.1.min.js"></script>
   	<script src="components/com_invest/assets/js/autocomplete/jquery-ui-1.8.17.custom.min.js"></script>
	<script>
	$(function() {
		var availableTags = [
			<?php echo implode(getData(),','); ?>
		];
		$( "#tags" ).autocomplete({
			source: availableTags
		});
	});
	</script>
    <?php

function getData()
{
	$listUpline = JRequest::getVar('listMember');
	$data = array();
	foreach($listUpline as $member)
	{
		$data[] = '"'.$member->nama.'"'; 
	}
	return array_values($data);
}
?>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
<?php echo "<input id='tags' placeHolder='".JText::_('COM_INVEST_MEMBER_FILTER_MEMBER')."' type='text' name='namaMember' value='".$nama_member."' /> ".$lists['status']."<input type=submit value=cari>"; ?>
	<table width="100%" class="adminlist">
        <thead>
            <tr>
            <th width="2%"><label>
              <input type="checkbox" name="toggle" onclick="checkAll(<?php echo count( $this->data['member'] ); ?>);" />
            </label></th>
            <th><?php echo JText::_('COM_INVEST_MEMBER_HEADER_NAME'); ?></th>
			<th><?php echo JText::_('COM_INVEST_MEMBER_HEADER_EMAIL'); ?></th>
			<th><?php echo JText::_('COM_INVEST_MEMBER_HEADER_HP'); ?></th>
			<th><?php echo JText::_('COM_INVEST_MEMBER_HEADER_JUMLAH_PLAN'); ?></th>
			<th><?php echo JText::_('COM_INVEST_MEMBER_HEADER_JUMLAH_BANK'); ?></th>
			<th><?php echo JText::_('COM_INVEST_MEMBER_HEADER_LEGALITAS'); ?></th>
			<th><?php echo JText::_('...'); ?></th>
            </tr>
        </thead>
  
  		
	  	<?php 
	  	for($i=0;$i<count($this->data['member']);$i++) {
		$row =& $this->data['member'][$i];
 	  	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$link = JRoute::_('index.php?option=com_invest&c=member&task=member.edit&cid[]='.$row->id);
		$memberplan = JRoute::_('index.php?option=com_invest&c=memberplan&member_id='.$row->id);
		$memberbank = JRoute::_('index.php?option=com_invest&c=memberbank&member_id='.$row->id);
	  	?>
      	<tr class="row<?= $i % 2; ?>">
        <td class="center"><?php echo $checked; ?></td>
        <td><?php echo "<a href=".$link.">".$row->nama."</a>"; ?></td>
		<td><?php echo $row->email; ?></td>
		<td><?php echo $row->hp; ?></td>
		<td><?php 
			// get member plan
			$dataPlan = $helper->getDataByParam("member_id","='".$row->id."'","#__invest_member_plan","Array"); 
			$plans = array();
			foreach($dataPlan as $plan)
			{
				$namaPlan = $helper->getDataByParam("id","='".$plan->plan_id."'","#__invest_plan");
				$plans[] = $namaPlan->plan." - ".$plan->nama;
			}
			if(!empty($plans)) { echo "<a href=".$memberplan.">".implode($plans,", ")."</a>"; } else { echo "belum memiliki plan"; }
		?></td>
		<td><?php
			//get Member Bank
			$dataBank = $helper->getDataByParam("member_id","='".$row->id."'","#__invest_member_bank","Array"); 
			$banks = array();
			foreach($dataBank as $bank)
			{
				$namaBank = $helper->getDataByParam("id","='".$bank->bank_id."'","#__invest_bank");
				$banks[] = $namaBank->bank;
			}
			if(!empty($banks)) { echo "<a href=".$memberbank.">".implode($banks,", ")."</a>"; } else { echo "belum memasukkan data bank"; }
		?></td>
		<td>
		<?php if(empty($row->legalitas)) { echo "belum mengupload berkas"; } else { echo "<a href='../".$helper->getConfig('uploadDir').$row->legalitas."' target='_blank'>Lihat File</a>"; } ?>
		</td>
		<td><?php if($row->status == "InActive")
		{
			echo "<img title='Keanggotaan belum aktif / data belum lengkap' src='components/com_invest/assets/images/warning.png' height='16' style='float:left' />";
		}
		else if(empty($row->email))
		{
			echo "<img title='Member belum terdaftar / keanggotaan telah dihapus' src='components/com_invest/assets/images/cross.png' height='16' style='float:left' /><a href='#' onclick='forceDelete(".$row->id.");'>Force Delete</a>";
		}
		else
		{
			echo "<img title='Keanggotaan Aktif' src='components/com_invest/assets/images/ceklist.png' height='16' style='float:left' />";
		}
		?>
		</td>
      	</tr>
      	<?php
	  	} 
	  	?>
		<tr><td colspan="15">&nbsp;</td></tr>
		<tr><td colspan="15">(*) <?php echo JText::_('COM_INVEST_MEMBER_BOTTOM_INFO'); ?></td></tr>
        <tr>
        <td colspan="9">
			<?php echo $this->data['pageNav']->getListFooter(); ?>		</td>
		</tr>
	</table>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="member" />
<input type="hidden" name="boxchecked" value="" />
<input type="hidden" name="task" value="" />    
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>
<script>
	function forceDelete(id)
	{
		var con = confirm('yakin ingin menghapus member ini');
		if(con == true)
		{
			window.open('index.php?option=com_invest&c=member&task=forceDelete&id='+id,'_parent');
		}
		else
		{
			alert('Perintah di batalkan');
		}
	}
</script>