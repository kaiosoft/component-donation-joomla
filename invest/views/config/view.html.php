<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewConfig extends JView
{

	function display($tpl = null)
	{
	
		// load model
		$app =& JFactory::getApplication();
		$modelconfig =& JModel::getInstance('config','InvestModel');
		$cat = JRequest::getVar('cat');
		
		$task = JRequest::getVar('task');
		switch($task)
		{
			case "config.edit":
			$config = $modelconfig->getConfigByCategory($cat); 
			break;
			
			default:
			$config = $modelconfig->getConfigCategory(); 
			break;
		}
		$data['config'] = $config;
		
		$this->assignRef('data',$data);
		$this->addToolbar($task);
		parent::display($tpl);
		
	}
	
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar($task) 
	{
		if($task=="config.add" || $task=="config.edit"){
			JToolBarHelper::title(JText::_('COM_INVEST_CONFIG_TITLE_ADD'));
			JToolBarHelper::save('config.save');
			JToolBarHelper::cancel('config.cancel');
		} else {
			JToolBarHelper::title(JText::_('COM_INVEST_CONFIG_TITLE_MANAGER'));
			//JToolBarHelper::deleteList('', 'order.delete');
			//JToolBarHelper::editList('order.edit');
			//JToolBarHelper::addNew('order.add');
		}
	}	
}	

?>
