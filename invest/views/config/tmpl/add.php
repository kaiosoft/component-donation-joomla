<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
require_once('components/com_invest/helpers/navigation.php');
$investHelper = new investHelper;

$editor =& JFactory::getEditor();
?>
<link href="components/com_invest/assets/tooltip/style.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="components/com_invest/assets/tooltip/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="components/com_invest/assets/tooltip/jquery.betterTooltip.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$('.investTip').betterTooltip({speed: 150, delay: 300});
		});
	</script>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
  <table width="100%" border="0" cellpadding="4" cellspacing="1" class="admintable">
  <?php foreach($this->data['config'] as $config) { ?>
    <tr>
      <td width="20%" align="right" class="key" valign="top"><?php echo $config->config; ?> :</td>
      <td align="left">
	  <?php
	  if(($config->config == "memberMailTemplate") || ($config->config == "adminMailTemplate"))
	  {
			echo $editor->display( 'value['.$config->id.']', $config->value, '400', '400', '20', '20', true, $params );
	  }
	  else
	  {
		  echo $editor->display( 'value['.$config->id.']', $config->value, '400', '400', '20', '20', false, $params );
		  ?>
		  <!-- <textarea name="value[<?php echo $config->id; ?>]" cols="60%" rows="5%" /><?php echo $investHelper->br2nl($config->value); ?></textarea> -->
		  <?php 
	  }
	  ?>
	  <a href="#" title="<?php echo $config->description; ?>" class="investTip">[?]</a></td>
    </tr>
	<tr><td>&nbsp;</td></tr>
	<?php } ?>
  </table>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="config" /> 
<input type="hidden" name="task" value="" />
  <?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>