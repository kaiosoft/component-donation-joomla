<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
require_once('components/com_invest/helpers/navigation.php');
?>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
<p style="clear:both;"></p>
<?php for($i=0; $i < count($this->data['config']); $i++){ 
				$row = &$this->data['config'][$i];
				$link = JRoute::_('index.php?option=com_invest&c=config&task=config.edit&cat='.$row->category);
				?>
<div class="boxCategory"><a href="<?php echo $link; ?>"><?php echo $row->category;?></a></div>
<?php } ?>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="config" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="task" value="" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>