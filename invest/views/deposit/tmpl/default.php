<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;

require_once('components/com_invest/helpers/navigation.php');
/*$lists=array();
$statuslist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_TRANSACTION_UPDATE_STATUS' ), 'status', 'value' );
$statuslist[]	= JHTML::_('select.option',  'Pending', JText::_( 'Pending' ), 'status', 'value' );
$statuslist[]	= JHTML::_('select.option',  'Success', JText::_( 'Success' ), 'status', 'value' );
$statuslist[]	= JHTML::_('select.option',  'Reject', JText::_( 'Reject' ), 'status', 'value' );
$lists['status']	= JHTML::_('select.genericlist', $statuslist, 'status', 'onchange="document.adminForm.submit();" class="inputbox" size="1"', 'status', 'value',JRequest::getVar('status'));
*/

//get var
$nama_member = JRequest::getVar('namaMember');
$mulai = JRequest::getVar('mulai');
$sampai = JRequest::getVar('sampai');

?>
<link rel="stylesheet" href="components/com_invest/assets/js/autocomplete/autocomplete.css" />
<script src="components/com_invest/assets/js/autocomplete/jquery-1.7.1.min.js"></script>
   	<script src="components/com_invest/assets/js/autocomplete/jquery-ui-1.8.17.custom.min.js"></script>
	<script>
	$(function() {
		var availableTags = [
			<?php echo implode(getData(),','); ?>
		];
		$( "#tags" ).autocomplete({
			source: availableTags
		});
	});
	</script>
    <?php

function getData()
{
	$listUpline = JRequest::getVar('listMember');
	$data = array();
	foreach($listUpline as $member)
	{
		$data[] = '"'.$member->nama.'"'; 
	}
	return array_values($data);
}
?>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
<?php echo "<input id='tags' placeHolder='".JText::_('COM_INVEST_MEMBER_FILTER_MEMBER')."' type='text' name='namaMember' value='".$nama_member."' /> Periode : ".JHTML::_('calendar',$_POST['mulai'],'mulai', 'mulai', '%Y-%m-%d')."-".JHTML::_('calendar', $_POST['sampai'],'sampai', 'sampai', '%Y-%m-%d')."<input type=submit value=cari>"; ?>
	<table width="100%" class="adminlist">
        <thead>
            <tr>
            <th width="2%" rowspan="2"><label>
              <input type="checkbox" name="toggle" onclick="checkAll(<?php echo count( $this->data['deposit'] ); ?>);" />
            </label></th>
            <th colspan="2"><?php echo JText::_('COM_INVEST_TRANSACTION_HEADER_MEMBER'); ?></th>
			<th rowspan="2"><?php echo JText::_('COM_INVEST_TRANSACTION_HEADER_NOMINAL');?></th>
			<th colspan="2"><?php echo JText::_('COM_INVEST_TRANSACTION_HEADER_BANKINFO'); ?></th>
			<th rowspan="2"><?php echo JText::_('COM_INVEST_TRANSACTION_HEADER_DATE'); ?></th>
			</tr>
			<tr>
			<th><?php echo JText::_('COM_INVEST_TRANSACTION_HEADER_NAMA'); ?></th>
			<th><?php echo JText::_('COM_INVEST_TRANSACTION_HEADER_PLAN'); ?></th>
			<th><?php echo JText::_('COM_INVEST_TRANSACTION_HEADER_BANK'); ?></th>
			<th><?php echo JText::_('COM_INVEST_TRANSACTION_HEADER_REKENING'); ?></th>
			</tr>
        </thead>
  
  		
	  	<?php 
	  	for($i=0;$i<count($this->data['deposit']);$i++) {
		$row =& $this->data['deposit'][$i];
 	  	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$link = JRoute::_('index.php?option=com_invest&c=deposit&task=deposit.edit&cid[]='.$row->id);
	  	?>
      	<tr class="row<?= $i % 2; ?>">
        <td class="center"><?php echo $checked; ?></td>
		<td><?php echo $row->nama; ?></td>
		<td><?php echo $row->plan." - ".$row->nama." (".$row->account.")"; ?></td>
		<td><?php echo "Rp. ".number_format($row->nominal,0,".",","); ?></td>
		<td><?php echo $row->bank; ?></td>
		<td><?php echo $row->an." [".implode(str_split($row->rekening,4),"-")."]"; ?></td>
		<td><?php echo $row->date; ?></td>
      	</tr>
      	<?php
	  	} 
	  	?>
		<tr><td colspan="15">&nbsp;</td></tr>
		<tr><td colspan="15">(*) <?php echo JText::_('COM_INVEST_DEPOSIT_BOTTOM_INFO'); ?></td></tr>
        <tr>
        <td colspan="9">
			<?php echo $this->data['pageNav']->getListFooter(); ?>		</td>
		</tr>
	</table>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="deposit" />
<input type="hidden" name="boxchecked" value="" />
<input type="hidden" name="task" value="" />    
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>
