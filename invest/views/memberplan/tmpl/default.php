<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
require_once('components/com_invest/helpers/navigation.php');

$statuslist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_FILTER_STATUS' ), 'value', 'text' );
$statuslist[]	= JHTML::_('select.option',  'InActive', JText::_( 'InActive' ), 'value', 'text' );
$statuslist[]	= JHTML::_('select.option',  'Confirm', JText::_( 'Confirm' ), 'value', 'text' );
$statuslist[]	= JHTML::_('select.option',  'Active', JText::_( 'Active' ), 'value', 'text' );
$statuslist[]	= JHTML::_('select.option',  'Complete', JText::_( 'Complete' ), 'value', 'text' );
$lists['status'] = JHTML::_('select.genericlist', $statuslist, 'fstatus', 'onchange="" class="inputbox" size="1"', 'value', 'text', JRequest::getVar('fstatus') );

$memberlist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_FILTER_MEMBER' ), 'id', 'nama' );
$memberlist = array_merge($memberlist, $this->data['listMember']);
$lists['member'] = JHTML::_('select.genericlist', $memberlist, 'member_id', 'onchange="" class="inputbox" size="1"', 'id', 'nama', JRequest::getVar('member_id') );

?>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
<p><?php echo $lists['member']." ".$lists['status']; ?><input type="button" value="Cari" onclick="document.adminForm.submit();" /></p>
	<table width="100%" class="adminlist">
        <thead>
            <tr>
            <th rowspan="2" width="2%"><label>
              <input type="checkbox" name="toggle" onclick="checkAll(<?php echo count( $this->data['plan'] ); ?>);" />
            </label></th>
            <th rowspan="2"><?php echo JText::_('COM_INVEST_MEMBERPLAN_HEADER_PLAN'); ?></th>
			<th rowspan="2"><?php echo JText::_('COM_INVEST_MEMBERPLAN_HEADER_MEMBER');?></th>
			<th rowspan="2"><?php echo JText::_('COM_INVEST_MEMBERPLAN_HEADER_INVESTASI');?></th>
            <th rowspan="2"><?php echo JText::_('COM_INVEST_MEMBERPLAN_HEADER_WAKTU');?></th>
            <th colspan="2"><?php echo JText::_('COM_INVEST_MEMBERPLAN_HEADER_PERSEN');?></th>
			<th rowspan="2"><?php echo JText::_('COM_INVEST_MEMBERPLAN_HEADER_STATUS');?></th>
            </tr>
			<tr>
			<th><?php echo JText::_('COM_INVEST_MEMBERPLAN_HEADER_PROFIT');?></th>
            <th><?php echo JText::_('COM_INVEST_MEMBERPLAN_HEADER_PENALTY');?></th>
			</tr>
        </thead>
  
  		
	  	<?php 
	  	for($i=0;$i<count($this->data['plan']);$i++) {
		$row =& $this->data['plan'][$i];
 	  	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$link = JRoute::_('index.php?option=com_invest&c=memberplan&task=plan.edit&cid[]='.$row->id);
		$linkToMember = JRoute::_('index.php?option=com_invest&c=member&task=member.edit&cid[]='.$row->member_id);
	  	?>
      	<tr class="row<?= $i % 2; ?>">
        <td class="center"><?php echo $checked; ?></td>
		<td><?php echo "<a href=".$link.">".$row->plan."-".$row->nama."(".$row->account.")</a>"; ?></td>
		<td><?php echo "<a href=".$linkToMember.">".$row->namaMember."</a>"; ?></td>
		<td><?php echo 'Rp. '.number_format($row->investasi,0,',','.'); ?></td>
		<td><?php echo $row->waktu.' Hari'; ?></td>
		<td><?php echo $row->profit.' %'; ?></td>
		<td><?php echo $row->penalty.' %'; ?></td>
		<td><?php 
		if($row->status == "InActive")
		{
			echo "<img title='investasi plan ini belum berjalan' src='components/com_invest/assets/images/cross.png' height='16' />".$row->status;
		}
		else if($row->status == "Confirm")
		{
			echo "<img title='Member telah melakukan konfirmasi pembayaran untuk investasi plan ini' src='components/com_invest/assets/images/warning.png' height='16' />".$row->status;
		}
		else if($row->status == "Confirmed")
		{
			echo "<img title='Konfirmasi pembayaran member ini telah anda terima, segera aktifkan plan investasi ini' src='components/com_invest/assets/images/warning.png' height='16' />".$row->status;
		}
		else if($row->status == "Active")
		{
			echo $row->status;
		}
		else if($row->status == "Withdraw")
		{
			echo "<img title='Member melakukan permintaan untuk penarikan uang plan investasinya' src='components/com_invest/assets/images/warning.png' height='16' />".$row->status;
		}
		else if($row->status == "Complete")
		{
			echo "<img title='Investasi plan ini telah selesai' src='components/com_invest/assets/images/ceklist.png' height='16' />".$row->status;
		}
		else 
		{
			echo "<span style='opacity:0.5'>".$row->status."</a>";
		}
		?></td>
      	</tr>
      	<?php
	  	} 
	  	?>
		<tr><td colspan="15">&nbsp;</td></tr>
		<tr><td colspan="15">(*) <?php echo JText::_('COM_INVEST_PLAN_BOTTOM_INFO'); ?></td></tr>
        <tr>
        <td colspan="9">
			<?php echo $this->data['pageNav']->getListFooter(); ?>		</td>
		</tr>
	</table>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="memberplan" />
<input type="hidden" name="boxchecked" value="" />
<input type="hidden" name="task" value="" />    
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>
