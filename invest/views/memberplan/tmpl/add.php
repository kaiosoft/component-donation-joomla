<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */


defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

require_once('components/com_invest/helpers/navigation.php');

$investHelper = new investHelper;
$pathXML = "components/com_invest/models/forms/memberplan.xml";
$form =& JForm::getInstance('plan', $pathXML);

$lists = array();
$planlist[]	= JHTML::_('select.option',  '0', JText::_( 'COM_INVEST_OPTION_SELECT_PLAN' ), 'id', 'plan' );
$planlist = array_merge($planlist, $this->data['listPlan']);
$lists['plan'] = JHTML::_('select.genericlist', $planlist, 'plan_id', 'onchange="" class="inputbox" size="1"', 'id', 'plan', $this->data['plan'][0]->plan_id );

$memberlist[]	= JHTML::_('select.option',  '0', JText::_( 'COM_INVEST_OPTION_SELECT_MEMBER' ), 'id', 'nama' );
$memberlist = array_merge($memberlist, $this->data['listMember']);
$lists['member'] = JHTML::_('select.genericlist', $memberlist, 'member_id', 'onchange="" class="inputbox" size="1"', 'id', 'nama', $this->data['plan'][0]->member_id );

$form->setFieldAttribute("investasi","default",$this->data['plan'][0]->investasi,"main");
$form->setFieldAttribute("nama","default",$this->data['plan'][0]->nama,"main");
$form->setFieldAttribute("user_id","default",$this->data['plan'][0]->user_id,"main");
$form->setFieldAttribute("verified","default",$this->data['plan'][0]->verified,"main");
$form->setFieldAttribute("status","default",$this->data['plan'][0]->status,"main");

?>
<script type="text/javascript">
Joomla.submitbutton = function(task)
{
	if(task == 'plan.save')
	{
		
       if((document.getElementById('plan_id').value == "0") || (document.getElementById('main_investasi').value == "") || (document.getElementById('main_nama').value == "") || 
		(document.getElementById('member_id').value == "") || (document.getElementById('main_status').value == ""))
		{
			alert('Semua fields bertanda (*) bintang harus terisi');
		}
		else
		{
			document.getElementById('task').value="plan.save";
			document.adminForm.submit();
		}
		
	}
	else
	{
		window.open('index.php?option=com_invest&c=memberplan','_parent');
	}
}
</script>
<script language="javascript" src="components/com_invest/assets/js/ajaxscript.js"></script>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
<!-- normal fieldsets -->
	<div class="width-60 fltlft">
		<?php
		// Iterate through the normal form fieldsets and display each one.
		foreach ($form->getFieldsets('main') as $fieldsets => $fieldset):
		?>
		<fieldset class="adminform">
			<legend>
				<?php echo JText::_($fieldset->name); ?>
			</legend>
			<!-- Fields go here -->
				<dl>
				<?php
				// Iterate through the fields and display them.
				foreach($form->getFieldset($fieldset->name) as $field):
					// If the field is hidden, only use the input.
					if ($field->hidden):
						echo $field->input;
					else:
					?>
					<dt>
						<?php echo $field->label; ?>
					</dt>
					<dd>
						<?php if($field->name == 'main[plan_id]') { echo $lists['plan']; } else if($field->name == 'main[member_id]') { echo $lists['member']; } else { echo $field->input; } ?>
					</dd>
					<?php
					endif;
				endforeach;
				?>
				</dl>
		</fieldset>
		<?php
		endforeach;
		?>
	</div>
	<div class="width-40 fltrt">
		<?php
		$transaksi = $investHelper->getDataByParam(array("member_plan_id","status"),array("='".$this->data['plan'][0]->id."'","='Pending'"),"#__invest_transaction","Array");
		echo "<div class='notifTransaksi'>";
		foreach($transaksi as $row)
		{
			echo "<div class='transaction'>";
			if($row->type == "Deposit")
			{
				$bank = $investHelper->getDataByParam("id","='".$row->bank_id."'","#__invest_bank");
				echo "Konfirmasi Pembayaran<div class='seeDetailTransaksi' id='".$row->id."' tag='Closed'>lihat detail</div>
				<div class='detailTransaksi' id='detail".$row->id."'>
				Tanggal :".$row->date."<br />
				Jenis Transaksi : Konfirmasi Pembayaran <br />
				Nominal Transaksi : Rp. ".number_format($row->nominal,0,',','.')."<br />
				Bank Tujuan : ".$bank->bank." No.Rekening : ".$bank->rekening." A/N ".$bank->an."
				</div>";
			}
			else
			{
				if($row->type == "Withdraw Profit")
				{
					$typeTransaksi = "Permintaan penarikan profit";
				}
				else
				{
					$typeTransaksi = "Permintaan penarikan investasi";
				}
				$memberBank = $investHelper->getDataByParam("id","='".$row->member_bank_id."'","#__invest_member_bank");
				$bank = $investHelper->getDataByParam("id","='".$memberBank->bank_id."'","#__invest_bank");
				echo $typeTransaksi." <div class='seeDetailTransaksi' id='".$row->id."' tag='Closed'>lihat detail</div>
				<div class='detailTransaksi' id='detail".$row->id."'>
				Tanggal :".$row->date."<br />
				Jenis Transaksi : ".$typeTransaksi."<br />
				Nominal Withdraw : Rp. ".number_format($row->nominal,0,',','.')."<br />
				Bank Member : ".$bank->bank." No.Rekening : ".$memberBank->rekening." A/N ".$memberBank->an."
				</div>";
			}
			echo "<p></p><a href='#' tag='Success' id='".$row->id."' class='doTransaksi'>Approve</a> | <a href='#' tag='Reject' id='".$row->id."' class='doTransaksi'>Reject</a><p></p></div>";
		} 
		
		echo "</div>";
		$history = $investHelper->getDataByParam(array("member_plan_id","status"),array("='".$this->data['plan'][0]->id."'","!='Pending'"),"#__invest_transaction","Array"," ORDER BY date DESC");
		echo "<div class='historyTransaksi'><div class='seeHistory' tag='closed'>History Transaksi</div>";
		echo "<div class='detailHistory'>";
		if(empty($history))
		{
			echo "Belum terdapat transaksi";
		}
		foreach($history as $row)
		{
			echo $row->date." - ".$row->type." Rp. ".number_format($row->nominal,0,',','.')." status ".$row->status."<br />";
		}
		echo "</div></div>";
		?>
	<div>
	
	<div style="clear:both"></div>
	</div>  
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="memberplan" />
<input type="hidden" name="account" value="<?php echo $this->data['plan'][0]->account; ?>" />
<input type="hidden" name="date" value="<?php echo $this->data['plan'][0]->date; ?>" />
<input type="hidden" name="profit" value="<?php echo $this->data['plan'][0]->profit; ?>" />
<input type="hidden" name="range_date" value="<?php echo $this->data['plan'][0]->range_date; ?>" />
<input type="hidden" name="persen_profit" value="<?php echo $this->data['plan'][0]->persen_profit; ?>" />
<input type="hidden" id="id" name="id" value="<?php echo $this->data['plan'][0]->id; ?>" />
<input type="hidden" id="task" name="task" value="" />
<?php 
if(($this->data['plan'][0]->id != 0) && ($this->data['plan'][0]->status == "InActive"))
{
	echo "<input type='hidden' id='sendEmail' name='sendEmail' value='sending' />";
}
else if(($this->data['plan'][0]->id != 0) && ($this->data['plan'][0]->status == "Confirm"))
{
	echo "<input type='hidden' id='sendEmail' name='sendEmail' value='sending' />";
}
else
{

}
?>
  <?php echo JHTML::_( 'form.token' ); ?>
</form>

<div class="topPopup"></div>
<div class="backPopup"></div>
</div>

<style>
.notifTransaksi
{
	margin:18px 0px;
}
.transaction , .historyTransaksi
{
	background:#EFE5A5;
	border:3px solid #ccc;
	padding:10px;
}
.seeDetailTransaksi, .seeHistory
{
	cursor:pointer;
	color:#2B91FF;
}
.detailTransaksi , .detailHistory
{
	display:none;
	background:#fff;
	padding:5px;
}
.keterangan
{
	background:#f3f3f3;
	padding:5px;
}
</style>
<script>
$(document).ready(function()
{
	$('.doTransaksi').click(function()
	{

		var status = $(this).attr('tag');
		if(status == 'Success')
		{
			var con = confirm('Dengan ini anda menyetujui atau anda menyatakan sudah menerima pembayaran member untuk konfirmasi pembayarannya atau melakukan transfer sesuai permintaan member untuk penarikan dana ? ');
		}
		else
		{
			var con = confirm('Dengan ini anda memebatalkan proses transaksi member ? ');
		}
		if(con == true)
		{
			$('.topPopup').html('<img src="components/com_invest/assets/images/process.gif" />');
			$('.topPopup').css('top','200px');
			$('.topPopup').css('left','500px');
			$('.topPopup').fadeIn();
			$('.backPopup').fadeIn();
			var id = $(this).attr('id');
			$.ajax({
				type:"POST",
				url:"<?php JURI::Base().'index.php'; ?>",
				data:{option:"com_invest",c:"memberplan",task:"doTransaksi",id:id,status:status},
			}).done(function(show)
			{
				$('.topPopup').html(show);
				window.open('index.php?option=com_invest&c=memberplan&task=plan.edit&cid[]=<?php echo $this->data['plan'][0]->id; ?>','_parent');
			});
		}
		else
		{
			alert('perintah dibatalkan');
		}
	});
	
	$('.seeDetailTransaksi').click(function()
	{
		var tag = $(this).attr('tag');
		var id = $(this).attr('id');
		
		if(tag == "Closed")
		{
			$('#detail'+id).slideDown('slow');
			$(this).attr('tag','Open');
			$(this).text('Tutup Detail');
		}
		else
		{
			$('#detail'+id).slideUp('slow');
			$(this).attr('tag','Closed');
			$(this).text('Lihat Detail');
		}
	});
	
	$('.seeHistory').click(function()
	{
		var tag = $(this).attr('tag');
		
		if(tag == "Closed")
		{
			$('.detailHistory').slideDown('slow');
			$(this).attr('tag','Open');
		}
		else
		{
			$('.detailHistory').slideUp('slow');
			$(this).attr('tag','Closed');
		}
	});
	
	$('.backPopup').click(function()
	{
		$('.topPopup').fadeOut();
		$(this).fadeOut();
	});
	
});
</script>