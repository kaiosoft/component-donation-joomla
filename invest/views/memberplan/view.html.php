<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewMemberplan extends JView
{

	function display($tpl = null)
	{
		$app =& JFactory::getApplication();
		$task = JRequest::getVar('task');
		$id = JRequest::getVar('cid');
		$helper = new investHelper;
    
		$modelPlan =& JModel::getInstance('memberplan','InvestModel');
		$modelPlan2 =& JModel::getInstance('plan','InvestModel');
		$modelMember =& JModel::getInstance('member','InvestModel');
		
		//set Filter
		$member_id = JRequest::getVar('member_id');
		$fstatus = JRequest::getVar('fstatus');
		$filter = $helper->setFilter(array("a.member_id","a.status"),array("='".$member_id."'","='".$fstatus."'"));
		if(empty($filter))
		{
			$filter = "WHERE a.status != 'Closed'";
		}
		
		$limit		= $app->getUserStateFromRequest( 'global.list.limit', 'limit', $app->getCfg('list_limit'), 'int' );
		$limitstart = $app->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
		
		// get the total number of records
		$total = $modelPlan->getTotal($filter);
			
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
		
		if(($task == "plan.add") || ($task == "plan.edit"))
		{
			$plan = $modelPlan->getPlanById($id[0]);
		}
		else
		{
			$plan = $modelPlan->getAllPlan($filter,$pageNav);
		}		

		$listPlan = $modelPlan2->getListPlan();
		$listMember = $modelMember->getListMember();
		
		//prepare data
		$data = array();
		$data['plan'] = $plan;
		$data['pageNav'] = $pageNav;
		$data['listPlan'] = $listPlan;
		$data['listMember'] = $listMember;

		$this->addToolBar($task);
		
		$this->assignRef('data',$data);
		parent::display($tpl);
		
	}
	
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar($task) 
	{
		if($task=="plan.add" || $task=="plan.edit"){
			JRequest::setVar('hidemainmenu', true);
			JToolBarHelper::title(JText::_('COM_INVEST_MEMBERPLAN_TITLE_ADD'));
			JToolBarHelper::save('plan.save');
			JToolBarHelper::cancel('plan.cancel');
		} else {
			JToolBarHelper::title(JText::_('COM_INVEST_MEMBERPLAN_TITLE_MANAGER'));
			JToolBarHelper::deleteList('', 'plan.delete');
			JToolBarHelper::editList('plan.edit');
			JToolBarHelper::addNew('plan.add');
		}
	}
		
}	

?>
