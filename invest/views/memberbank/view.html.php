<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewMemberbank extends JView
{

	function display($tpl = null)
	{
		$app =& JFactory::getApplication();
		$task = JRequest::getVar('task');
		$id = JRequest::getVar('cid');
		$helper = new investHelper;
		
	    $modelBank =& JModel::getInstance('memberbank','InvestModel');
		$modelBank2 =& JModel::getInstance('bank','InvestModel');
		$modelMember =& JModel::getInstance('member','InvestModel');
		
		//set Filter
		$member_id = JRequest::getVar('member_id');
		$bank_id = JRequest::getVar('bank_id');
		$filter = $helper->setFilter(array("member_id","bank_id"),array("='".$member_id."'","='".$bank_id."'"));
		
		$limit		= $app->getUserStateFromRequest( 'global.list.limit', 'limit', $app->getCfg('list_limit'), 'int' );
		$limitstart = $app->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
		
		// get the total number of records
		$total = $modelBank->getTotal($filter);
			
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
		
		if(($task == "bank.add") || ($task == "bank.edit"))
		{
			$bank = $modelBank->getBankById($id[0]);
		}
		else
		{
			$bank = $modelBank->getAllBank($filter,$pageNav);
		}		
		
		$listMember = $modelMember->getListMember();
		$listBank = $modelBank2->getListBank();

		//prepare data
		$data = array();
		$data['bank'] = $bank;
		$data['pageNav'] = $pageNav;
		$data['listMember'] = $listMember;
		$data['listBank'] = $listBank;

		$this->addToolBar($task);
		
		$this->assignRef('data',$data);
		parent::display($tpl);
		
	}
	
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar($task) 
	{
		if($task=="bank.add" || $task=="bank.edit"){
			JRequest::setVar('hidemainmenu', true);
			JToolBarHelper::title(JText::_('COM_INVEST_MEMBERBANK_TITLE_ADD'));
			JToolBarHelper::save('bank.save');
			JToolBarHelper::cancel('bank.cancel');
		} else {
			JToolBarHelper::title(JText::_('COM_INVEST_MEMBERBANK_TITLE_MANAGER'));
			JToolBarHelper::deleteList('', 'bank.delete');
			JToolBarHelper::editList('bank.edit');
			JToolBarHelper::addNew('bank.add');
		}
	}
		
}	

?>
