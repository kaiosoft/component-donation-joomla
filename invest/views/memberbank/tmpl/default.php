<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
require_once('components/com_invest/helpers/navigation.php');

$memberlist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_FILTER_MEMBER' ), 'id', 'nama' );
$memberlist = array_merge($memberlist, $this->data['listMember']);
$lists['member'] = JHTML::_('select.genericlist', $memberlist, 'member_id', 'onchange="" class="inputbox" size="1"', 'id', 'nama', JRequest::getVar('member_id') );

$banklist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_FILTER_BANK' ), 'id', 'bank' );
$banklist = array_merge($banklist, $this->data['listBank']);
$lists['bank'] = JHTML::_('select.genericlist', $banklist, 'bank_id', 'onchange="" class="inputbox" size="1"', 'id', 'bank', JRequest::getVar('bank_id') );
?>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
<p><?php echo $lists['member']." ".$lists['bank']; ?><input type="button" value="Cari" onclick="document.adminForm.submit();" /></p>
	<table width="100%" class="adminlist">
        <thead>
            <tr>
            <th width="2%"><label>
              <input type="checkbox" name="toggle" onclick="checkAll(<?php echo count( $this->data['bank'] ); ?>);" />
            </label></th>
            <th><?php echo JText::_('COM_INVEST_BANK_HEADER_BANK'); ?></th>
			<th><?php echo JText::_('COM_INVEST_BANK_HEADER_REKENING');?></th>
			<th><?php echo JText::_('COM_INVEST_BANK_HEADER_AN'); ?></th>
			<th><?php echo JText::_('COM_INVEST_BANK_HEADER_MEMBER'); ?></th>
			</tr>
        </thead>
  
  		
	  	<?php 
	  	for($i=0;$i<count($this->data['bank']);$i++) {
		$row =& $this->data['bank'][$i];
 	  	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$link = JRoute::_('index.php?option=com_invest&c=memberbank&task=bank.edit&cid[]='.$row->id);
	  	?>
      	<tr class="row<?= $i % 2; ?>">
        <td class="center"><?php echo $checked; ?></td>
        <td><?php echo "<a href=".$link.">".$row->bank."</a>"; ?></td>
		<td><?php echo implode(str_split($row->rekening,4),"-"); ?></td>
		<td><?php echo $row->an; ?></td>
		<td><?php echo $row->nama; ?></td>
      	</tr>
      	<?php
	  	} 
	  	?>
		<tr><td colspan="15">&nbsp;</td></tr>
		<tr><td colspan="15">(*) <?php echo JText::_('COM_INVEST_MEMBERBANK_BOTTOM_INFO'); ?></td></tr>
        <tr>
        <td colspan="9">
			<?php echo $this->data['pageNav']->getListFooter(); ?>		</td>
		</tr>
	</table>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="memberbank" />
<input type="hidden" name="boxchecked" value="" />
<input type="hidden" name="task" value="" />    
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>
