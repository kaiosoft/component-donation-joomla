<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;

?>
<div id="dashboardBox">
	<h2> Data Member </h2>
	<div class="box">
		<a href="index.php?option=com_invest&c=member"><img src="components/com_invest/assets/images/icon-64-member.png" /><br />Member</a>
	</div>
	<div class="box">
		<a href="index.php?option=com_invest&c=plan"><img src="components/com_invest/assets/images/icon-64-plan.png" /><br />Member Plan</a>
	</div>
	<div class="box">
		<a href="index.php?option=com_invest&c=bank"><img src="components/com_invest/assets/images/icon-64-bank.png" /><br />Member Bank</a>
	</div>
	<div style="clear:both"></div>
	<p>&nbsp;</p>
	<div class="box">
		<a href="index.php?option=com_invest&c=plan"><img src="components/com_invest/assets/images/icon-64-plan.png" /><br />Plan</a>
	</div>
	<div class="box">
		<a href="index.php?option=com_invest&c=bank"><img src="components/com_invest/assets/images/icon-64-bank.png" /><br />Bank</a>
	</div>
	<div class="box">
		<a href="index.php?option=com_invest&c=deposit"><img src="components/com_invest/assets/images/icon-64-deposit.png" /><br />Confirmation</a>
	</div>
	<div class="box">
		<a href="index.php?option=com_invest&c=withdraw"><img src="components/com_invest/assets/images/icon-64-withdraw.png" /><br />Withdraw</a>
	</div>
	<div class="box">
		<a href="index.php?option=com_invest&c=config"><img src="components/com_invest/assets/images/icon-64-config.png" /><br />Configuration</a>
	</div>
	<div class="clear"></div>
</div>
<div id="dashboardInfo">
	<h2>Component Invest</h2>
	<p>
	Version : 1.0<br />
	Creating date : April<br />
	Author : Kaio Software
	</p>
	<p>
	Component Invest adalah sebuah sistem yang berjalan di joomla 1.6.x, 1.7.x, 2.5.x digunakan untuk menangani informasi menyeluruh dari member Trader
	beberapa fitur com ajib ini antaranya :
	<ul>
		<li>Keanggotaan / Member</li>
		<li>Data Exchange (broker)</li>
		<li>Deposit</li>
		<li>Withdraw</li>
		<li>Report / History</li>
	</ul>
	Untuk informasi lengkap kunjungi website kami :
	<ul>
	<li><a href="http://kaiogroup.com" target="_blank">http://www.Kaiogroup.com</a></li>
	<li><a href="mailto:info@kaiogroup.com">info@kaiogroup.com</a></li>
	</ul>
	</p>
	<p>&nbsp;</p>
	<p align="center">Powered by <a href="http://kaiogroup.com" target="_blank">Kaiogroup</a></p>
</div>