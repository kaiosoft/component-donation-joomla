<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewDashboard extends JView
{

	function display($tpl = null)
	{
		$task = JRequest::getVar('task');
		
		$this->addToolBar($task);
		parent::display($tpl);
		
	}
	
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar($task) 
	{
			JToolBarHelper::title(JText::_('Dashboard'));
	}
		
}	

?>
