<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewWithdraw extends JView
{

	public $statusTransaction;

	function display($tpl = null)
	{
		$app =& JFactory::getApplication();
		$task = JRequest::getVar('task');
		$id = JRequest::getVar('cid');
		$investHelper = new investHelper;
		$status = JRequest::getVar('status');
		
		$modelWithdraw =& JModel::getInstance('withdraw','InvestModel');
		$modelMember =& JModel::getInstance('member','InvestModel');
		
		//set variable
		$nama_member = JRequest::getVar('namaMember');
		$mulai = JRequest::getVar('mulai');
		if(!empty($mulai))
		{
			$mulai = $mulai." 00:00:00";
		}
		$sampai = JRequest::getVar('sampai');
		if(!empty($sampai))
		{
			$sampai = $sampai." 23:59:00";
		}
		
		//setFilter
		$paramFilter = array("f.nama=","a.date>=","a.date<=");
		$valueFilter = array($nama_member,$mulai,$sampai);
		$filter = $investHelper->setFilter($paramFilter,$valueFilter);
		$filter = $investHelper->unsetWhereFilter($filter);
		
		$limit		= $app->getUserStateFromRequest( 'global.list.limit', 'limit', $app->getCfg('list_limit'), 'int' );
		$limitstart = $app->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
		
		// get the total number of records
		$total = $modelWithdraw->getTotal($filter);
			
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
		
		if(($task == "withdraw.add") || ($task == "withdraw.edit"))
		{
			$withdraw = $modelWithdraw->getWithdrawById($id[0]);
		}
		else
		{
			$withdraw = $modelWithdraw->getAllWithdraw($filter,$pageNav);
		}		
		
		$listMember = $modelMember->getListMember();
		JRequest::setVar('listMember',$listMember);
		
		if(!empty($status))
		{
			$this->statusTransaction = $status;
			$this->updateStatus();	
		}

		//prepare data
		$data = array();
		$data['withdraw'] = $withdraw;
		$data['pageNav'] = $pageNav;

		$this->addToolBar($task);
		
		$this->assignRef('data',$data);
		parent::display($tpl);
		
	}
	
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar($task) 
	{
		if($task=="withdraw.add" || $task=="withdraw.edit"){
			JRequest::setVar('hidemainmenu', true);
			JToolBarHelper::title(JText::_('COM_INVEST_WITHDRAW_TITLE_ADD'));
			JToolBarHelper::save('withdraw.save');
			JToolBarHelper::cancel('withdraw.cancel');
		} else {
			JToolBarHelper::title(JText::_('COM_INVEST_WITHDRAW_TITLE_MANAGER'));
			//JToolBarHelper::deleteList('', 'withdraw.delete');
			//JToolBarHelper::editList('withdraw.edit');
			//JToolBarHelper::addNew('withdraw.add');
		}
	}
	
	function updateStatus()
	{
		$app =& JFactory::getApplication();
		$investHelper = new investHelper;
		$user =& JFactory::getUser();
		
		/*//cek permission
		if($ajibHelper->getPermission($user->id) <= 6)
		{
			$text = "Anda tidak mempunyai izin untuk melakukan ini";
			$app->redirect( 'index.php?option=com_ajib&c=order',$text,'error' ); 
			exit();
		}*/
		
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), 'method', 'array' );
		$n		= count( $cid );  
		 
		if (count( $cid ) < 1) {
			echo "Pilih terlebih dulu data yang akan anda update";
			return false;
		}
		else if(count($cid) == 1)
		{
			if(array_sum($cid) == 0)
			{
			$text = "Pilih terlebih dulu data yang akan anda update";
			$app->redirect('index.php?option=com_invest&c=withdraw',$text,'error');
			}
		}
		
		JArrayHelper::toInteger( $cid );
		
		$data = $investHelper->updateStatusTransaction($this->statusTransaction,$cid);
		$text = $data['text'];
		$mode = $data['mode'];
		
		if($mode != "error")
		{
			//send mail
			if($investHelper->getConfig('sendEmail') == "true")
			{
				
				$emailAdmin = $investHelper->getConfig('emailAdmin');
				
				print_r($cid);
				
				for($i=0;$i<count($cid);$i++)
				{
					$member = $investHelper->getDataMemberById($cid[$i]);
					if($this->statusTransaction == "Success")
					{
							$msg = $investHelper->getConfig('successWithdraw');
							$msg = str_replace("[namaMember]",$member->nama,$msg);
							$msg = str_replace("[nominalDollar]",$member->dollar,$msg);
							$msg = str_replace("[nominalRupiah]",$member->rupiah,$msg);
							$msg = str_replace("[rekening]",$member->bank." a/n".$member->an." No rekening ".$member->rekening,$msg);
					}
					else
					{
							$msg = $investHelper->getConfig('rejectWithdraw');
							$msg = str_replace("[namaMember]",$member->nama,$msg);
					}
					
					$investHelper->sendMail($member->email,$emailAdmin,"Admin Invest","Status Transaction",$msg);
				}
			}
		}
	
		if(empty($text))
		{
		$text = "Status Telah Di Update";
		}
		$app->redirect( 'index.php?option=com_invest&c=withdraw',$text,$mode ); 
	}
		
}	

?>

