<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */


defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

require_once('components/com_invest/helpers/navigation.php');
?>
<script type="text/javascript">
Joomla.submitbutton = function(task)
{
	if(task == 'bank.save')
	{
		 
        if((document.getElementById('bank').value == "") ||	(document.getElementById('rekening').value == "") || 
		(document.getElementById('an').value == ""))
		{
			alert('Semua fields bertanda (*) bintang harus terisi');
		}
		else
		{
			document.getElementById('task').value="bank.save";
			document.adminForm.submit();
		}
	}
	else
	{
		window.open('index.php?option=com_invest&c=bank','_parent');
	}
}
</script>
<script language="javascript" src="components/com_invest/assets/js/ajaxscript.js"></script>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
  <table width="100%" border="0" cellpadding="4" cellspacing="1" class="admintable">
    <tr>
      <td width="18%" align="right" class="key"><?php echo JText::_('COM_INVEST_BANK_HEADER_BANK'); ?> * : </td>
      <td width="82%"><input name="bank" type="text" id="bank" value="<?php echo $this->data['bank'][0]->bank; ?>" size="30"/></td>
    </tr>
	<tr>
      <td width="18%" align="right" class="key"><?php echo JText::_('COM_INVEST_BANK_HEADER_REKENING'); ?> * : </td>
      <td width="82%"><input name="rekening" type="text" id="rekening" value="<?php echo $this->data['bank'][0]->rekening; ?>" size="30"/></td>
    </tr>
	<tr>
      <td width="18%" align="right" class="key"><?php echo JText::_('COM_INVEST_BANK_HEADER_AN'); ?> * : </td>
      <td width="82%"><input name="an" type="text" id="an" value="<?php echo $this->data['bank'][0]->an; ?>" size="30"/></td>
    </tr>
  </table>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="bank" />
<input type="hidden" id="id" name="id" value="<?php echo $this->data['bank'][0]->id; ?>" />
<input type="hidden" id="task" name="task" value="" />
  <?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>