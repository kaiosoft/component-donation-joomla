<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */


defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

require_once('components/com_invest/helpers/navigation.php');

$pathXML = "components/com_invest/models/forms/plan.xml";
$form =& JForm::getInstance('plan', $pathXML);

$form->setFieldAttribute("plan","default",$this->data['plan'][0]->plan,"main");
$form->setFieldAttribute("investasi","default",$this->data['plan'][0]->investasi,"main");
$form->setFieldAttribute("waktu","default",$this->data['plan'][0]->waktu,"main");
$form->setFieldAttribute("description","default",$this->data['plan'][0]->description,"main");
$form->setFieldAttribute("term","default",$this->data['plan'][0]->term,"main");
$form->setFieldAttribute("profit","default",$this->data['plan'][0]->profit,"extra");
$form->setFieldAttribute("penalty","default",$this->data['plan'][0]->penalty,"extra");
?>
<script type="text/javascript">
Joomla.submitbutton = function(task)
{
	if(task == 'plan.save')
	{
		 
        if((document.getElementById('main_plan').value == "") || (document.getElementById('main_investasi').value == "") || (document.getElementById('main_waktu').value == "") || 
		(document.getElementById('extra_profit').value == "") || (document.getElementById('extra_penalty').value == ""))
		{
			alert('Semua fields bertanda (*) bintang harus terisi');
		}
		else
		{
			document.getElementById('task').value="plan.save";
			document.adminForm.submit();
		}
		
	}
	else
	{
		window.open('index.php?option=com_invest&c=plan','_parent');
	}
}
</script>
<script language="javascript" src="components/com_invest/assets/js/ajaxscript.js"></script>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
<!-- normal fieldsets -->
	<div class="width-60 fltlft">
		<?php
		// Iterate through the normal form fieldsets and display each one.
		foreach ($form->getFieldsets('main') as $fieldsets => $fieldset):
		?>
		<fieldset class="adminform">
			<legend>
				<?php echo JText::_($fieldset->name); ?>
			</legend>
			<!-- Fields go here -->
				<dl>
				<?php
				// Iterate through the fields and display them.
				foreach($form->getFieldset($fieldset->name) as $field):
					// If the field is hidden, only use the input.
					if ($field->hidden):
						echo $field->input;
					else:
					?>
					<dt>
						<?php echo $field->label; ?>
					</dt>
					<dd<?php echo ($field->type == 'Editor' || $field->type == 'Textarea') ? ' style="clear: both; margin: 0;"' : ''?>>
						<?php echo $field->input; ?>
					</dd>
					<?php
					endif;
				endforeach;
				?>
				</dl>
		</fieldset>
		<?php
		endforeach;
		?>
	</div>
	<div class="width-40 fltrt">
    <?php 
	jimport('joomla.html.pane');
	//sliders | tabs
	$pane =& JPane::getInstance('tabs', array('startOffset'=>2, 'allowAllClose'=>true, 'opacityTransition'=>true, 'duration'=>600));
	echo $pane->startPane("extra"); ?>
        <?php
        // Iterate through the extra form fieldsets and display each one.
        foreach ($form->getFieldsets("extra") as $fieldsets => $fieldset):
        ?>
        <?php echo $pane->startPanel(JText::_($fieldset->name), $fieldsets); ?>
        <fieldset class="panelform">
            <dl>
            <?php
            // Iterate through the fields and display them.
            foreach($form->getFieldset($fieldset->name) as $field):
                // If the field is hidden, just display the input.
                if ($field->hidden):
                    echo $field->input;
                else:
                ?>
                <dt>
                    <?php echo $field->label; ?>
                </dt>
                <dd>
                    <?php echo $field->input; ?>
                </dd>
                <?php
                endif;
            endforeach;
            ?>
            </dl>
        </fieldset>
        <?php echo $pane->endPanel(); ?>
        <?php
        endforeach;
        ?>
    <?php echo $pane->endPane(); ?>
	</div>
	</div>  
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="plan" />
<input type="hidden" id="id" name="id" value="<?php echo $this->data['plan'][0]->id; ?>" />
<input type="hidden" id="task" name="task" value="" />
  <?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>