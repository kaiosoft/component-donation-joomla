<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
require_once('components/com_invest/helpers/navigation.php');
?>
<div id="boxRight">
<form action="index.php" method="post" name="adminForm">
	<table width="100%" class="adminlist">
        <thead>
            <tr>
            <th rowspan="2" width="2%"><label>
              <input type="checkbox" name="toggle" onclick="checkAll(<?php echo count( $this->data['plan'] ); ?>);" />
            </label></th>
            <th rowspan="2"><?php echo JText::_('COM_INVEST_PLAN_HEADER_PLAN'); ?></th>
			<th rowspan="2"><?php echo JText::_('COM_INVEST_PLAN_HEADER_INVESTASI');?></th>
            <th rowspan="2"><?php echo JText::_('COM_INVEST_PLAN_HEADER_WAKTU');?></th>
            <th colspan="2"><?php echo JText::_('COM_INVEST_PLAN_HEADER_PERSEN');?></th>
            </tr>
			<tr>
			<th><?php echo JText::_('COM_INVEST_PLAN_HEADER_PROFIT');?></th>
            <th><?php echo JText::_('COM_INVEST_PLAN_HEADER_PENALTY');?></th>
			</tr>
        </thead>
  
  		
	  	<?php 
	  	for($i=0;$i<count($this->data['plan']);$i++) {
		$row =& $this->data['plan'][$i];
 	  	$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$link = JRoute::_('index.php?option=com_invest&c=plan&task=plan.edit&cid[]='.$row->id);
	  	?>
      	<tr class="row<?= $i % 2; ?>">
        <td class="center"><?php echo $checked; ?></td>
		<td><?php echo "<a href=".$link.">".$row->plan."</a>"; ?></td>
		<td><?php echo 'Rp. '.number_format($row->investasi,0,',','.'); ?></td>
		<td><?php echo $row->waktu.' Hari'; ?></td>
		<td><?php echo $row->profit.' %'; ?></td>
		<td><?php echo $row->penalty.' %'; ?></td>
      	</tr>
      	<?php
	  	} 
	  	?>
		<tr><td colspan="15">&nbsp;</td></tr>
		<tr><td colspan="15">(*) <?php echo JText::_('COM_INVEST_PLAN_BOTTOM_INFO'); ?></td></tr>
        <tr>
        <td colspan="9">
			<?php echo $this->data['pageNav']->getListFooter(); ?>		</td>
		</tr>
	</table>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="plan" />
<input type="hidden" name="boxchecked" value="" />
<input type="hidden" name="task" value="" />    
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>
