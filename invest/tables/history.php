<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * @package		Joomla
 * @subpackage	Invest
 */
class TableWithdraw extends JTable
{
	var $id			= null;
	var $member_exchange_id		= null;
	var $tiket		= null;
	var $history		= null;
	var $nominal		= null;
	var $keterangan		= null;
	var $status		= null;
	var $date		= null;

	function __construct( &$db )
	{
		parent::__construct( '#__invest_history', 'id', $db );
	}

}
?>