<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * @package		Joomla
 * @subpackage	Invest
 */
class TableExchange extends JTable
{
	var $id			= null;
	var $exchange		= null;
	var $kurs_jual		= null;
	var $kurs_beli		= null;
	var $fee		= null;
	var $referal		= null;

	function __construct( &$db )
	{
		parent::__construct( '#__invest_exchange', 'id', $db );
	}

}
?>