<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * @package		Joomla
 * @subpackage	Invest
 */
class TablePlan extends JTable
{
	var $id			= null;
	var $plan		= null;
	var $investasi		= null;
	var $waktu		= null;
	var $profit		= null;
	var $penalty		= null;
	var $description		= null;
	var $term		= null;

	function __construct( &$db )
	{
		parent::__construct( '#__invest_plan', 'id', $db );
	}

}
?>