<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * @package		Joomla
 * @subpackage	Invest
 */
class TableMemberplan extends JTable
{
	var $id			= null;
	var $member_id		= null;
	var $plan_id		= null;
	var $account		= null;
	var $nama		= null;
	var $investasi		= null;
	var $profit		= null;
	var $persen_profit		= null;
	var $range_date		= null;
	var $status		= null;
	
	function __construct( &$db )
	{
		parent::__construct( '#__invest_member_plan', 'id', $db );
	}

}
?>