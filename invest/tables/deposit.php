<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * @package		Joomla
 * @subpackage	Invest
 */
class TableTransaction extends JTable
{
	var $id			= null;
	var $member_exchange_id		= null;
	var $type		= null;
	var $tiket		= null;
	var $dollar		= null;
	var $rupiah		= null;
	var $bank_id		= null;
	var $member_bank_id		= null;
	var $status		= null;
	var $date		= null;

	function __construct( &$db )
	{
		parent::__construct( '#__invest_transaction', 'id', $db );
	}

}
?>