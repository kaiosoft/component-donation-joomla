<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * @package		Joomla
 * @subpackage	Invest
 */
class TableMemberbank extends JTable
{
	var $id			= null;
	var $member_id		= null;
	var $bank_id		= null;
	var $rekening		= null;
	var $an		= null;

	function __construct( &$db )
	{
		parent::__construct( '#__invest_member_bank', 'id', $db );
	}

}
?>