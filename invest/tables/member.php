<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * @package		Joomla
 * @subpackage	Invest
 */
class TableMember extends JTable
{

 	 	 	 	 	

	var $id			= null;
	var $user_id	= null;
	var $nama		= null;
	var $type		= null;
	var $penanggung_jawab		= null;
	var $hp			= null;
	var $status		= null;
	var $balance		= null;
	var $legalitas		= null;
	var $date		= null;

	function __construct( &$db )
	{
		parent::__construct( '#__invest_member', 'id', $db );
	}

}
?>