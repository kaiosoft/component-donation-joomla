<?

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
defined('_JEXEC') or die('Restricted access');
 
// import joomla controller library
jimport('joomla.application.component.controller');
 
require_once(JPATH_COMPONENT.DS.'controller.php');

JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'function.php');

?><script src="administrator/components/com_invest/assets/js/jquery.js"></script><?php
$doc =& JFactory::getDocument();
$doc->addStyleSheet('components/com_invest/assets/stylesheet/style.css');
 
// Get an instance of the controller prefixed by HelloWorld
//$controller = JController::getInstance('Dashboard'); 

$controller = JRequest::getWord('view');

// Require specific controller if requested
if($controller==''){
	$controller = "Plan";
}
 
$controller_path = strtolower($controller);
$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller_path.'.php';
require_once $path;
 
// Create the controller
$classname    = 'InvestController'.ucfirst($controller);
$controller   = new $classname( );
 
// Perform the Request task
$controller->execute(JRequest::getCmd('task'));
 
// Redirect if set by the controller
$controller->redirect();
?>