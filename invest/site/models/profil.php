<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class InvestModelProfil extends JModel{

	function getProfil($user_id){
		$sql = "SELECT a.*,b.email FROM #__invest_member AS a LEFT JOIN #__users AS b ON (b.id=a.user_id) WHERE a.user_id='".$user_id."'";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObject();
		return $rows;
	}
	
	function cekCompleteProfil($data)
	{
		$complete = array();
		//kelengkapan profil
		if((empty($data->nama)) || (empty($data->type)) || (empty($data->hp))
		|| (empty($data->penanggung_jawab)) || (empty($data->hp)) || (empty($data->identitas))
		|| (empty($data->no_identitas)))
		{
			$complete[] = "inComplete";
		}
		
		//aktif user
		if($data->status == "InActive")
		{
			$complete[] = "inActive";
		}
		
		//data bank
		$sql = "SELECT * FROM #__invest_member_bank WHERE member_id='".$data->id."'";
		$this->_db->setQuery($sql);
		$bank = $this->_db->loadObjectList();
		$jmlhBank = count($bank);
		
		if(empty($jmlhBank))
		{
			$complete[] = "emptyBank";
		}
		
		return $complete;
		
	}
	
}

?>