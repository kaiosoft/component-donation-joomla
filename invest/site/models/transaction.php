<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class InvestModelTransaction extends JModel{

	function getWithdraw($filter, $pageNav){
		$sql = "SELECT a.*,b.nama,c.plan,d.rekening,d.an,e.bank FROM 
		#__invest_transaction AS a LEFT JOIN 
		#__invest_member_plan AS b ON (b.id=a.member_plan_id) LEFT JOIN 
		#__invest_plan AS c ON (c.id=b.plan_id) LEFT JOIN 
		#__invest_member_bank AS d ON (d.id=a.member_bank_id) LEFT JOIN
		#__invest_bank AS e ON (e.id=d.bank_id) WHERE a.type!='Deposit' ".$filter." ORDER BY a.date DESC";
		$this->_db->setQuery($sql, $pageNav->limitstart, $pageNav->limit);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getDeposit($filter, $pageNav){
		$sql = "SELECT a.*,b.nama,c.plan,d.rekening,d.an,d.bank FROM 
		#__invest_transaction AS a LEFT JOIN 
		#__invest_member_plan AS b ON (b.id=a.member_plan_id) LEFT JOIN 
		#__invest_plan AS c ON (c.id=b.plan_id) LEFT JOIN 
		#__invest_bank AS d ON (d.id=a.bank_id) WHERE a.type='Deposit' ".$filter;
		$this->_db->setQuery($sql, $pageNav->limitstart, $pageNav->limit);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getTotal($filter)
	{
		$query = "SELECT * FROM #__invest_transaction".$filter;
		$this->_db->setQuery( $query );
		$total = $this->_db->loadResult();
		return $total;
	}
}

?>