<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class InvestModelMyplan extends JModel{

	function getAllPlan($filter, $pageNav){
		$sql = "SELECT a.*,b.plan,c.nama AS namaMember FROM #__invest_member_plan AS a LEFT JOIN #__invest_plan AS b ON (b.id=a.plan_id) LEFT JOIN #__invest_member AS c ON (c.id=a.member_id) ".$filter;
		$this->_db->setQuery($sql, $pageNav->limitstart, $pageNav->limit);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getPlanById($id)
	{
		$sql = "SELECT * FROM #__invest_plan WHERE id = '".$id."'";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getTotal($filter)
	{
		$query = "SELECT COUNT(*) FROM #__invest_member_plan AS a LEFT JOIN #__invest_plan AS b ON (b.id=a.plan_id) LEFT JOIN #__invest_member AS c ON (c.id=a.member_id) ".$filter;
		$this->_db->setQuery( $query );
		$total = $this->_db->loadResult();
		return $total;
	}
	
	function getListPlan()
	{
		$query = "SELECT id,exchange FROM #__invest_plan";
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;	
	}
	
	
}

?>