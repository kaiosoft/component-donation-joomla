<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewMyPlan extends JView
{

	function display($tpl = null)
	{
		$app =& JFactory::getApplication();
		$task = JRequest::getVar('task');
		$user =& JFactory::getUser();
		$helper = new investHelper;
		
		$modelPlan =& JModel::getInstance('myplan','InvestModel');
		
		if(empty($user->id))
		{
			header("location:".JURI::Base()."index.php?option=com_invest&view=plan");
		}
		
		//setFilter
		$member = $helper->getDataByParam("user_id","='".$user->id."'","#__invest_member");
		$id = JRequest::getVar('id');
		$filter = $helper->setFilter(array("a.member_id","a.id"),array("='".$member->id."'","='".$id."'"));
		
		$limit		= $app->getUserStateFromRequest( 'global.list.limit', 'limit', $app->getCfg('list_limit'), 'int' );
		$limitstart = $app->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
		
		// get the total number of records
		$total = $modelPlan->getTotal($filter);
			
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
		
		$plan = $modelPlan->getAllPlan($filter,$pageNav);
		
		//prepare data
		$data = array();
		$data['plan'] = $plan;
		$data['pageNav'] = $pageNav;
		
		$this->assignRef('data',$data);
		parent::display($tpl);
		
	}
		
}	

?>
