<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
//not allow direct access
//error_reporting(0);
defined('_JEXEC') or die('Restricted Access');

$lists = array();
$chartlist[]	= JHTML::_('select.option',  'AreaChart', JText::_( 'Area Chart' ), 'value', 'text' );
$chartlist[]	= JHTML::_('select.option',  'LineChart', JText::_( 'Line Chart' ), 'value', 'text' );
$chartlist[]	= JHTML::_('select.option',  'ColumnChart', JText::_( 'Column Chart' ), 'value', 'text' );
$lists['chart'] = JHTML::_('select.genericlist', $chartlist, 'chart', 'class="inputbox" size="1"', 'value', 'text', $_GET['typechart']);

?>
<div class="COM_INV_STATMYPLAN">
<?php
$helper = new investHelper;
$type = JRequest::getVar('type');


//preparing data
$persen_profit = explode(";",$this->data['plan'][0]->persen_profit);
$range_date = explode(";",$this->data['plan'][0]->range_date);

//get plan
$plan = $helper->getDataByParam("id","='".$this->data['plan'][0]->plan_id."'","#__invest_plan");
$profitInvest = $helper->profitInvest($this->data['plan'][0]->investasi,$this->data['plan'][0]->persen_profit);
$withdrawalProfit = $profitInvest - $this->data['plan'][0]->profit;

//show range date by date
$showDate = array();
$now = date("Y-m-d");
$jam = date("H");
foreach($range_date as $row)
{
	if($row < $now)
	{
		$showDate[] = $row;
	}
	else if(($row == $now) && ($jam >= 16))
	{
		$showDate[] = $row;
	}
	else
	{
		break;
	}
}

//count total range_date by date
$total = count($showDate);

//get profit
$profitToday = $persen_profit[$total-1];
$profitYesterday = $persen_profit[$total-2];
$compare = $profitToday - $profitYesterday;

$imgUpProfit = "<img src='".JURI::Base()."administrator/components/com_invest/assets/images/increase.png' height='16' />";
$imgDownProfit = "<img src='".JURI::Base()."administrator/components/com_invest/assets/images/decrease.png' height='16' />";
?><p onclick='window.print();' style='cursor:pointer'>Print</p><?php
//get range date
if(empty($showDate))
{
	$isShowDate = 0;
	$html = "<div class='showMessInfo'>Persentase profit anda dapat anda lihat mulai tanggal ".$range_date[1]."</div>";
}
else
{
	if($this->data['plan'][0]->status == "Active")
	{
		$isShowDate = 1;
		if($jam < 16)
		{
			$html .= "<div class='showMessInfo'>Persentase profit hari ini dapat anda lihat lewat pukul 16.00 WIB</div>";
		}
		else
		{
			$html .= "<div class='showMessInfo'>Profit Anda Hari ini Adalah ".$statusProfit.$profitToday."</div>";
		}
	}
}

?>
    <script type="text/javascript" src="administrator/components/com_invest/assets/js/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
	  
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Profit %'],
		  <?php for($i=0;$i<count($showDate);$i++) { ?>
          ['<?php echo $showDate[$i]; ?>',  <?php echo $persen_profit[$i]; ?>],
		  <?php } ?>
        ]);

		var typechart = "<?php echo $_GET['typechart']; ?>";
		
		if(typechart == "AreaChart")
		{
			  var options = {
			  title: 'Statistic Profit Investasi',
			  fontSize : '11',
			  hAxis: {title: 'Date',  titleTextStyle: {color: 'red'}}
			};
		
			var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
			chart.draw(data, options);
		}
		else if(typechart == "LineChart")
		{
			var options = {
			  title: 'Statistic Profit Investasi',
			  fontSize : '11'
			};

			var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
			chart.draw(data, options);
		}
		else if(typechart == "ColumnChart")
        {
			 var options = {
			  title: 'Statistic Profit Investasi',
			  fontSize : '11',
			  hAxis: {title: 'Date',  titleTextStyle: {color: 'red'}}
			};
			var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
			chart.draw(data, options);
		}
		else
		{
			  var options = {
			  title: 'Statistic Profit Investasi',
			  fontSize : '11',
			  hAxis: {title: 'Date',  titleTextStyle: {color: 'red'}}
			};
		
			var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
			chart.draw(data, options);
		}
		
      }
	  
	  var jQuery = jQuery.noConflict();
	  jQuery(document).ready(function()
	  {
			jQuery('#chart').change(function()
			{
				var typechart = jQuery(this).val();
				window.open("<?php echo JURI::Current(); ?>?view=myplan&task=statistic&id=<?php echo $_GET['id']; ?>&typechart="+typechart,'_parent');
			});
	  });
    </script>
	
	<?php 
	if($isShowDate > 0) { 
	$range_date = explode(";",$this->data['plan'][0]->range_date);
	$jumlah = count($range_date);
	$html .= '<div class="BoxProdDetails">
    Invest Product : '.$this->data['plan'][0]->plan.'<br />
    Invest Plan Name : '.$this->data['plan'][0]->nama.'<br />
    Invest Value : Rp. '.number_format($this->data['plan'][0]->investasi,0,',','.').'<br />
    Invest Period : '.$range_date[0]." to ".$range_date[$jumlah-1];
	
    $html .= '</div>
    <div id="chart_div"></div>
	<table width="100%" class="sumPlan">
	<tr>
    <th class="sumPlanAwal">Day</th>
    <th>Date</th>
    <th>Profit (%)</th>
    <th>Profit (+ Rp)</th>
	<th>Withdraw (- Rp)</th>
	<th>Investment <br />(Rp. '.number_format($this->data['plan'][0]->investasi,0,',','.').')</th>';
	for($i=0;$i<$total;$i++)
	{
	$withdrawTransaction = $helper->getDataByParam(array("date ","member_plan_id","status","type"),array("LIKE '%".$showDate[$i]."%'","='".$this->data['plan'][0]->id."'","='Success'","!='Deposit'"),"#__invest_transaction");
	$no = $i + 1;
	
	$akumulasi_profit[] = $persen_profit[$i];
	$total_profit = array_sum($akumulasi_profit);
	//protifakumulatif dikurangi dengan withdrawal profit untuk mengetahui member telah melakukan penarikan profit atau belum
	$profitAkumulatif = $this->data['plan'][0]->investasi*($total_profit/100);
	
	
	/*if($profitAkumulatif < $withdrawalProfit)
	{
		$profitAkumulatif = 0;
	}
	else
	{
		$profitAkumulatif = $profitAkumulatif - $withdrawalProfit;
	}*/
	
	$myprofit = $this->data['plan'][0]->investasi*($persen_profit[$i]/100);
	
	$total_investasi = $this->data['plan'][0]->investasi+$profitAkumulatif;
	//compare invest
	
	if($i >= 1)
	{
		$compare = $persen_profit[$i] - $persen_profit[$i - 1];
		if($persen_profit[$i] > $persen_profit[$i - 1])
		{
			$statusProfit = $imgUpProfit;
		}
		else
		{
			$statusProfit = $imgDownProfit;
		}
	}
	else
	{
		$statusProfit = $imgUpProfit;
	}
	$html .= "<tr>
	<td class='sumPlanAwal'>".$no."</td>
	<td>".$showDate[$i]."</td>
	<td>".$statusProfit.$persen_profit[$i]."%</td>
	<td>Rp. ".number_format($myprofit,0,',','.')."</td><td>";
	if(!empty($withdrawTransaction->nominal))
	{
		$html .= "Rp. ".number_format($withdrawTransaction->nominal,0,',','.');
	}
	else
	{
		$html .= "-";
	}
	$withdrawAkkumulatif[] = $withdrawTransaction->nominal;
	$myprofitakumulatif[] = $myprofit-$withdrawTransaction->nominal;
	$myInvest = $this->data['plan'][0]->investasi+array_sum($myprofitakumulatif);
	$html .= "</td><td>";
	if(!empty($withdrawTransaction->nominal))
	{
		$html .= $imgDownProfit;
	}
	$html .= "Rp. ".number_format($myInvest,0,',','.')."</td></tr>";
	}
	$totalByWithdraw = $total_investasi-$withdrawalProfit;
	$html .= "<tr bgcolor='#f3f3f3'><td colspan='3' align='right'>Total Invest (Rp. ".number_format($this->data['plan'][0]->investasi,0,',','.').") : </td><td>+ Rp. ".number_format($profitAkumulatif,0,',','.')."</td><td> - Rp. ".number_format(array_sum($withdrawAkkumulatif),0,',','.')."</td><td> = Rp. ".number_format($totalByWithdraw,0,',','.')."</td></tr></table>";
	}
	//privew 
	echo $html;
	?>
</div>
<script>
var jQuery = jQuery.noConflict();
jQuery(document).ready(function()
{
	jQuery('.boxFooterCopy').fadeOut();
	jQuery('.boxFooter').fadeOut();
	jQuery('#boxTop').fadeOut();
	jQuery('#boxMainMenu').fadeOut();
	jQuery('#boxPageKiri').fadeOut();
	jQuery('.COM_INV_STATMYPLAN').fadeIn();
});
</script>
<?php
//
?>