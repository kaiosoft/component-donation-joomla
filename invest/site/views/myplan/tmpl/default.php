<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted Access');
?>
<div class="COM_INV_MYPLAN">
<?php
if(empty($this->data['plan']))
{
	echo "Anda belum mempunyai investasi plan, Tambahkan <a href='".JURI::Base()."index.php?option=com_invest&view=plan' />disini</a>";
}
else
{
echo "<a href='".JURI::Base()."index.php/produk-investasi' class='addPlanInMemarea' />Add Plan</a>";
?>
<form action="index.php" method="post" name="adminForm">
<h2>Summary Plan Investasi Anda</h2>
<table width="100%" class="sumPlan">
	<tr>
		<th class='sumPlanAwal'>ID Plan</th>
		<th>Invest Plan</th>
		<th>Invest Value</th>
		<th>Start date</th>
		<th>Finish date</th>
		<th width="30%">Status</th>
	</tr>
<?php
	foreach($this->data['plan'] as $row)
	{
		$stat = JRoute::_('index.php?option=com_invest&view=myplan&task=statistic&id='.$row->id);
		$range_date = explode(";",$row->range_date);
		$jumlah = count($range_date);
		switch($row->status)
		{
			case "InActive":
			$status = "Inactive";
			break;
			
			case "Confirm":
			$status = "Konfirmasi Pembayaran anda telah terkirim, hingga kami menerima pembayaran anda maka plan ini bisa mulai berjalan";
			break;
			
			case "Active":
			$status = "Active";
			break;
			
			case "Withdraw":
			$status = "Permintaan anda untuk menarik dana sedang kami proses, jika penarikan bukan pada waktu yang tepat / ditentukan sesuai perjanjian maka anda dikenakan penalty";
			break;
			
			case "Complete":
			$status = "Plan ini telah selesai, anda bisa mencairkan dana investasi anda melauli form withdraw";
			break;
			
			default:
			$status = "Closed";
			break;
		}
		echo "<tr valign=top><td class='sumPlanAwal'>";
			if($row->status == "Active" || $row->status == "Complete") {
				echo "<a href=".$stat."><a href='".$stat."'>".$row->account."</a></a>";
			}
			else{
				echo $row->account;
			}
			
		echo"
		</td>
		<td> Name : ".$row->nama."<br/>Product : ".$row->plan."</td>
		<td>Rp. ".number_format($row->investasi,0,',','.')."</td>
		<td>".$range_date[0]."</td>
		<td>".$range_date[$jumlah-1]."</td>
		<td style='font-size:11px'>".$status;
		if($row->status == "Active" || $row->status == "Complete") {
			echo "<br /><input type='button' value='Withdraw' id='".$row->id."' class='getWithdraw' />";
		}
		echo "</td><td>
			</td></tr>";
	}
?>
</table>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="plan" />
<input type="hidden" name="boxchecked" value="" />
<input type="hidden" name="task" value="" />    
<?php echo JHTML::_( 'form.token' ); ?>
</form>
<div class="topPopup"></div>
<div class="backPopup"></div>
<script>
var jQuery = jQuery.noConflict();
jQuery(document).ready(function()
{
	jQuery('.getWithdraw').click(function()
	{
		jQuery('.topPopup').html('<img src="<?php echo JURI::Base(); ?>administrator/components/com_invest/assets/images/process.gif" />');
		jQuery('.topPopup').fadeIn();
		jQuery('.backPopup').fadeIn();
		
		var memberplan = jQuery(this).attr('id');
		
		jQuery.ajax({
		type:"POST",
		url:"<?php echo JURI::Base().'index.php';?>",
		data:{option:"com_invest",view:"myplan",task:"withdrawInvest",memberplan:memberplan},
		}).done(function(show)
		{
			jQuery('.topPopup').html(show);
			jQuery('#type').change(function()
			{
				var type = jQuery(this).val();
				var myprofit = jQuery("#myprofit").val();
				var myinvest = jQuery("#myinvest").val();
				
				if(type == "Withdraw Profit")
				{
					jQuery("#nominal").val(myprofit);
				}
				else
				{
					jQuery("#nominal").val(myinvest);
				}
			});
			jQuery('.withdrawButton').click(function()
			{
				var email = jQuery('#email').val();
				var nama = jQuery('#nama').val();
				var plan_id = jQuery('#plan_id').val();
				var type = jQuery('#type').val();
				var nominal = jQuery('#nominal').val();
				var myprofit = jQuery("#myprofit").val();
				var myinvest = jQuery("#myinvest").val();
				var member_bank_id = jQuery('#member_bank_id').val();
				
				if((email == "") || (nama == "") || (plan_id == "") || (type == "") || (member_bank_id == ""))
				{
					alert('nominal dan bank harus terisi');
				}
				else if(nominal == 0)
				{
					alert('anda belum mempunyai profit');
				}
				else if((type == "Withdraw Profit") && (nominal >  myprofit))
				{
					alert('Maaf, profit anda tidak cukup untuk melakukan penarikan profit sebesar '+nominal);
				}
				else if((type == "Withdraw Invest") && (nominal >  myinvest))
				{
					alert('Maaf, investasi anda tidak cukup untuk melakukan penarikan investasi sebesar '+nominal);
				}
				else
				{
					document.withdrawForm.submit();
				}
				
			});
			
		});
	});
	
	jQuery('.backPopup').click(function()
	{
		jQuery(this).fadeOut();
		jQuery('.topPopup').fadeOut();
	});
});
</script>
<?php } ?>
</div>