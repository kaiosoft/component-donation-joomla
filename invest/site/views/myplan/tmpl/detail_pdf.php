<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
//not allow direct access
//error_reporting(0);
defined('_JEXEC') or die('Restricted Access');

$helper = new investHelper;

$persen_profit = explode(";",$this->data['plan'][0]->persen_profit);
$range_date = explode(";",$this->data['plan'][0]->range_date);

//get plan
$plan = $helper->getDataByParam("id","='".$this->data['plan'][0]->plan_id."'","#__invest_plan");
$profitInvest = $helper->profitInvest($this->data['plan'][0]->investasi,$this->data['plan'][0]->persen_profit);
$withdrawalProfit = $profitInvest - $this->data['plan'][0]->profit;

//show range date by date
$showDate = array();
$now = date("Y-m-d");
$jam = date("H");
foreach($range_date as $row)
{
	if($row < $now)
	{
		$showDate[] = $row;
	}
	else if(($row == $now) && ($jam >= 16))
	{
		$showDate[] = $row;
	}
	else
	{
		break;
	}
}

//count total range_date by date
$total = count($showDate);

//get profit
$profitToday = $persen_profit[$total-1];
$profitYesterday = $persen_profit[$total-2];
$compare = $profitToday - $profitYesterday;

$imgUpProfit = "<img src='".JURI::Base()."administrator/components/com_invest/assets/images/increase.png' height='16' />";
$imgDownProfit = "<img src='".JURI::Base()."administrator/components/com_invest/assets/images/decrease.png' height='16' />";
$imgUpProfit = "<img src='".JURI::Base()."administrator/components/com_invest/assets/images/increase.png' height='16' />";
$imgDownProfit = "<img src='".JURI::Base()."administrator/components/com_invest/assets/images/decrease.png' height='16' />";

//get range date
if(empty($showDate))
{
	$isShowDate = 0;
}
else
{
	if($this->data['plan'][0]->status == "Active")
	{
		$isShowDate = 1;
	}
}

			require_once('administrator/components/com_invest/assets/fpdf/html2pdf.php');
			$pdf = new PDF_HTML();
			$pdf->AddPage('L');
			$pdf->SetFont('Arial','B',11);
			$pdf->image('administrator/components/com_invest/assets/images/icon-64-plan.png',25,5,20,20);
			$pdf->SetFont('Arial','B',18);
			//Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
			$pdf->cell(35);
			$pdf->Cell(0,5,'Naga Dana',0,1);
			$pdf->SetFont('Arial','B',14);
			$pdf->cell(35);
			$pdf->cell(0,10,'Investasi Aman Yang Menguntungkan',0,1);
			$pdf->cell(0,10,'',0,1);
			$pdf->SetFont('Arial','B',13);
			$pdf->SetFillColor(235,235,235);
			$pdf->cell(80);
			$pdf->cell(0,10,"Statistik Investasi ".$this->data['plan'][0]->plan." - ".$this->data['plan'][0]->nama." (".$this->data['plan'][0]->account.")",0,1);
			$pdf->cell(0,10,'',0,1);
			$pdf->SetFont('Arial','B',12);
			$pdf->SetFillColor(235,235,235);
			$pdf->cell(15);
			$pdf->cell(20,20,'Day',1,0,'C',1);
			$pdf->cell(30,20,'Date',1,0,'C',1);
			$pdf->cell(30,20,'Profit (%)',1,0,'C',1);
			$pdf->cell(30,20,'Profit (+ Rp)',1,0,'C',1);
			$pdf->cell(40,20,'Withdraw (- Rp)',1,0,'C',1);
			$pdf->cell(60,20,'Investment (Rp. '.number_format($this->data['plan'][0]->investasi,0,',','.').')',1,1,'C',1);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetFont('Arial','',10);
			for($i=0;$i<$total;$i++)
			{
			$withdrawTransaction = $helper->getDataByParam(array("date ","member_plan_id","status","type"),array("LIKE '%".$showDate[$i]."%'","='".$this->data['plan'][0]->id."'","='Success'","!='Deposit'"),"#__invest_transaction");
			$no = $i + 1;
			
			$akumulasi_profit[] = $persen_profit[$i];
			$total_profit = array_sum($akumulasi_profit);
			//protifakumulatif dikurangi dengan withdrawal profit untuk mengetahui member telah melakukan penarikan profit atau belum
			$profitAkumulatif = $this->data['plan'][0]->investasi*($total_profit/100);
			
			$myprofit = $this->data['plan'][0]->investasi*($persen_profit[$i]/100);
			
			$total_investasi = $this->data['plan'][0]->investasi+$profitAkumulatif;
			
			if(!empty($withdrawTransaction->nominal))
			{
				$mywithdraw = "Rp. ".number_format($withdrawTransaction->nominal,0,',','.');
				
			}
			else
			{
				$mywithdraw = " - ";
			}
			$withdrawAkkumulatif[] = $withdrawTransaction->nominal;
			$myprofitakumulatif[] = $myprofit-$withdrawTransaction->nominal;
			$myInvest = $this->data['plan'][0]->investasi+array_sum($myprofitakumulatif);
			$pdf->cell(15);
			$pdf->cell(20,20,$no,1,0,'C',1);
			$pdf->cell(30,20,$showDate[$i],1,0,'C',1);
			$pdf->cell(30,20,$persen_profit[$i],1,0,'C',1);
			$pdf->cell(30,20,'Rp. '.number_format($myprofit,0,',','.'),1,0,'C',1);
			$pdf->cell(40,20,$mywithdraw,1,0,'C',1);
			$pdf->cell(60,20,'Rp. '.number_format($myInvest,0,',','.'),1,1,'C',1);
			}
			
			$totalByWithdraw = $total_investasi-$withdrawalProfit;
			$pdf->cell(15);
			$pdf->cell(80,20,'Total Invest Rp. '.number_format($this->data['plan'][0]->investasi,0,',','.'),1,0,'C',1);
			$pdf->cell(30,20,'+ Rp. '.number_format($profitAkumulatif,0,',','.'),1,0,'C',1);
			$pdf->cell(40,20,'- Rp. '.number_format(array_sum($withdrawAkkumulatif)),1,0,'C',1);
			$pdf->cell(60,20,'= Rp. '.number_format($totalByWithdraw,0,',','.'),1,1,'C',1);					

			$pdf->Output();
		
		break;
		
		/*$totalByWithdraw = $total_investasi-$withdrawalProfit;
		$pdf->cell(15);
		$pdf->cell(20,20,"",1,0,'R',1);
		$pdf->cell(20,20,"Rp. ".number_format($profitAkumulatif,0,',','.'),1,0,'R',1);
		//$pdf->cell(20,20,"Rp. ".number_format(array_sum($withdrawAkkumulatif),0,',','.'),1,0,'R',1);
		$pdf->cell(50,20,"Rp. ".number_format($totalByWithdraw,0,',','.'),1,1,'C',1);*/

?>