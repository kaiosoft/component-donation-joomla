<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
?>
<div class="COM_INV_EDITPROFIL">
<?php
$user =& JFactory::getUser();
$helper = new investHelper;

require_once('administrator/components/com_invest/helpers/regional.php');
$regional = new regionalHelper;

$data = array();
$data['country'] = $this->data['profil']->negara;
$data['state'] = $this->data['profil']->provinsi;
$data['city'] = $this->data['profil']->kota;

$regionalKey = $regional->getKeyRegional($data);

$listCountry = $regional->getCountry();
$listState = $regional->getState($regionalKey['country']);
$listCity = $regional->getCity($regionalKey['country'],$regionalKey['state']);

$typelist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_OPTION_SELECT_TYPE' ), 'value', 'text' );
$typelist[]	= JHTML::_('select.option',  'Personal', JText::_( 'Personal' ), 'value', 'text' );
$typelist[]	= JHTML::_('select.option',  'Lembaga', JText::_( 'Organisasi / Lembaga' ), 'value', 'text' );
$lists['type'] = JHTML::_('select.genericlist', $typelist, 'type', 'onchange="" class="inputbox" size="1"', 'value', 'text', $this->data['profil']->type );

$identitaslist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_OPTION_SELECT_IDENTITAS' ), 'value', 'text' );
$identitaslist[]	= JHTML::_('select.option',  'KTP', JText::_( 'KTP' ), 'value', 'text' );
$identitaslist[]	= JHTML::_('select.option',  'SIM', JText::_( 'SIM' ), 'value', 'text' );
$identitaslist[]	= JHTML::_('select.option',  'PASSPORT', JText::_( 'PASSPORT' ), 'value', 'text' );
$identitaslist[]	= JHTML::_('select.option',  'SIUP', JText::_( 'SIUP' ), 'value', 'text' );
$lists['identitas'] = JHTML::_('select.genericlist', $identitaslist, 'identitas', 'onchange="" class="inputbox" size="1"', 'value', 'text', $this->data['profil']->identitas );

$countrylist[]	= JHTML::_('select.option',  '', JText::_( 'Pilih Negara' ), 'value', 'text' );
$countrylist	= array_merge($countrylist, $listCountry);
$lists['country'] = JHTML::_('select.genericlist', $countrylist, 'country', 'class="inputbox" size="1"', 'value', 'text' , $regionalKey['country']  );

$statelist		= $listState;
$lists['state'] = JHTML::_('select.genericlist', $statelist, 'state', 'class="inputbox" size="1"', 'value', 'text', $regionalKey['state'] );

$citylist		= $listCity;
$lists['city'] 	= JHTML::_('select.genericlist', $citylist, 'city', 'class="inputbox" size="1"', 'value', 'text', $regionalKey['city'] );

if(!empty($this->data['listBank']))
{
$banklist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_OPTION_SELECT_BANK' ), 'id', 'bank' );
}
else
{
$banklist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_OPTION_SELECT_BANK_EMPTY' ), 'id', 'bank' );
}
$banklist = array_merge($banklist,$this->data['listBank']);



?>
<script type="text/javascript" src="administrator/components/com_invest/assets/js/addfield/duplicate.js"></script>
<form name="investProfil" method="POST" action="index.php?option=com_invest&view=profil&task=saveProfil" enctype="multipart/form-data">

<h2>Data Diri</h2> 	 	
<p><?php echo $lists['type']; ?></p>
<p><input type="text" name="nama" id="nama" placeHolder="Nama" value="<?php echo $this->data['profil']->nama; ?>" /></p>
<p><input type="text" name="penanggung_jawab" id="penanggung_jawab" placeHolder="Penanggung Jawab" value="<?php echo $this->data['profil']->penanggung_jawab; ?>" /></p>
<p><input type="text" name="hp" id="hp" placeHolder="Handphone" value="<?php echo $this->data['profil']->hp; ?>" /></p>
<p><textarea name="alamat" id="alamat" placeHolder="alamat"><?php echo $this->data['profil']->alamat; ?></textarea></p>
<p><?php echo $lists['country']; ?></p>
<p><div class="showState"><?php if(count($listState) > 1) { echo $lists['state']; } else { echo "<input type='text' name='state' id='provinsi' placeHolder='Provinsi' value='".$regionalKey['state']."' />"; } ?></div></p>
<p><div class="showCity"><?php if(count($listCity) > 1) { echo $lists['city']; } else { echo "<input type='text' name='city' id='kota' placeHolder='Kota' value='".$regionalKey['city']."' />"; } ?></div></p>
<p><input type="text" name="kode_pos" id="kode_pos" placeHolder="kode pos" value="<?php echo $this->data['profil']->kode_pos; ?>" /></p>
<p>&nbsp;</p>
<h2>Tanda Identitas Diri</h2>
<p><?php echo $lists['identitas']; ?></p>
<p><input type="text" name="no_identitas" id="no_identitas" placeHolder="no_identitas" value="<?php echo $this->data['profil']->no_identitas; ?>" /></p>
<?php if(empty($this->data['profil']->legalitas)) { ?>
<input type="file" name="legalitas" placeHolder="Legalitas Member" /><br />
*Jika anda merupakan personal silakan upload (ktp / sim / passport anda);<br />
*Jika anda merupakan organisasi / Lembaga silakan upload (SIUP / Surat keterangan bahwa anda sebuah badan lembaga / organisasi);
<?php }
else
{
	echo "File telah anda upload, sedang menunggu persetujuan admin";
	?><input type="hidden" name="legalitas" value="<?php echo $this->data['profil']->legalitas; ?>" /><?php
}
 ?>
<p>&nbsp;</p>
<h2>Data Account Bank</h2>
<?php 
  $bankMember = $helper->getDataByParam("member_id","='".$this->data['profil']->id."'","#__invest_member_bank","array");
  if(count($bankMember) >= 1)
  {
	  for($i=0;$i<count($bankMember);$i++)
	  {
	  $lists['bank']	= JHTML::_('select.genericlist', $banklist, 'member_bank_id[]', 'onchange="" class="inputbox" size="1"', 'id', 'bank', $bankMember[$i]->bank_id );
	  ?>
		<p class="clone2"><?php echo $lists['bank']; ?>|<input name="no_rekening[]" type="text" id="no_rekening" placeHolder="<?php echo JText::_('COM_INVEST_OPTION_REKENING'); ?>" value="<?php echo $bankMember[$i]->rekening; ?>" />|<input name="an[]" placeHolder="<?php echo JText::_('COM_INVEST_OPTION_AN'); ?>" type="text" id="an" value="<?php echo $bankMember[$i]->an; ?>"/><a href="#" class="add" rel=".clone2">add more</a><?php if(!empty($bankMember[$i]->id)){ ?>| <a href="#" id="<?php echo $bankMember[$i]->id; ?>" class="removeBank">remove</a><?php } ?></p>
		  <input type="hidden" name="idBank[]" value="<?php echo $bankMember[$i]->id; ?>" />
		<?php 
		}
	}
	else
	{
	 $lists['bank']	= JHTML::_('select.genericlist', $banklist, 'member_bank_id[]', 'onchange="" class="inputbox" size="1"', 'id', 'bank', $bankMember[$i]->bank_id );
	?>
	 <p class="clone2"><?php echo $lists['bank']; ?>|<input name="no_rekening[]" type="text" id="no_rekening" placeHolder="<?php echo JText::_('COM_INVEST_OPTION_REKENING'); ?>" value="<?php echo $bankMember[$i]->rekening; ?>"/>|<input name="an[]" placeHolder="<?php echo JText::_('COM_INVEST_OPTION_AN'); ?>" type="text" id="an" value="<?php echo $bankMember[$i]->an; ?>"/><a href="#" class="add" rel=".clone2">add more</a>| <a href="#" onclick="jQuery(this).parent().fadeIn(function(){ jQuery(this).remove()  }); return false">remove</a></p>
	<?php } ?>
<input type="hidden" name="id" value="<?php echo $this->data['profil']->id; ?>" />
<p><input type="button" class="saveProfil" value="<?php echo JText::_('SAVE_DATA_PROFIL'); ?>" /></p>
</form>
<div class="topPopup"></div>
<div class="backPopup"></div>
<script>
var jQuery = jQuery.noConflict();
jQuery(document).ready(function()
{
	jQuery(function(){
		var removeLink = ' <a class="remove" href="#" onclick="jQuery(this).parent().fadeIn(function(){ jQuery(this).remove() }); return false">remove</a>';
		jQuery('a.add').relCopy({ /*append: removeLink */});        
	});
	
	jQuery('#type').click(function()
	{
		var type = jQuery(this).val();
		if(type == "Lembaga")
		{
			jQuery('#nama').attr('placeHolder','Nama Perusahaan / Organisasi');
			jQuery('#penanggung_jawab').fadeIn();
			jQuery('#hp').attr('placeHolder','Handphone Penanggung jawab');
		}
		else
		{
			jQuery('#nama').attr('placeHolder','Nama');
			jQuery('#penanggung_jawab').fadeOut();
			jQuery('#hp').attr('placeHolder','Handphone');
		}
	});
	
	jQuery('.saveProfil').click(function()
	{
		var type = jQuery('#type').val();
		var nama = jQuery('#nama').val();
		var hp = jQuery('#hp').val();
		var penanggung_jawab = jQuery('#penanggung_jawab').val();
		
		if((type == "") || (nama == "") || (hp == ""))
		{
			alert('semua field harus terisi');
		}
		else if((type == "Lembaga") && (penanggung_jawab == ""))
		{
			alert('semua field harus terisi');
		}
		else
		{
			/*jQuery.ajax({
			type:"POST",
			url:"<?php echo JURI::Base().'index.php'; ?>",
			data:{option:"com_invest",view:"profil",task:"saveProfil",type:type,nama:nama,hp:hp,penanggung_jawab:penanggung_jawab},
			}).done(function(show)
			{
				jQuery('.topPopup').html(show);
				jQuery('.topPopup').fadeIn();
				jQuery('.backPopup').fadeIn();
				window.open('index.php?option=com_invest&view=profil','_self');
			});*/
			//jQuery(document).investProfil.submit();
			document.investProfil.submit();
		}
	});
	
	jQuery('.backPopup').click(function()
	{
		jQuery(this).fadeOut();
		jQuery('.topPopup').fadeOut();
	});
	
	jQuery('#country').change(function()
	{
		var country = jQuery(this).val();
		jQuery('.showState').append('<img src="<?php echo JURI::Base(); ?>administrator/components/com_invest/assets/images/mini_process.gif" />');
		jQuery.ajax({
		type : "POST",
		url : "<?php JURI::Base().'index.php'; ?>",
		data : {option:"com_invest",view:"plan",task:"getRegional",regional:"state",country:country},
		}).done(function(show)
		{
			jQuery(".showState").html(show);
			jQuery('#state').change(function()
			{
				jQuery('.showCity').append('<img src="<?php echo JURI::Base(); ?>administrator/components/com_invest/assets/images/mini_process.gif" />');
				var state = jQuery(this).val();
				jQuery.ajax({
				type : "POST",
				url : "<?php JURI::Base().'index.php'; ?>",
				data : {option:"com_invest",view:"plan",task:"getRegional",regional:"city",country:country,state:state},
				}).done(function(show)
				{
					jQuery(".showCity").html(show);
				});
			});
		});
	});
	
	jQuery('.removeBank').click(function()
	{
		var id = jQuery(this).attr('id');
		jQuery.ajax({
		type : "POST",
		url : "<?php JURI::Base().'index.php'; ?>",
		data : {option:"com_invest",view:"plan",task:"removeBank",id:id},
		}).done(function(show)
		{
			
			alert(show);
		});
		jQuery(this).parent().fadeIn(function(){ jQuery(this).remove()  });
		
		return false;
	});
	
});
</script>
</div>