<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
$msgProfil = JRequest::getVar('msgProfil');

defined('_JEXEC') or die;
?>
<div class="COM_INV_PROFIL">
<?php if(!empty($msgProfil)){ echo "<div class='noteProfil'>".$msgProfil."</div>"; } ?>
	<div class="boxProfil">
		<a href="index.php?option=com_invest&view=profil&task=editprofil"><img src="administrator/components/com_invest/assets/images/icon-64-member.png" /><br />Profil</a>
	</div>
	<div class="boxProfil">
		<a href="index.php?option=com_invest&view=myplan"><img src="administrator/components/com_invest/assets/images/icon-64-plan.png" /><br />My Plan</a>
	</div>
	<div class="boxProfil">
		<a href="index.php?option=com_invest&view=transaction&task=deposit"><img src="administrator/components/com_invest/assets/images/icon-64-deposit.png" /><br />Confirmation</a>
	</div>
	<div class="boxProfil">
		<a href="index.php?option=com_invest&view=transaction&task=withdraw"><img src="administrator/components/com_invest/assets/images/icon-64-withdraw.png" /><br />Withdraw</a>
	</div>
	<div class="clear"></div>
</div>