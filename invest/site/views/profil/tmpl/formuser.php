<?php
defined('_JEXEC') or die ('Restricted Access');
?>
<div class="COM_INV_FORMUSER">
<?php
$typelist[]	= JHTML::_('select.option',  '', JText::_( 'COM_INVEST_OPTION_SELECT_TYPE' ), 'value', 'text' );
$typelist[]	= JHTML::_('select.option',  'Personal', JText::_( 'Personal' ), 'value', 'text' );
$typelist[]	= JHTML::_('select.option',  'Lembaga', JText::_( 'Organisasi / Lembaga' ), 'value', 'text' );
$lists['type'] = JHTML::_('select.genericlist', $typelist, 'type_user', 'onchange="" class="inputbox" size="1"', 'value', 'text' );

if(JRequest::getVar('formuser') == "login")
{
?>
<form method="POST" name="investLogin" action="index.php?option=com_invest&view=plan&task=doLogin">
<h2>Form Login</h2>
<hr>
<input type="text" name="username" id="username" placeHolder="Username" /><br />
<input type="password" name="password" id="password" placeHolder="Password" /><br />
<input type="hidden" name="plan_id" id="plan_id" value="<?php echo JRequest::getVar('plan_id'); ?>" />
<p><input type="hidden" name="namaplan" id="namaplan" value="<?php echo JRequest::getVar('namaplan'); ?>" /></p>
<input type="submit" value="login" />
</form>
<?php } else { ?>
<form method="POST" name="investRegister" action="index.php?option=com_invest&view=plan&task=doRegister">
<h2>Form Register</h2>
<hr>
<p><?php echo $lists['type']; ?></p>
<p><input type="text" name="email" id="email" placeHolder="Email" value="<?php echo JRequest::getVar('email'); ?>" /></p>
<p><input type="text" name="nama" id="nama" placeHolder="Nama" /></p>
<p><input type="text" name="penanggung_jawab" id="penanggung_jawab" placeHolder="Penanggung Jawab" /></p>
<p><input type="text" name="hp" id="hp" placeHolder="Handphone" /></p>
<p><input type="hidden" name="plan_id" id="plan_id" value="<?php echo JRequest::getVar('plan_id'); ?>" /></p>
<p><input type="hidden" name="namaplan" id="namaplan" value="<?php echo JRequest::getVar('namaplan'); ?>" /></p>
<p><input type="submit" value="Register" /></p>
</form>
<?php 
}
?>
<script>
var jQuery = jQuery.noConflict();
jQuery(document).ready(function()
{
	jQuery('#type').click(function()
	{
		var type = jQuery(this).val();
		
		if(type == "Lembaga")
		{
			jQuery('#nama').attr('placeHolder','Nama Perusahaan / Organisasi');
			jQuery('#penanggung_jawab').fadeIn();
			jQuery('#hp').attr('placeHolder','Handphone Penanggung jawab');
			alert('lembaga');
		}
		else
		{
			jQuery('#nama').attr('placeHolder','Nama');
			jQuery('#penanggung_jawab').fadeOut();
			jQuery('#hp').attr('placeHolder','Handphone');
			alert('personal');
		}
	});
});
</script>
</div>