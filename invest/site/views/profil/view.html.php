<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewProfil extends JView
{

	function display($tpl = null)
	{
		$app =& JFactory::getApplication();
		$task = JRequest::getVar('task');
		$user =& JFactory::getUser();
		$helper = new investHelper;
		
		if(empty($user->id))
		{
			$app->redirect('index.php?option=com_invest&view=plan&task=login','Silakan login terlebih dahulu','error');
		}

	    $modelProfil =& JModel::getInstance('profil','InvestModel');
		
		$limit		= $app->getUserStateFromRequest( 'global.list.limit', 'limit', $app->getCfg('list_limit'), 'int' );
		$limitstart = $app->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
			
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
		
		$profil = $modelProfil->getProfil($user->id);

		if(empty($task))
		{
		//cek complete profil
		$isComplete = $modelProfil->cekCompleteProfil($profil);
			if(!empty($isComplete))
			{
				foreach($isComplete as $row)
				{
					if($row == "inComplete")
					{
						$msg = "Profil anda belum lengkap silakan lengkapi data diri anda";
						$app->redirect('index.php?option=com_invest&view=profil&task=editprofil',$msg,'error');
					}
					else if($row == "inActive")
					{
						$msg = "Keanggotaan anda belum di aktifkan";
					}
					else
					{
						$msg = "Anda belum menambahkan data bank";
					}
				}
				
				JRequest::setVar('msgProfil',$msg);
			}
		}
		
		$listBank = $helper->getDataByParam("","","#__invest_bank","array");
		
		//prepare data
		$data = array();
		$data['profil'] = $profil;
		$data['isComplete'] = $isComplete;
		$data['listBank'] = $listBank;
		$data['listPlan'] = $listPlan;
		$data['pageNav'] = $pageNav;
		
		
		
		$this->assignRef('data',$data);
		parent::display($tpl);
		
	}
		
}	

?>
