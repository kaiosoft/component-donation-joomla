<div class="COM_INV_FORMUSER">
<?php
defined('_JEXEC') or die ('Restricted Access');
$helper = new investHelper;
$dataPlan = $helper->getDataByParam("id","='".JRequest::getVar('plan_id')."'","#__invest_plan");

require_once('administrator/components/com_invest/helpers/regional.php');
$regional = new regionalHelper;
$listCountry = $regional->getCountry();
$listState = $regional->getState();
$listCity = $regional->getCity();

$typelist[]	= JHTML::_('select.option',  'Personal', JText::_( 'Personal' ), 'value', 'text' );
$typelist[]	= JHTML::_('select.option',  'Lembaga', JText::_( 'Organisasi / Lembaga' ), 'value', 'text' );
$lists['type'] = JHTML::_('select.genericlist', $typelist, 'type', 'onchange="" class="inputbox" size="1"', 'value', 'text' );

$countrylist[]	= JHTML::_('select.option',  '', JText::_( 'Pilih Negara' ), 'value', 'text' );
$countrylist	= array_merge($countrylist, $listCountry);
$lists['country'] = JHTML::_('select.genericlist', $countrylist, 'country', 'class="inputbox" size="1"', 'value', 'text' );

$statelist		= $listState;
$lists['state'] = JHTML::_('select.genericlist', $statelist, 'state', 'class="inputbox" size="1"', 'value', 'text' );

$citylist		= $listCity;
$lists['city'] 	= JHTML::_('select.genericlist', $citylist, 'city', 'class="inputbox" size="1"', 'value', 'text' );

if(JRequest::getVar('formuser') == "login")
{
?>
<form method="POST" name="investLogin" action="index.php?option=com_invest&view=plan&task=doLogin">
<h2>Form Login</h2>
<hr>
<input type="text" name="username" id="username" placeHolder="Username" /><br />
<input type="password" name="password" id="password" placeHolder="Password" /><br />
<input type="hidden" name="plan_id" id="plan_id" value="<?php echo JRequest::getVar('plan_id'); ?>" />
<p><input type="hidden" name="namaplan" id="namaplan" value="<?php echo JRequest::getVar('namaplan'); ?>" /></p>
<p><input type="hidden" name="investasi" id="investasi" value="<?php echo JRequest::getVar('investasi'); ?>" /></p>
<input type="submit" value="login" />
</form>
<?php } else { ?>
<form method="POST" name="investRegister" action="index.php?option=com_invest&view=plan&task=doRegister">
<div class="formOrderPlan">
<h2>Data Plan Investasi Anda.!</h2>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<?php echo $dataPlan->plan; ?>
   	</span>
    <span class="formOrderPlanDesc">Produk Investasi Pilihan Anda.!</span>
    <div class="clearFloat"></div>
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<?php echo JRequest::getVar('namaplan'); ?>
    </span>
    <span class="formOrderPlanDesc">Nama Plan Produk Investasi yang anda tentukan</span>
    <div class="clearFloat"></div>
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<?php echo "Rp. ".number_format(JRequest::getVar('investasi'),0,',','.'); ?>
    </span>
    <span class="formOrderPlanDesc">Nilai Investasi Anda</span>
    <div class="clearFloat"></div>
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<?php echo JRequest::getVar('email'); ?>
    </span>
    <span class="formOrderPlanDesc">Apakah ini benar email Anda ? dan apakah email tersebut masih aktif.</span>
    <div class="clearFloat"></div>
</div>
</div>
<h2>Mohon Lengkapi Data Anda.!</h2>
<div class="formOrderPlan">
<div class="investAddPlanBox">
<span class="formOrderPlanInput">
	<?php echo $lists['type']; ?>
</span>
    <span class="formOrderPlanDesc">Silahkan pilih, Apakah Investasi ini atas nama Diri Anda Pribadi/Personal, atau atas nama Perusahaan, atau atas nama Lembaga atau Organisasi.</span>
    <div class="clearFloat"></div>
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<input type="text" name="nama" id="nama" placeHolder="Nama Lengkap Anda" />
    </span>
    <span class="formOrderPlanDesc">Silahkan Masukkan Nama Lengkap anda sesuai dengan kartu identitas diri (KTP/SIM/PASPORT) yang masih berlaku.</span>
    <div class="clearFloat"></div>
</div>
<div id="penanggung_jawab2" class="investAddPlanBox">
	<span id="penanggung_jawab2" class="formOrderPlanInput">
	<input type="text" name="penanggung_jawab" id="penanggung_jawab" placeHolder=" Nama Penanggung Jawab" />
    </span>
    <span id="penanggung_jawab2" class="formOrderPlanDesc">Silahkan Masukkan Nama Lengkap anda sesuai dengan kartu identitas diri (KTP/SIM/PASPORT) yang masih berlaku.</span>
    <div class="clearFloat"></div>    
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<input type="text" name="hp" id="hp" placeHolder="Phone/Mobile" />
    </span>
    <span class="formOrderPlanDesc">Masukkan Nomor telepon anda yang dapat kami hubungi.</span>
    <div class="clearFloat"></div>
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<textarea name="alamat" id="alamat" placeHolder="Alamat" /></textarea>
    </span>
    <span class="formOrderPlanDesc">Masukkan Alamat anda.</span>
    <div class="clearFloat"></div>
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<?php echo $lists['country']; ?>
    </span>
    <span class="formOrderPlanDesc">Pilih Negara.</span>
    <div class="clearFloat"></div>
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<div class="showState"><?php echo $lists['state']; ?></div>
    </span>
    <span class="formOrderPlanDesc">Masukkan Provinsi tempat anda tinggal.</span>
    <div class="clearFloat"></div>
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<div class="showCity"><?php echo $lists['city']; ?></div>
    </span>
    <span class="formOrderPlanDesc">Masukkan Kota tempat anda tinggal.</span>
    <div class="clearFloat"></div>
</div>
<div class="investAddPlanBox">
	<span class="formOrderPlanInput">
	<input type="text" name="kode_pos" id="kode_pos" placeHolder="Kode Pos" />
    </span>
    <span class="formOrderPlanDesc">Masukkan Kota tempat anda tinggal.</span>
    <div class="clearFloat"></div>
</div>
	<input type="hidden" name="plan_id" id="plan_id" value="<?php echo JRequest::getVar('plan_id'); ?>" />
	<input type="hidden" name="namaplan" id="namaplan" value="<?php echo JRequest::getVar('namaplan'); ?>" />
	<input type="hidden" name="investasi" id="investasi" value="<?php echo JRequest::getVar('investasi'); ?>" />
	<input type="hidden" name="email" id="email" value="<?php echo JRequest::getVar('email'); ?>" />
	<div class="clearFloat"></div>
	<span class="formOrderPlanInput">
    <input type="button" class="doRegister" value="Saya akan Registrasi dan Order Plan ini" />
    </span>
    <span class="formOrderPlanDesc">Setelah Anda melakukan Registrasi, secara otomatis anda akan masuk kedalam member Area kami. User Account  akan kami kirimkan ke alamat Email Anda.</span>
    <div class="clearFloat"></div>
    </span>
	</div>
</div>
</form>
<?php 
}
?>
<script>
var jQuery = jQuery.noConflict();
jQuery(document).ready(function()
{
	jQuery('#penanggung_jawab, #penanggung_jawab2').css('display','none');
	jQuery('#type').change(function()
	{
		var type = jQuery(this).val();
		if(type == "Lembaga")
		{
			jQuery('#nama').attr('placeHolder','Nama Perusahaan / Organisasi');
			jQuery('#penanggung_jawab, #penanggung_jawab2').fadeIn();
			jQuery('#hp').attr('placeHolder','Handphone Penanggung jawab');
		}
		else
		{
			jQuery('#nama').attr('placeHolder','Nama');
			jQuery('#penanggung_jawab, #penanggung_jawab2').fadeOut();
			jQuery('#hp').attr('placeHolder','Handphone');
		}
	});
	
	
	jQuery('#country').change(function()
	{
		var country = jQuery(this).val();
		jQuery('.showState').append('<img src="<?php echo JURI::Base(); ?>administrator/components/com_invest/assets/images/mini_process.gif" />');
		jQuery.ajax({
		type : "POST",
		url : "<?php JURI::Base().'index.php'; ?>",
		data : {option:"com_invest",view:"plan",task:"getRegional",regional:"state",country:country},
		}).done(function(show)
		{
			jQuery(".showState").html(show);
			jQuery('#state').change(function()
			{
				jQuery('.showCity').append('<img src="<?php echo JURI::Base(); ?>administrator/components/com_invest/assets/images/mini_process.gif" />');
				var state = jQuery(this).val();
				jQuery.ajax({
				type : "POST",
				url : "<?php JURI::Base().'index.php'; ?>",
				data : {option:"com_invest",view:"plan",task:"getRegional",regional:"city",country:country,state:state},
				}).done(function(show)
				{
					jQuery(".showCity").html(show);
				});
			});
		});
	});
	
	jQuery('.doRegister').click(function()
	{
		var plan_id = jQuery("#plan_id").val();
		var namaplan = jQuery("#namaplan").val();
		var investasi = jQuery("#investasi").val();
		var email = jQuery("#email").val();
		var type = jQuery("#type").val();
		var nama = jQuery("#nama").val();
		var penanggung_jawab = jQuery("#penanggung_jawab").val();
		var hp = jQuery("#hp").val();
		var alamat = jQuery("#alamat").val();
		var negara = jQuery("#country").val();
		var provinsi = jQuery("#state").val();
		var kota = jQuery("#city").val();
		var kode_pos = jQuery("#kode_pos").val();
		
		if((plan_id == "") || (namaplan == "") || (investasi == "") || (email == "") || (type == "") || (nama == "") || (hp == "") || (alamat == "") || (negara == "") || (provinsi == "") || (kota == "") || (kode_pos == ""))
		{
			alert('semua fields harus terisi');
		}
		else
		{
			document.investRegister.submit();
		}
	});
});
</script>
</div>