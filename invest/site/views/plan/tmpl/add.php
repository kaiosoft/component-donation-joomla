<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

//error_reporting(0);
defined('_JEXEC') or die;
$user =& JFactory::getUser();
?>
<div class="DIV_INV_ORDERPLAN">
<?php echo "<h2>Produck Investasi ".$this->data['plan'][0]->plan."</h2>"; ?>
<div class="formOrderPlan">
    <form name="investAddPlan" method="POST" action="index.php?option=com_invest&view=plan&task=savePlan" >
    <?php if(empty($user->id)) { ?>
    <div class="investAddPlanBox">
    	<span class="formOrderPlanInput"><input type="text" class="inputbox" name="email" id="email" placeHolder="Email" /></span>
        <span class="formOrderPlanDesc">Masukkan email Anda yang masih aktif, karena semua data akan kami kirimkan ke email Anda.</span>
        <div class="clearFloat"></div>
    </div>
    <?php } ?>
    <div class="investAddPlanBox">
    	<span class="formOrderPlanInput"><input type="text" class="inputbox" name="nama" id="nama" placeHolder="Nama Plan Investasi" /></span>
        <span class="formOrderPlanDesc">Anda dapat memberi nama untuk setiap Plan Investasi sesuai keinginan anda agar mudah di ingat. ex : My Love.</span><div class="clearFloat"></div>
    </div>
     <div class="investAddPlanBox">
     	<span class="formOrderPlanInput"><input type="text" class="inputbox" name="investasi" id="investasi" tag="<?php echo $this->data['plan'][0]->investasi; ?>" placeHolder="Minimal nilai Investasi Rp. <?php echo number_format($this->data['plan'][0]->investasi,0,',','.'); ?>" /></span>
         <span class="formOrderPlanDesc">Untuk Produk Investasi <strong><?php echo $this->data['plan'][0]->plan; ?></strong> Minimal nilai Investasi adalah <strong>Rp.<?php echo number_format($this->data['plan'][0]->investasi,0,',','.'); ?></strong>. Masukkan nominal Investasi tanpa disertai tanda titik. </span><div class="clearFloat"></div>
     </div>
    <!-- <input type="checkbox" name="aggree" id="aggree" value="1" />Saya mengerti, dan menyetujui persyaratan yang diberikan<br /> -->
    <input type="hidden" name="plan_id" id="plan_id" value="<?php echo $this->data['plan'][0]->id; ?>" />
    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user->id; ?>" />
     <div><input type="button" value="Saya Order Produk Investasi ini" class="doOrder" /></div>
    </form>
</div>
<script>
var jQuery = jQuery.noConflict();
jQuery(document).ready(function()
{
	jQuery('.doOrder').click(function()
	{
	
		var planId = jQuery('#plan_id').val();
		var user_id = jQuery('#user_id').val();
		var email = jQuery('#email').val();
		var nama = jQuery('#nama').val();
		var investasi = jQuery('#investasi').val();
		var minInvestasi = jQuery('#investasi').attr('tag');
		
		if(investasi == "")
		{
			jQuery('#investasi').val(jQuery('#investasi').attr('tag'));
		}
		
		if(nama == "")
		{
			alert('semua fields harus terisi');
		}
		else if((user_id == 0) && (email == ""))
		{
			alert('semua fields harus terisi');
		}
		else if(isNaN(investasi) == true)
		{
			alert('investasi harus berupa angka');
		}
		else if(investasi <= (minInvestasi-1))
		{
			alert('investasi minimal '+minInvestasi);
		}
		else
		{
			document.investAddPlan.submit();
		}
	});
});
</script>
</div>
<?php 

break; 
?>