<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
$user =& JFactory::getUser();
?>
<div class="COM_INV_PLAN">
<form action="index.php" method="post" name="adminForm">
<?php
	foreach($this->data['plan'] as $row)
	{
		echo "<div class='COMFOR_BOXOUT'>
				<div class='boxPlan'>
					<ul class='boxPlanInv'>
						<li class='seeDescription' id='".$row->id."' tag='closed' class='seeDescription'><a href='#'>Descreption</a></li>
						<li class='seeTerm' id='".$row->id."' tag='closed'><a href='#'>Term Of Plan</a></li>
						<li class='detailPlan' tag='".$row->id."'><a href='#'>Add Plan</a></li>
					</ul>
				<div class='planTitle'>".$row->plan."</div>
<div class='clearFloat'></div>
					<ul>
						<li>Minimal Investasi : Rp. ".number_format($row->investasi,0,',','.')."</li>
						<li>Jangka Waktu : ".$row->waktu." Hari</li>
					</ul>
				</div>
				
				<div>
		
				
				<div class='descriptionPlan' id='description".$row->id."'>";
				if(!empty($row->description)){ echo $row->description; } else { echo "Belum ada keterangan"; }
				
				echo "</div>
				<div class='termOfPlan' id='term".$row->id."'>";
				if(!empty($row->term)){ echo $row->term; } else { echo "Belum ada Syarat Dan Ketentuan"; }
				echo "</div>";
				echo "</div></div>";
	}
?>
<input type="hidden" name="option" value="com_invest" />
<input type="hidden" name="c" value="plan" />
<input type="hidden" name="boxchecked" value="" />
<input type="hidden" name="task" value="" />    
<?php echo JHTML::_( 'form.token' ); ?>
</form>
<div class="topPopup"></div>
<div class="backPopup"></div>
<style>

</style>
<script>
var jQuery = jQuery.noConflict();
jQuery(document).ready(function()
{
	jQuery('.detailPlan').click(function()
	{
		var planId = jQuery(this).attr('tag');
		jQuery('.topPopup').html('<img src="<?php echo JURI::Base(); ?>administrator/components/com_invest/assets/images/process.gif" />');
		jQuery('.topPopup').fadeIn();
		jQuery('.backPopup').fadeIn();
		jQuery.ajax({
			type:"POST",
			url:"<?php echo JURI::Base().'index.php'; ?>",
			data:{option:"com_invest",view:"plan",task:"orderPlan",id:planId},
		}).done(function(show)
		{
			jQuery('.topPopup').html(show);
			jQuery('.doOrderPlan').click(function()
			{
				alert('okeh');
			});
		});
	});
	
	
	jQuery('.seeDescription').click(function()
	{
		var id = jQuery(this).attr('id');
		var tag = jQuery(this).attr('tag');
		if(tag == "closed")
		{
			jQuery('#description'+id).slideDown('fast');
			jQuery(this).html('Close');
			jQuery(this).attr('tag','open');
			jQuery('#description'+id).focus();
			
			//for closing term
			jQuery('#term'+id).slideUp('fast');
			jQuery('.seeTerm').html('Term Of Plan');
			jQuery('.seeTerm').attr('tag','closed');
		}
		else
		{
			jQuery('#description'+id).slideUp('fast');
			jQuery(this).html('Description');
			jQuery(this).attr('tag','closed');
		}
		
	});
	
	jQuery('.seeTerm').click(function()
	{
		var id = jQuery(this).attr('id');
		var tag = jQuery(this).attr('tag');
		if(tag == "closed")
		{
			jQuery('#term'+id).slideDown('fast');
			jQuery(this).html('Close');
			jQuery(this).attr('tag','open');
			jQuery('#term'+id).focus();
			
			//for closing description 
			jQuery('#description'+id).slideUp('fast');
			jQuery('.seeDescription').html('Description');
			jQuery('.seeDescription').attr('tag','closed');
		}
		else
		{
			jQuery('#term'+id).slideUp('fast');
			jQuery(this).html('Term Of Plan');
			jQuery(this).attr('tag','closed');
		}
		
	});
	
	jQuery('.backPopup').click(function()
	{
		jQuery(this).fadeOut();
		jQuery('.topPopup').fadeOut();
	});
});
</script>
<div style="clear:both;"></div>
</div>