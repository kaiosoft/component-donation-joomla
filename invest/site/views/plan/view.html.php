<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewPlan extends JView
{

	function display($tpl = null)
	{
		$app =& JFactory::getApplication();
		$task = JRequest::getVar('task');
		$id = JRequest::getVar('id');
    
	    $modelPlan =& JModel::getInstance('plan','InvestModel');
		
		$limit		= $app->getUserStateFromRequest( 'global.list.limit', 'limit', $app->getCfg('list_limit'), 'int' );
		$limitstart = $app->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
		
		// get the total number of records
		$total = $modelPlan->getTotal($filter);
			
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
		
		if($task == "orderPlan")
		{
		$plan = $modelPlan->getPlanById($id);
		}
		else
		{
		$plan = $modelPlan->getAllPlan($filter,$pageNav);
		}
		
		$listPlan = $modelPlan->getListPlan();

		//prepare data
		$data = array();
		$data['plan'] = $plan;
		$data['listPlan'] = $listPlan;
		$data['pageNav'] = $pageNav;
		
		$this->assignRef('data',$data);
		parent::display($tpl);
		
	}
		
}	

?>
