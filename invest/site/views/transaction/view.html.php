<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class InvestViewTransaction extends JView
{

	function display($tpl = null)
	{
		$app =& JFactory::getApplication();
		$task = JRequest::getVar('task');
		$user =& JFactory::getUser();
		$helper = new investHelper;
		$db =& JFactory::getDBO();
		
		if(empty($user->id))
		{
			$app->redirect('index.php?option=com_invest&view=plan&task=login','Silakan login terlebih dahulu','error');
		}

	    $modelTransaction =& JModel::getInstance('transaction','InvestModel');
		
		$member = $helper->getDataByParam("user_id","='".$user->id."'","#__invest_member");
		$filter = $helper->setFilter("a.member_id","='".$member->id."'");
		$filter = $helper->unsetWhereFilter($filter);
		
		$limit		= $app->getUserStateFromRequest( 'global.list.limit', 'limit', $app->getCfg('list_limit'), 'int' );
		$limitstart = $app->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
			
		$total = $modelTransaction->getTotal($filter);
		
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
		
		if($task == "withdraw")
		{
			$transaction = $modelTransaction->getWithdraw($filter,$pageNav);
		}
		else
		{
			$transaction = $modelTransaction->getDeposit($filter,$pageNav);
		}
		
		$listBank = $helper->getDataByParam("","","#__invest_bank","array");
		
		//prepare data
		$data = array();
		$data['transaction'] = $transaction;
		$data['pageNav'] = $pageNav;

		$this->assignRef('data',$data);
		parent::display($tpl);
		
	}
		
}	

?>
