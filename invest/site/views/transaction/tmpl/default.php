<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;
?>
<div class="COM_INV_TRANSACTION">
<?php
echo "<table>
		<tr>
			<th>#</th>
			<th>Tanggal</th>
			<th>Plan</th>
			<th>Nominal</th>
			<th>Bank</th>
			<th>Status</th>
		</tr>";
		$i = 1;
foreach($this->data['transaction'] as $row)
{
	echo "<tr>
			<td>".$i."</td>
			<td>".$row->date."</td>
			<td>".$row->plan." - ".$row->nama."</td>
			<td>Rp. ".number_format($row->nominal,0,',','.')."</td>
			<td>Bank ".$row->bank." ,No. Rek : ".$row->rekening." ,A/N ".$row->an."</td>
			<td>".$row->status."</td>
		</tr>";
		$i++;
}
echo "</table>";

?>
</div>