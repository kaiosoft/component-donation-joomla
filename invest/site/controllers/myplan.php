<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

class InvestControllerMyplan extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		$task = JRequest::getVar('task');
		switch($task)
		{
			case "statistic":
			JRequest::setVar('layout','detail');
			break;
			
			case "withdrawInvest":
			JRequest::setVar('layout','withdraw_form');
			break;
			
			case "printStatistic":
			JRequest::setVar('layout','detail_print');
			break;
			
			case "pdfStatistic":
			JRequest::setVar('layout','detail_pdf');
			break;
			
			default:
			JRequest::setVar('layout','default');
			break;
		
		}
		parent::__construct( $config );
	}

	/**
	 * Display the list of Plan
	 */
	function display()
	{
		JRequest::setVar('view', 'myplan' );		
		parent::display(); 
	}
		
}

?>