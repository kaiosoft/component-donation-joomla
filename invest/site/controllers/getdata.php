<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );
error_reporting(0);

class InvestControllerGetdata extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		$task = JRequest::getVar('task');
		switch($task)
		{
			case "cekMemberPlan":
			$this->cekMemberPlan();
			break;
			
			case "getDataBank":
			$this->getDataBank();
			break;
			
			case "cekMember":
			$this->cekMember();
			break;
			
			case "saveTransaction":
			$this->saveTransaction();
			break;
			
			default:
			echo "getData";
			break;
			
		
		}
		parent::__construct( $config );
	}
	
	function cekMemberPlan()
	{
		$post = JRequest::get("POST");
		$helper = new investHelper;
		
		$db =& JFactory::getDBO();
		$sql = "SELECT a.*,b.email FROM #__invest_member AS a LEFT JOIN #__users AS b ON (b.id=a.user_id) WHERE b.email='".$post['email']."'";
		$db->setQuery($sql);
		$member = $db->loadObject();
		
		if($_POST['type'] == "Withdraw")
		{
			$sql = "SELECT a.*,b.bank FROM #__invest_member_bank AS a LEFT JOIN #__invest_bank AS b ON (b.id=a.bank_id) WHERE a.member_id='".$member->id."'";
			$db->setQuery($sql);
			$listBank = $db->loadObjectList();
		}
		else
		{
			$listBank = $helper->getDataByParam("","","#__invest_bank");
		}
		
		
		$lists = array();
		
		$banklist[]		= JHTML::_('select.option',  '', JText::_( 'MOD_INVEST_OPTION_BANK' ), 'id', 'bank' );
		$banklist		= array_merge($banklist,$listBank);
		$lists['bank']	= JHTML::_('select.genericlist', $banklist, 'bank_id', 'class="inputbox" size="1"', 'id', 'bank', $_POST['bank_id'] );	
		
		
		if(!empty($member->id))
		{
			$memberPlan = $helper->getDataByParam(array("plan_id","member_id","nama"),array("='".$post['plan']."'","='".$member->id."'","='".$post['nama']."'"),"#__invest_member_plan");
			if(!empty($memberPlan->id))
			{
				echo "<p>".$lists['bank']."</p>
				<p class='detailBank'></p>
				<p><textarea name='comment' placeHolder='Masukkan pesan jika perlu'></textarea></p>
				<input type='hidden' id='memberPlanId' value='".$memberPlan->id."' />
				<input type='hidden' id='type' value='".$memberPlan->id."' />
				<input type='hidden' id='nominal' value='".$memberPlan->id."' />
				<input type='hidden' id='date' value='".date("Y-m-d")."' />
				<p><input type='button' value='proses' class='doTransaction' /></p>";
			}
			else
			{
				echo "member plan tidak tersedia";
			}
		}
		else
		{
			echo "Maaf, kami tidak menemukan member dengan email ".$post['email'];
		}
		echo "<input type='button' value='back' class='backForm'>";
		
		break;
	}
	
	function saveTransaction()
	{
		$db =& JFactory::getDBO();
		$helper = new investHelper;
		$post = JRequest::get("post");
		$app =& JFactory::getApplication();
		
		$db =& JFactory::getDBO();
		$sql = "SELECT a.*,b.email FROM #__invest_member AS a LEFT JOIN #__users AS b ON (b.id=a.user_id) WHERE b.email='".$post['email']."'";
		$db->setQuery($sql);
		$member = $db->loadObject();
		
		$memberPlan = $helper->getDataByParam(array("plan_id","member_id","nama"),array("='".$post['plan']."'","='".$member->id."'","='".$post['nama']."'"),"#__invest_member_plan");
		
		$axisTransaction = $helper->getDataByParam(array("member_plan_id","type","status"),array("='".$memberPlan->id."'","='".$post['type']."'","='Pending'"),"#__invest_transaction");
		if(!empty($axisTransaction->id))
		{
			$msg = "Permintaan, anda masih dalam proses";
			$mode = "error";
		}
		else
		{
			$sql = "INSERT INTO #__invest_transaction (member_id,member_plan_id,type,nominal,bank_id,member_bank_id,date) VALUES ('".$member->id."','".$memberPlan->id."','".$post['type']."','".$post['nominal']."','".$post['bank_id']."','".$post['member_bank_id']."','".date("Y-m-d H:i:s")."')";
			$db->setQuery($sql);
			
			if(!$db->query())
			{
				$msg = "Gagal menyimpan data";
				$mode = "error";
			}
			else
			{
				if($post['type'] == "Deposit")
				{
					$statusTransaction = "Confirm";
					$returnLink = "deposit";
					
					//send mail to member
					$notifMember = $helper->getConfig('confirmInvest');
					$templateMember = $helper->getConfig('memberMailTemplate');
					$templateMember = str_replace("[textEmail]",$notifMember,$templateMember);
					$senderMail = $helper->getConfig('senderMail');
					$senderName = $helper->getConfig('senderName');
					$helper->sendMail($post['email'],$senderMail,$senderName,"Pemberitahuan Permintaan Konfirmasi",$templateMember,"true");
						
					//send mail to admin
					$notifAdmin = $helper->getConfig('memberConfirmInvest');
					$adminMail = $helper->getConfig('adminMail');
					$helper->sendMail($adminMail,'info@kaiogroup.com','Administrator Website',"Pemberitahuan Permintaan Konfirmasi",$notifAdmin);
				}
				else
				{
					$statusTransaction = "Withdraw";
					$returnLink = "withdraw";
					
					//send mail to member
					$notifMember = $helper->getConfig('withdrawInvest');
					$templateMember = $helper->getConfig('memberMailTemplate');
					$templateMember = str_replace("[textEmail]",$notifMember,$templateMember);
					$senderMail = $helper->getConfig('senderMail');
					$senderName = $helper->getConfig('senderName');
					$helper->sendMail($post['email'],$senderMail,$senderName,"Pemberitahuan Permintaan Penarikan Invesatasi",$templateMember);
						
					//send mail to admin
					$notifAdmin = $helper->getConfig('memberWithdrawInvest');
					$adminMail = $helper->getConfig('adminMail');
					$helper->sendMail($adminMail,'info@kaiogroup.com','Administrator Website',"Pemberitahuan Permintaan Penarikan Invesatasi",$notifAdmin);
				}
			
				$msg = "Berhasil menyimpan data";
				$sql = "UPDATE #__invest_member_plan SET status='".$statusTransaction."' WHERE id='".$memberPlan->id."'";
				$db->setQuery($sql);
				$db->query();
			}
		}
		
		if(empty($post['breakPage']))
		{
		$app->redirect('index.php?option=com_invest&view=transaction&task='.$returnLink,$msg,$mode);
		}
		else
		{
		break;
		}
	}
	
	function cekMember()
	{
		$post =& JRequest::get("post");
		$db =& JFactory::getDBO();
		$helper = new investHelper;
		
		$sql = "SELECT a.email,b.* FROM #__users AS a LEFT JOIN #__invest_member AS b ON (b.user_id=a.id) WHERE a.email='".$post['email']."'";
		$db->setQuery($sql);
		$member = $db->loadObject();
		$lists = array();
		
		if(!empty($member->id))
		{
			$cekMemberPlan = $helper->getDataByParam("member_id","='".$member->id."'","#__invest_member_plan","array");
			if(empty($cekMemberPlan))
			{
				echo "anda belum mempunyai plan";
			}
			else
			{	
				if((!empty($post['nama'])) && (!empty($post['plan_id'])))
				{
				
					$cekPlan = $helper->getDataByParam(array("plan_id","member_id","nama"),array("='".$post['plan_id']."'","='".$member->id."'","='".$post['nama']."'"),"#__invest_member_plan");
					if(!empty($cekPlan->id))
					{
						if($post['type'] == "Deposit")
						{
						
							$listBank = $helper->getDataByParam("","","#__invest_bank","array");
							$banklist[]		= JHTML::_('select.option',  '', JText::_( 'MOD_INVEST_OPTION_BANK' ), 'id', 'bank' );
							$banklist 		= array_merge($banklist,$listBank);
							$lists['bank']	= JHTML::_('select.genericlist', $banklist, 'bank_id', 'class="inputbox" size="1"', 'id', 'bank' );
							
							if($cekPlan->status != "InActive")
							{
								echo "Anda tidak perlu lagi melakukan konfirmasi, karena anda telah melakukan konfirmasi / plan ini telah aktif";
							}
							else
							{
								echo $lists['bank']."<input type='hidden' id='nomTransfer' value='".$cekPlan->investasi."' />";
							}
						}
						else
						{
							$sql = "SELECT a.*,b.bank FROM #__invest_member_bank AS a LEFT JOIN #__invest_bank AS b ON (b.id=a.bank_id) WHERE a.member_id='".$member->id."'";
							$db->setQuery($sql);
							$banklist[]		= JHTML::_('select.option',  '', JText::_( 'MOD_INVEST_OPTION_MEMBER_BANK' ), 'id', 'bank' );
							$banklist 		= array_merge($banklist,$db->loadObjectList());
							$lists['bank']	= JHTML::_('select.genericlist', $banklist, 'member_bank_id', 'class="inputbox" size="1"', 'id', 'bank' );
							
							if(($cekPlan->status == "InActive") || ($cekPlan->status == "Confirm"))
							{
								echo "pilih type konfirmasi jika anda ingin melakukan konfirmasi pembayaran";
							}
							else if($cekPlan->status == "Active")
							{
								if($post['type'] == "Withdraw Profit")
								{
									$profitInvest = $helper->getProfitInvest($cekPlan);
									$profitInvest = $profitInvest - $cekPlan->investasi;
									echo $lists['bank']."<input type='hidden' id='nomTransfer' value='".$profitInvest."' />";
								}
								else
								{
									echo "Plan ini masih berjalan, anda akan dikenakan penalty jika withdraw saat ini <br />";
									$profitInvest = $helper->getProfitInvest($cekPlan);
									echo $lists['bank']."<input type='hidden' id='nomTransfer' value='".$profitInvest."' />";
								}
							}
							else if($cekPlan->status == "Withdraw")
							{
								echo "Permintaan withdraw anda masih dalam proses";
							}
							else
							{
								$profitInvest = $helper->getProfitInvest($cekPlan);
								echo $lists['bank']."<input type='hidden' id='nomTransfer' value='".$profitInvest."' />";
							}
						}
						
					}
					else
					{
						echo "plan / nama plan yang anda masukkan salah";
					}
				}
				else
				{
					echo "pilih plan, dan masukkan nama plan";
				}
				
			}
		}
		else
		{
			
			if(empty($post['email']))
			{
				echo "Silakan masukkan email";
			}
			else
			{
				echo "Email belum terdaftar, silakan daftar terlebih dahulu";
			}
		}
		
		break;
	}
	
	function getDataBank()
	{
		$bank_id = JRequest::getVar('bank_id');
		$member_bank_id = JRequest::getVar('member_bank_id');
		$helper = new investHelper();
		
		if(!empty($bank_id))
		{
			$bank = $helper->getDataByParam("id","='".$bank_id."'","#__invest_bank");
		}
		else
		{
			$bank = $helper->getDataByParam("id","='".$member_bank_id."'","#__invest_member_bank");
		}
		echo "No rek : ".$bank->rekening."<br />A/N : ".$bank->an;
		break;
	}
	
	
		
}

?>