<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

class InvestControllerTransaction extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		$task = JRequest::getVar('task');
		$user =& JFactory::getUser();
		switch($task)
		{
			case "detail":
			JRequest::setVar('layout','add');
			break;
			
			default:
			JRequest::setVar('layout','default');
			break;
		}
		
		parent::__construct( $config );
	}

	/**
	 * Display the list of Transaction
	 */
	function display()
	{
		JRequest::setVar('view', 'transaction' );		
		parent::display(); 
	}
		
}

?>