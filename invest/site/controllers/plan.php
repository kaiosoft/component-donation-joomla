<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
error_reporting(0);
jimport( 'joomla.application.component.controller' );

class InvestControllerPlan extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		$task = JRequest::getVar('task');
		$user =& JFactory::getUser();
		switch($task)
		{
			case "detailPlan":
			$this->detailPlan();
			break;
			
			case "doLogin":
			$this->doLogin();
			break;
			
			case "doRegister":
			$this->doRegister();
			break;
			
			case "login":
			JRequest::setVar('formuser','login');
			JRequest::setVar('layout','formuser');
			break;
			
			case "register":
			JRequest::setVar('formuser','register');
			JRequest::setVar('layout','formuser');
			break;
			
			case "orderPlan":
			JRequest::setVar('layout','add');
			break;
			
			case "getRegional":
			$this->getRegional();
			break;
			
			default:
			JRequest::setVar('layout','default');
			break;
		}
		
		parent::__construct( $config );
	}

	/**
	 * Display the list of Plan
	 */
	function display()
	{
		JRequest::setVar('view', 'plan' );		
		parent::display(); 
	}
	
	function detailPlan()
	{
		$post = JRequest::get('post');
		$helper = new investHelper;
		
		$plan = $helper->getDataByParam("id","='".$post['planId']."'","#__invest_plan");
		echo "<center>
		<h2>Plan Investasi '".$plan->plan."'</h2>
		<div class='detailThisPlan'>
		Minimal Investasi : Rp. ".number_format($plan->investasi,0,',','.')."<br />
		Jangka Waktu : ".$plan->waktu." Hari Kerja<br />
		Profit Investasi : ".$plan->profit."%
		</div>
		<div class='termOfCondition'>".$plan->term."</div>
		<input type=button class='orderButton' value='Order Plan' />
		</center>";
		
		break;
	}
	
	
	//register user
	function doRegister()
	{
		$app =& JFactory::getApplication();
		$post = JRequest::get('post');
		$helper = new investHelper();
		$db =& JFactory::getDBO();
		$sql = "SELECT a.*,b.email FROM #__invest_member AS a LEFT JOIN #__users AS b ON (b.id=a.user_id) WHERE b.email='".$post['email']."'";
		$db->setQuery($sql);
		$member = $db->loadObject();
		
		if(!empty($member->id))
		{
			echo "maaf, email tersebut telah terdaftar";
		}
		else
		{
			//prepare username & email
			$data['username'] = $post['email'];
			$data['email'] = $post['email'];
			$data['name'] = $post['nama'];
			$registerUser = $helper->registerUser($data,"2","index.php?option=com_invest&view=plan");
			$post['user_id'] = $registerUser['user_id'];
			$myPassword = $registerUser['password'];
			if(!empty($post['user_id']))
			{
				if((empty($post['penanggung_jawab'])) && ($post['type'] == "Personal"))
				{
					$post['penanggung_jawab'] = $post['nama'];
				}
				
				require_once('administrator/components/com_invest/helpers/regional.php');
				$regionalHelper = new regionalHelper;
				$regional = $regionalHelper->getRegional($post);
				
				$sql = "INSERT INTO #__invest_member (user_id,nama,type,penanggung_jawab,hp,alamat,kota,provinsi,negara,kode_pos,date) VALUES ('".$post['user_id']."','".$post['nama']."','".$post['type']."','".$post['penanggung_jawab']."','".$post['hp']."','".$post['alamat']."','".$regional['kota']."','".$regional['provinsi']."','".$regional['negara']."','".$post['kode_pos']."','".date("Y-m-d H:i:s")."')";
				$db->setQuery($sql);
				if(!$db->query())
				{
					echo "<script>alert('gagal melakukan pendaftaran');
					history.back(-1);
					</script>";
				}
				else
				{
					//send mail to member
					$notifMember = $helper->getConfig('registerMember');
					$notifMember = str_replace("[email]",$post['email'],$notifMember);
					$notifMember = str_replace("[password]",$myPassword,$notifMember);
					$templateMember = $helper->getConfig('memberMailTemplate');
					echo $templateMember = str_replace("[textEmail]",$notifMember,$templateMember);
					$senderMail = $helper->getConfig('senderMail');
					$senderName = $helper->getConfig('senderName');
					$helper->sendMail($post['email'],$senderMail,$senderName,"Pemberitahuan Pendaftaran",$templateMember,"true");
					
					$msg = "Anda Berhasil Terdaftar, Username : ".$post['email']." & Password : ".$myPassword." Silakan ubah password anda di halaman account";
					
					//send mail to admin
					$notifAdmin = $helper->getConfig('memberRegister');
					$adminMail = $helper->getConfig('adminMail');
					$helper->sendMail($adminMail,'info@kaiogroup.com','Administrator Website',"Pemberitahuan Pendaftaran",$notifAdmin);
					
					JRequest::setVar('username',$post['email']);
					JRequest::setVar('password',$myPassword);
					JRequest::setVar('notifUser',$msg);
					$this->doLogin();
					$app->redirect('index.php?option=com_invest&view=profil');
				}
			}
			else
			{
				$msg =  "Gagal Menyimpan Data";
			}
		}
		
		echo $msg;
		break;
	}
	
	function doLogin()
	{
		$post =& JRequest::get('post');
		//JSession::checkToken('post') or jexit(JText::_('JInvalid_Token'));

		$app = JFactory::getApplication();

		// Populate the data array:
		$data = array();
		$data['return'] = base64_decode(JRequest::getVar('return', '', 'POST', 'BASE64'));
		$data['username'] = JRequest::getVar('username', '', 'method', 'username');
		$data['password'] = JRequest::getString('password', '', 'post', JREQUEST_ALLOWRAW);

		// Set the return URL if empty.
		if (empty($data['return'])) {
			$data['return'] = 'index.php?option=com_users&view=profile';
		}

		// Set the return URL in the user state to allow modification by plugins
		$app->setUserState('users.login.form.return', $data['return']);

		// Get the log in options.
		$options = array();
		$options['remember'] = JRequest::getBool('remember', false);
		$options['return'] = $data['return'];

		// Get the log in credentials.
		$credentials = array();
		$credentials['username'] = $data['username'];
		$credentials['password'] = $data['password'];

		// Perform the log in.
		if (true === $app->login($credentials, $options)) {
			// Success
			$app->setUserState('users.login.form.data', array());
			//$app->redirect(JRoute::_($app->getUserState('users.login.form.return'), false));
			$login = 1;
		} else {
			// Login failed !
			$data['remember'] = (int)$options['remember'];
			$app->setUserState('users.login.form.data', $data);
			//$app->redirect(JRoute::_('index.php?option=com_users&view=login', false));
			$login = 0;
		}
		if($login == 1)
		{
			if((!empty($post['plan_id'])) || (!empty($post['namaplan'])) || (!empty($post['investasi'])))
			{
				
				JRequest::setVar('plan_id',$post['plan_id']);
				JRequest::setVar('nama',$post['namaplan']);
				JRequest::setVar('investasi',$post['investasi']);
				$notifUser = JRequest::getVar('notifUser');
				JRequest::setVar('notifUser',$notifUser);
				$this->savePlan();
			}
			else
			{
				$app->redirect(JRoute::_('index.php?option=com_invest&view=profil', false));
			}
		}
		else
		{
			JRequest::setVar('layout','formuser');
			JRequest::setVar('formuser','login');
			$this->display();
			//$app->redirect(JRoute::_('index.php?option=com_invest&view=plan&task=login', false));
		}
	}
	
	function savePlan()
	{
		$post = JRequest::get("post");
		$helper = new investHelper;
		$db =& JFactory::getDBO();
		$user =& JFactory::getUser();
		$app =& JFactory::getApplication();
		
		$isMember = $helper->getDataByParam("user_id","='".$user->id."'","#__invest_member");
		if(!empty($isMember->id))
		{
			$axisPlan = $helper->getDataByParam(array("member_id","plan_id","nama"),array("='".$isMember->id."'","='".$post['plan_id']."'","='".$post['nama']."'"),"#__invest_member_plan");
			if(!empty($axisPlan->id))
			{
				$msg = "Plan dengan nama tersebut telah anda tambahkan sebelumnya";
				$mode = "error";
			}
			else
			{
				if((!empty($post['plan_id'])) || (!empty($post['nama'])) || (!empty($post['investasi'])))
				{
					$post['account'] = $isMember->id.$post['plan_id'].rand(111,999);
					
					$sql = "INSERT INTO #__invest_member_plan (member_id,plan_id,account,nama,investasi,date) VALUES('".$isMember->id."','".$post['plan_id']."','".$post['account']."','".$post['nama']."','".$post['investasi']."','".date("Y-m-d H:i:s")."')";
					$db->setQuery($sql);
					if(!$db->query())
					{
						$msg = "Gagal memasukkan data";
						$mode = "error";
					}
					else
					{
						$msg = "Berhasil menambahkan plan investasi baru";
						
						//prepare data to send
						$plan = $helper->getDataByParam("id","='".$post['plan_id']."'","#__invest_plan");
						$rekening = $helper->getDataByParam("","","#__invest_bank","array");
						$rekenings = array();
						
						foreach($rekening as $row)
						{
							$rekenings[] = $row->bank." No. Rekening : ".$row->rekening." A/N : ".$row->an;
						}
						
						//send mail to member
						$notifMember = $helper->getConfig('addedPlan');
						$notifMember = str_replace("[nama]",$isMember->nama,$notifMember);
						$notifMember = str_replace("[plan]",$plan->plan,$notifMember);
						$notifMember = str_replace("[memberplan]",$plan->plan." - ".$post->nama,$notifMember);
						$notifMember = str_replace("[term]",$plan->term,$notifMember);
						$notifMember = str_replace("[memberinvestasi]",$post['investasi'],$notifMember);
						$notifMember = str_replace("[rekening]",implode($rekenings,"<br />"),$notifMember);
						$templateMember = $helper->getConfig('memberMailTemplate');
						$templateMember = str_replace("[textEmail]",$notifMember,$templateMember);
						$senderMail = $helper->getConfig('senderMail');
						$senderName = $helper->getConfig('senderName');
						$helper->sendMail($user->email,$senderMail,$senderName,"Pemberitahuan Penambahan Plan Investasi",$templateMember,"true");
						
						//send mail to admin
						$notifAdmin = $helper->getConfig('memberAddedPlan');
						$notifAdmin = str_replace("[email]",$post['email'],$notifAdmin);
						$notifAdmin = str_replace("[memberplan]",$plan->plan." - ".$post->nama,$notifAdmin);
						$notifAdmin = str_replace("[memberinvestasi]",$post['investasi'],$notifAdmin);
						$adminMail = $helper->getConfig('adminMail');
						$helper->sendMail($adminMail,'info@kaiogroup.com','Administrator Website',"Pemberitahuan Penambahan Plan Investasi",$notifAdmin);
					}
				}
				else
				{
					$msg = "Gagal memasukkan data, silakan ulangi kembali";
					$mode = "error";
				}
			}
			$app->redirect('index.php?option=com_invest&view=myplan',$msg."<br />".JRequest::getVar('notifUser'),$mode);
		}
		else
		{
			$sql = "SELECT a.*,b.email FROM #__invest_member AS a LEFT JOIN #__users AS b ON (b.id=a.user_id) WHERE b.email='".$post['email']."'";
			$db->setQuery($sql);
			$member = $db->loadObject();
			
			if(!empty($member->id))
			{
				echo "Email ".$post['email']." Telah terdaftar, Silakan login untuk melanjutkan pembelian plan";
				JRequest::setVar('formuser','login');
				JRequest::setVar('plan_id',$post['plan_id']);
				JRequest::setVar('email',$post['email']);
				JRequest::setVar('namaplan',$post['nama']);
				JRequest::setVar('investasi',$post['investasi']);
			}
			else
			{
				echo "<div class='textPesan'>Langkah ke-2 : Email ".$post['email']." belum terdaftar sebagai member kami, silahkan lengkapi data Anda untuk melakukan Registrasi member dan melanjutkan proses order Plan Investasi Anda.</div><div class='clearFloat'></div>";
				JRequest::setVar('formuser','register');
				JRequest::setVar('plan_id',$post['plan_id']);
				JRequest::setVar('namaplan',$post['nama']);
				JRequest::setVar('investasi',$post['investasi']);
			}
			
			JRequest::setVar('layout','formuser');
			$this->display();
		}
	}
	
	function getRegional()
	{
		require_once('administrator/components/com_invest/helpers/regional.php');
		$helper = new regionalHelper;
		
		$regional = JRequest::setVar('regional');
		$country = JRequest::getVar('country');
		$state = JRequest::getVar('state');
		
		if($regional == "state")
		{
			$listState 		= $helper->getState($country);
			$jumlah = count($listState);
			if($jumlah > 1)
			{
				$statelist		= $listState;
				echo $lists['state'] = JHTML::_('select.genericlist', $statelist, 'state', 'class="inputbox" size="1"', 'value', 'text' );
			}
			else
			{
				echo "<input type='text' id='state' name='state' placeHolder='Masukkan Provinsi' />";
			}
		}
		else
		{
			$listCity 		= $helper->getCity($country,$state);
			$jumlah			= count($listCity);
			if($jumlah > 1)
			{
				$citylist		= $listCity;
				echo $lists['city'] 	= JHTML::_('select.genericlist', $citylist, 'city', 'class="inputbox" size="1"', 'value', 'text' );
			}
			else
			{
				echo "<input type='text' name='city' id='city' placeHolder='Masukkan Kota' />";
			}
		}
		break;
	}
	
	function removeBank()
	{
		$id = $_POST['id'];
		$db =& JFactory::getDBO();
		$sql = "DELETE FROM #__invest_member_bank WHERE id='".$id."'";
		$db->setQuery($sql);
		if(!$db->query())
		{
			echo "Gagal menghapus data";
		}
		else
		{
			echo "Berhasil menghapus data bank";
		}
		break;
	}
	
}

?>