<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
error_reporting(0);
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

class InvestControllerProfil extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		$task = JRequest::getVar('task');
		switch($task)
		{
			case "editprofil":
			JRequest::setVar('layout','editprofil');
			break;
			
			case "saveProfil":
			$this->saveProfil();
			break;
			
			default:
			JRequest::setVar('layout','default');
			break;
		}
		
		parent::__construct( $config );
	}

	/**
	 * Display the list of profil
	 */
	function display()
	{
		JRequest::setVar('view', 'profil' );		
		parent::display(); 
	}
	
	function saveProfil()
	{
		$post = JRequest::get('post');
		$user =& JFactory::getUser();
		$helper = new investHelper;
		$app =& JFactory::getApplication();
		$db =& JFactory::getDBO();
		
		if(empty($user))
		{
			$app->redirect('index.php?option=com_invest&view=plan&task=login','Login Terlebih Dahulu','error');
		}
		else
		{
			if($post['type'] == "Personal")
			{
				$post['penanggung_jawab'] = $post['nama'];
			}
			
			/* Do Upload File */
			if(!empty($_FILES['legalitas']))
			{
				$getMember = $helper->getDataByParam("id","='".$post['id']."'","#__invest_member");
				$destination = $helper->getConfig('uploadDir');
				$maxSizeUpload = $helper->getConfig('maxSizeUpload');
				$originalName = $_FILES['legalitas']['name'];
				$format = substr($originalName,strpos($originalName,"."));
				$nameFile = $getMember->nama.$getMember->user_id.$format;
				if($_FILES['legalitas']['size'] > $maxSizeUpload)
				{
					$fileMsg = "Ukuran file terlalu besar, batas maksimum adalah ".$maxSizeUpload;
				}
				else
				{
					if($legalitas['error'] > 0)
					{
						$fileMsg = "Return Code: " . $_FILES["legalitas"]["error"] . "<br />";
					}
					else
					{
						$type = $_FILES['legalitas']['type'];
						if(($type == "image/jpeg") || ($type == "image/png"))
						{
							//if(!file_exists($destination . $namaFile))
							$post['filename'] = $nameFile;
							move_uploaded_file($_FILES["legalitas"]["tmp_name"],
							$destination . $nameFile);
							$fileMsg = "File telah terupload, menunggu persetujuan admin untuk mengaktifkan keanggotaan anda";
						}
						else
						{
							$fileMsg = "File bertipe ".$type." tidak di izinkan, paling tidak jpg / png";
						}
						
					}
				}
				
			}
			/* End of Upload File */
			
			require_once('administrator/components/com_invest/helpers/regional.php');
			$regionalHelper = new regionalHelper;
			$regional = $regionalHelper->getRegional($post);
			
			if(empty($post['filename']))
			{
				$post['filename'] = $post['legalitas'];
			}
			
			echo $sql = "UPDATE #__invest_member SET nama='".$post['nama']."',identitas='".$post['identitas']."',no_identitas='".$post['no_identitas']."',alamat='".$post['alamat']."',kota='".$regional['kota']."',provinsi='".$regional['provinsi']."',negara='".$regional['negara']."',kode_pos='".$post['kode_pos']."',penanggung_jawab='".$post['penanggung_jawab']."',hp='".$post['hp']."',type='".$post['type']."',legalitas='".$post['filename']."' WHERE user_id='".$user->id."'";
			$db->setQuery($sql);
			if(!$db->query())
			{
				$msg = "Gagal menyimpan data, silakan ulangi kembali";
			}
			else
			{
				$msg = "Profil telah tersimpan";
			}
			
			
			
			if(!empty($post['member_bank_id']))
			{
				for($i=0;$i<count($post['member_bank_id']);$i++)
				{
					$cekBank = $helper->getDataByParam("id","='".$post['idBank'][$i]);
					if(!empty($cekBank->id))
					{
						if((!empty($post['member_bank_id'][$i])) && (!empty($post['no_rekening'][$i])) && (!empty($post['an'][$i])))
						{
							$sql = "UPDATE #__invest_member_bank SET bank_id='".$post['member_bank_id'][$i]."',rekening='".$post['no_rekening'][$i]."',an='".$post['an'][$i]."' WHERE id='".$cekBank->id."'";
							$db->setQuery($sql);
							$db->query();
						}
					}
					else
					{
						$axisBank = $helper->getDataByParam(array("member_id","bank_id","rekening"),array("='".$post['id']."'","='".$post['member_bank_id'][$i]."'","='".$post['no_rekening'][$i]."'"),"#__invest_member_bank");
						if(!empty($axisBank->id))
						{
							
						}
						else
						{
							if((!empty($post['member_bank_id'][$i])) && (!empty($post['no_rekening'][$i])) && (!empty($post['an'][$i])))
							{
								$sql = "INSERT INTO #__invest_member_bank (member_id,bank_id,rekening,an) VALUES ('".$post['id']."','".$post['member_bank_id'][$i]."','".$post['no_rekening'][$i]."','".$post['an'][$i]."')";
								$db->setQuery($sql);
								$db->query();
							}
						}
					}
				}
			}
		}
		$app->redirect('index.php?option=com_invest&view=profil&task=editprofil',$msg."<br />".$fileMsg);
	}
		
}

?>