<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
class regionalHelper{
	
	//created, 2 juni 2012
	function getCountry()
	{
		$country = array();
		$country["AF"] = "Afghanistan";
		$country["ZA"] = "Afrika Selatan";
		$country["AL"] = "Albania";
		$country["DZ"] = "Algeria";
		$country["US"] = "Amerika Serikat";
		$country["AO"] = "Angola";
		$country["SA"] = "Arab Saudi";
		$country["AR"] = "Argentina";
		$country["AM"] = "Armenia";
		$country["AU"] = "Australia";
		$country["AT"] = "Austria";
		$country["AZ"] = "Azerbaijan";
		$country["BH"] = "Bahrain";
		$country["BD"] = "Bangladesh";
		$country["BY"] = "Belarus";
		$country["BJ"] = "Benin";
		$country["BO"] = "Bolivia";
		$country["BW"] = "Botswana";
		$country["BR"] = "Brasil";
		$country["BN"] = "Brunei";
		$country["BG"] = "Bulgaria";
		$country["CL"] = "Cile";
		$country["CI"] = "C�te d'Ivoire";
		$country["SV"] = "El Salvador";
		$country["FJ"] = "Fiji";
		$country["PH"] = "Filipina";
		$country["GE"] = "Georgia";
		$country["GU"] = "Guam";
		$country["GT"] = "Guatemala";
		$country["GN"] = "Guinea";
		$country["GY"] = "Guyana";
		$country["HT"] = "Haiti";
		$country["HN"] = "Honduras";
		$country["IN"] = "India";
		$country["ID"] = "Indonesia";
		$country["GB"] = "Inggris";
		$country["IQ"] = "Irak";
		$country["IE"] = "Irlandia";
		$country["IM"] = "Isle of Man";
		$country["IT"] = "Italia";
		$country["DE"] = "Jerman";
		$country["KH"] = "Kamboja";
		$country["CM"] = "Kamerun";
		$country["CA"] = "Kanada";
		$country["KZ"] = "Kazakstan";
		$country["KE"] = "Kenya";
		$country["KG"] = "Kirgizstan";
		$country["CO"] = "Kolombia";
		$country["KR"] = "Korea Selatan";
		$country["HR"] = "Kroatia";
		$country["KW"] = "Kuwait";
		$country["LA"] = "Laos";
		$country["LB"] = "Lebanon";
		$country["LR"] = "Liberia";
		$country["LT"] = "Lituania";
		$country["MG"] = "Madagaskar";
		$country["MK"] = "Makedonia";
		$country["MV"] = "Maladewa";
		$country["MY"] = "Malaysia";
		$country["MA"] = "Maroko";
		$country["MU"] = "Mauritius";
		$country["EG"] = "Mesir";
		$country["MD"] = "Moldova";
		$country["ME"] = "Montenegro";
		$country["MZ"] = "Mozambik";
		$country["NA"] = "Namibia";
		$country["NG"] = "Nigeria";
		$country["NI"] = "Nikaragua";
		$country["PK"] = "Pakistan";
		$country["PS"] = "Palestina";
		$country["PA"] = "Panama";
		$country["PY"] = "Paraguay";
		$country["PE"] = "Peru";
		$country["PL"] = "Polandia";
		$country["PT"] = "Portugal";
		$country["PR"] = "Puerto Riko";
		$country["QA"] = "Qatar";
		$country["CZ"] = "Republik Cheska";
		$country["CD"] = "Republik Demokrasi Kongo";
		$country["DO"] = "Republik Dominika";
		$country["CG"] = "Republik Kongo";
		$country["RO"] = "Rumania";
		$country["RU"] = "Rusia";
		$country["RW"] = "Rwanda";
		$country["NZ"] = "Selandia Baru";
		$country["SN"] = "Senegal";
		$country["RS"] = "Serbia";
		$country["SC"] = "Seychelles";
		$country["SL"] = "Sierra Leone";
		$country["SG"] = "Singapura";
		$country["SI"] = "Slovenia";
		$country["ES"] = "Spanyol";
		$country["LK"] = "Sri Lanka";
		$country["SD"] = "Sudan";
		$country["SE"] = "Swedia";
		$country["TW"] = "Taiwan";
		$country["TJ"] = "Tajikistan";
		$country["TZ"] = "Tanzania";
		$country["TH"] = "Thailand";
		$country["TT"] = "Trinidad dan Tobago";
		$country["TN"] = "Tunisia";
		$country["TR"] = "Turki";
		$country["UG"] = "Uganda";
		$country["UA"] = "Ukraina";
		$country["AE"] = "Uni Emirat Arab";
		$country["UY"] = "Uruguay";
		$country["UZ"] = "Uzbekistan";
		$country["VN"] = "Vietnam";
		$country["YE"] = "Yemen";
		$country["JO"] = "Yordania";
		$country["GR"] = "Yunani";
		
		return $country;
	}
	
	function getState($country="") {
		
		$state = array();
		
		switch($country)
		{
			case "ID":
			$state['11'] = "Aceh, D.I.";
			$state['51'] = "Bali";
			$state['Bangka'] = "Bangka Belitung";
			$state['36'] = "Banten";
			$state['17'] = "Bengkulu";
			$state['75'] = "Gorontalo";
			$state['31'] = "Jakarta, DKI";
			$state['15'] = "Jambi";
			$state['32'] = "Jawa Barat";
			$state['33'] = "Jawa Tengah";
			$state['35'] = "Jawa Timur";
			$state['61'] = "Kalimantan Barat";
			$state['63'] = "Kalimantan Selatan";
			$state['62'] = "Kalimantan Tengah";
			$state['64'] = "Kalimantan Timur";
			$state['19'] = "Kep Bangka Belitung";
			$state['21'] = "Kep Riau";
			$state['18'] = "Lampung";
			$state['81'] = "Maluku";
			$state['82'] = "Maluku Utara";
			$state['52'] = "Nusa Tenggara Barat";
			$state['53'] = "Nusa Tenggara Timur";
			$state['91'] = "Papua Barat";
			$state['92'] = "Papua Tengah";
			$state['Paptim'] = "Papua Timur";
			$state['14'] = "Riau";
			$state['73'] = "Sulawesi Selatan";
			$state['72'] = "Sulawesi Tengah";
			$state['74'] = "Sulawesi Tenggara";
			$state['71'] = "Sulawesi Utara";
			$state['76'] = "Sulawesi Barat";
			$state['13'] = "Sumatra Barat";
			$state['16'] = "Sumatra Selatan";
			$state['12'] = "Sumatra Utara";
			$state['34'] = "Yogyakarta, D.I.";
			break;
			
			default:
			$state[''] = "Pilih Negara Terlebih Dahulu";
			break;
		
		}
		return $state;
	}
	
	function getCity($country="",$state="") {
		
		$kab = array();
		
		if($country == "ID")
		{
			//indonesia city
			switch($state){
			case "11":
				$city['013'] = "kabupaten aceh selatan";
				$city['021'] = "kabupaten aceh tenggara";
				$city['036'] = "kabupaten aceh timur";
				$city['044'] = "kabupaten aceh tengah";
				$city['052'] = "kabupaten aceh barat";
				$city['067'] = "kabupaten aceh besar";
				$city['075'] = "kabupaten pidie";
				$city['083'] = "kabupaten aceh utara";
				$city['091'] = "kabupaten bireun";
				$city['102'] = "kabupaten aceh singkil";
				$city['117'] = "kabupaten simeulue";
				$city['156'] = "kabupaten nagan raya";
				$city['713'] = "kota banda aceh";
				$city['721'] = "kota sabang";
				$city['736'] = "kota lhokseumawe";
				$city['744'] = "kota langsa";
				break; 
				
				case "12":
				$city['016'] = "kabupaten nias";
				$city['024'] = "kabupaten tapanuli selatan";
				$city['032'] = "kabupaten tapanuli tengah";
				$city['047'] = "kabupaten tapanuli utara";
				$city['055'] = "kabupaten labuhan batu";
				$city['063'] = "kabupaten asahan";
				$city['071'] = "kabupaten simalungun";
				$city['086'] = "kabupaten dairi";
				$city['094'] = "kabupaten karo";
				$city['105'] = "kabupaten deli serdang";
				$city['113'] = "kabupaten langkat";
				$city['121'] = "kabupaten toba samosir";
				$city['136'] = "kabupaten mandailing natal";
				$city['716'] = "kota sibolga";
				$city['724'] = "kota tanjung balai";
				$city['732'] = "kota p. siangtar";
				$city['747'] = "kota tebingtinggi";
				$city['755'] = "kota medan";
				$city['763'] = "kota binjai";
				$city['771'] = "kota p. sidempuan";
				break;
				
				case "13":
				$city['012'] = "kabupaten pesisir selatan";
				$city['027'] = "kabupaten solok";
				$city['035'] = "kabupaten sawah lunto";
				$city['043'] = "kabupaten tanah datar";
				$city['051'] = "kabupaten padang pariaman";
				$city['065'] = "kabupaten agam";
				$city['074'] = "kabupaten limapuluhkota";
				$city['082'] = "kabupaten pasaman";
				$city['097'] = "kabupaten kep mentawai";
				$city['046'] = "kabupaten dharmasraya";
				$city['712'] = "kota padang";
				$city['727'] = "kota solok";
				$city['735'] = "kota sawah lunto";
				$city['743'] = "kota padang panjang";
				$city['751'] = "kota bukit tinggi";
				$city['765'] = "kota payakumbuh";
				$city['774'] = "kota pariaman";
				break;
				
				case "14":
				$city['015'] = "kabupaten indragiri hulu";
				$city['023'] = "kabupaten indragiri hilir";
				$city['031'] = "kabupaten kepulauan riau";
				$city['046'] = "kabupaten kampar";
				$city['054'] = "kabupaten bengkalis";
				$city['062'] = "kabupaten karimun";
				$city['077'] = "kabupatenkuantan singingi";
				$city['085'] = "kabupaten Natuna";
				$city['093'] = "kabupaten siak";
				$city['104'] = "kabupaten rokan hilir";
				$city['112'] = "kabupaten rokan hulu";
				$city['127'] = "kabupaten pelalawan";
				$city['715'] = "kota pekanbaru";
				$city['723'] = "kota batam";
				$city['731'] = "kota dumai";
				$city['746'] = "kota tanjung pinang";
				break;
				
				case "15":
				$city['011'] = "kabupaten kerinci";
				$city['026'] = "kabupaten bangko sarolangun";
				$city['034'] = "kabupaten batanghari";
				$city['065'] = "kabupaten sarolangun";
				$city['073'] = "kabupaten merangin";
				$city['081'] = "kabupaten tj. jabung timur";
				$city['096'] = "kabupaten tj. jabung barat";
				$city['107'] = "kabupaten muaro jambi";
				$city['115'] = "kabupaten bungo";
				$city['123'] = "kabupaten tebo";
				$city['711'] = "kota jambi";
				break;
				
				case "16":
				$city['014'] = "kabupaten ogan komering Ulu";
				$city['022'] = "kabupaten ogan komering Ilir";
				$city['037'] = "kabupaten muara enim";
				$city['045'] = "kabupaten lahat";
				$city['053'] = "kabupaten musi rawas";
				$city['061'] = "kabupaten musi banyuasin";
				$city['714'] = "kota palembang";
				$city['737'] = "kota prabumulih";
				$city['745'] = "kota pagaralam";
				$city['753'] = "kota lubuk linggau";
				break;
				
				case "17":
				$city['017'] = "kabupaten bengkulu selatan, kab bengkulu selatan";
				$city['025'] = "kabupaten rejang lebong, kab rejang lebong";
				$city['033'] = "kabupaten bengkulu utara, kab bengkulu utara";
				$city['717'] = "kota bengkulu, bengkulu";
				break;
				
				case "18":
				$city['013'] = "kab lampung selatan";
				$city['021'] = "kab lampung tengah";
				$city['036'] = "kab lampung utara";
				$city['044'] = "kab lampung barat";
				$city['052'] = "kab tanggamus";
				$city['067'] = "kab tulang bawang";
				$city['075'] = "kab lampung timur";
				$city['083'] = "kab way kanan";
				$city['713'] = "kota bandar lampung";
				$city['721'] = "kota metro";
				break;
				
				case "19":
				$city['016'] = "kabupaten bangka";
				$city['024'] = "kabupaten belitung";
				$city['716'] = "kota pangkal pinang";
				break;
				
				case "30":
				$city['014'] = "kabupaten pandeglang";
				$city['022'] = "kabupaten lebak";
				$city['037'] = "kabupaten tanggerang";
				$city['045'] = "kabupaten serang";
				$city['714'] = "kota tanggerang";
				$city['722'] = "kota cilegon";
				break;
				
				case "31":
				$city['01'] = "administratif kepulauan seribu";
				$city['733'] = "jakarta Pusat";
				$city['756'] = "jakarta utara";
				$city['741'] = "jakarta barat";
				$city['717'] = "jakarta selatan";
				$city['725'] = "jakarta timur";
				break;
				
				case "32":
				$city['036'] = "kabupaten bogor";
				$city['044'] = "kabupaten sucityumi";
				$city['052'] = "kabupaten cianjur";
				$city['067'] = "kabupaten bandung";
				$city['075'] = "kabupaten garut";
				$city['083'] = "kabupaten tasikmalaya";
				$city['091'] = "kabupaten ciamis";
				$city['102'] = "kabupaten kuningan";
				$city['117'] = "kabupaten cirebon";
				$city['125'] = "kabupaten majalengka";
				$city['133'] = "kabupaten sumedang";
				$city['141'] = "kabupaten indramayu";
				$city['156'] = "kabupaten subang";
				$city['164'] = "kabupaten purwakarta";
				$city['172'] = "kabupaten karawang";
				$city['187'] = "kabupaten bekasi";
				$city['000'] = "kabupaten bandung barat";
				$city['713'] = "kota bogor";
				$city['721'] = "kota sucityumi";
				$city['736'] = "kota bandung";
				$city['744'] = "kota cirebon";
				$city['767'] = "kota bekasi";
				$city['775'] = "kota depok";
				$city['783'] = "kota cimahi";
				$city['791'] = "kota tasikmalaya";
				$city['000'] = "kota banjar";
				break;
				
				case "33":
				$city['016'] = "kabupaten cilacap";
				$city['024'] = "kabupaten banyumas";
				$city['032'] = "kabupaten purbalingga";
				$city['047'] = "kabupaten banjarnegara";
				$city['055'] = "kabupaten kebumen";
				$city['063'] = "kabupaten purworejo";
				$city['071'] = "kabupaten wonosobo";
				$city['086'] = "kabupaten magelang";
				$city['094'] = "kabupaten boyolali";
				$city['105'] = "kabupaten klaten";
				$city['113'] = "kabupaten sukoharjo";
				$city['121'] = "kabupaten wonogiri";
				$city['136'] = "kabupaten karanganyar";
				$city['144'] = "kabupaten sragen";
				$city['152'] = "kabupaten grobogan";
				$city['167'] = "kabupaten blora";
				$city['175'] = "kabupaten rembang";
				$city['183'] = "kabupaten pati";
				$city['191'] = "kabupaten kudus";
				$city['202'] = "kabupaten jepara";
				$city['217'] = "kabupaten demak";
				$city['225'] = "kabupaten semarang";
				$city['233'] = "kabupaten temanggung";
				$city['241'] = "kabupaten kendal";
				$city['256'] = "kabupaten batang";
				$city['264'] = "kabupaten pekalongan";
				$city['272'] = "kabupaten pemalang";
				$city['287'] = "kabupaten tegal";
				$city['295'] = "kabupaten brebes";
				$city['716'] = "kota magelang";
				$city['724'] = "kota surakarta";
				$city['732'] = "kota salatiga";
				$city['747'] = "kota semarang";
				$city['757'] = "kota pekalongan";
				$city['763'] = "kota tegal";
				break;
				
				case "34":
				$city['012'] = "kab kulonprogo";
				$city['027'] = "kab bantul";
				$city['035'] = "kab gunung kidul";
				$city['043'] = "kab sleman";
				$city['712'] = "kota yogyakarta";
				break;
				
				case "35":
				$city['015'] = "kabupaten pacitan";
				$city['023'] = "kabupaten ponorogo";
				$city['031'] = "kabupaten trenggalek";
				$city['046'] = "kabupaten tulungagung";
				$city['054'] = "kabupaten blitar";
				$city['062'] = "kabupaten kediri";
				$city['077'] = "kabupaten malang";
				$city['085'] = "kabupaten lumajang";
				$city['093'] = "kabupaten jember";
				$city['104'] = "kabupaten banyuwangi";
				$city['112'] = "kabupaten bondowoso";
				$city['127'] = "kabupaten situbondo";
				$city['135'] = "kabupaten probolinggo";
				$city['143'] = "kabupaten pasuruan";
				$city['151'] = "kabupaten sidoarjo";
				$city['166'] = "kabupaten mojokerto";
				$city['174'] = "kabupaten jombang";
				$city['182'] = "kabupaten nganjuk";
				$city['197'] = "kabupaten madiun";
				$city['201'] = "kabupaten mangetan";
				$city['216'] = "kabupaten ngawi";
				$city['224'] = "kabupaten bojonegoro";
				$city['232'] = "kabupaten tuban";
				$city['247'] = "kabupaten lamongan";
				$city['255'] = "kabupaten gresik";
				$city['263'] = "kabupaten bangkalan";
				$city['271'] = "kabupaten sampang";
				$city['286'] = "kabupaten pamekasan";
				$city['294'] = "kabupaten sumenep";
				$city['715'] = "kota kediri";
				$city['723'] = "kota blitar";
				$city['731'] = "kota malang";
				$city['746'] = "kota probolinggo";
				$city['754'] = "kota pasuruan";
				$city['762'] = "kota mojokerto";
				$city['777'] = "kota madiun";
				$city['785'] = "kota surabaya";
				$city['793'] = "kota batu";
				break;
				
				case "51":
				$city['014'] = "kab jembrana";
				$city['022'] = "kab tabanan";
				$city['037'] = "kab badung";
				$city['045'] = "kab gianyar";
				$city['053'] = "kab klungkung";
				$city['061'] = "kab bangli";
				$city['076'] = "kab karangasem";
				$city['084'] = "kab buleleng";
				$city['714'] = "kota denpasar";
				break;
				
				case "52":
				$city['017'] = "kabupaten lombok barat";
				$city['025'] = "kabupaten lombok tengah";
				$city['033'] = "kabupaten lombok timur";
				$city['041'] = "kabupaten sumbawa";
				$city['026'] = "kabupaten dompu";
				$city['064'] = "kabupaten bima";
				$city['717'] = "kota mataram";
				break;
				
				case "53":
				$city['013'] = "kabupaten sumba barat";
				$city['021'] = "kabupaten sumba timur";
				$city['036'] = "kabupaten kupang";
				$city['044'] = "kabupaten timur tengah selatan";
				$city['052'] = "kabupaten timur tengah utara";
				$city['067'] = "kabupaten belu";
				$city['075'] = "kabupaten alor";
				$city['083'] = "kabupaten flores timur";
				$city['091'] = "kabupaten sikka";
				$city['102'] = "kabupaten ende";
				$city['117'] = "kabupaten ngada";
				$city['125'] = "kabupaten manggarai";
				$city['133'] = "kabupaten lembata";
				$city['711'] = "kota kupang";
				break;
				
				case "61":
				$city['016'] = "kabupaten sambas";
				$city['024'] = "kabupaten pontianak";
				$city['032'] = "kabupaten sanggau";
				$city['047'] = "kabupaten ketapang";
				$city['055'] = "kabupaten sintang";
				$city['063'] = "kabupaten kapuas hulu";
				$city['071'] = "kabupaten bengkayang";
				$city['086'] = "kabupaten landak";
				$city['716'] = "kota pontianak";
				$city['724'] = "kota singkawang";
				break;
				
				case "62":
				$city['012'] = "kab kota waringin barat";
				$city['027'] = "kab kota waringin timur";
				$city['035'] = "kab katingan";
				$city['043'] = "kab kapuas";
				$city['051'] = "kab barito selatan";
				$city['074'] = "kab barito utara";
				$city['082'] = "kab gunung mas";
				$city['097'] = "kab murung raya";
				$city['101'] = "kab seruyan";
				$city['712'] = "kota palangkaraya";
				break;
				
				case "63":
				$city['015'] = "kabupaten tanah laut";
				$city['023'] = "kabupaten kota baru";
				$city['031'] = "kabupaten banjar";
				$city['046'] = "kabupaten barito kuala";
				$city['054'] = "kabupaten tapin";
				$city['062'] = "kabupaten hulu sei selatan";
				$city['077'] = "kabupaten hulu sei tengah";
				$city['085'] = "kabupaten hulu sei utara";
				$city['093'] = "kabupaten tabalong";
				$city['715'] = "kota banjarmasin";
				$city['723'] = "kota banjar baru";
				break;
				
				case "64":
				$city['011'] = "kabupaten pasir";
				$city['026'] = "kabupaten kutai";
				$city['034'] = "kabupaten berau";
				$city['042'] = "kabupaten bulungan";
				$city['057'] = "kabupaten nunukan";
				$city['065'] = "kabupaten kutai timur";
				$city['073'] = "kabupaten malinau";
				$city['081'] = "kabupaten kutai barat";
				$city['711'] = "kota balikpapan";
				$city['726'] = "kota samarinda";
				$city['734'] = "kota tarakan";
				$city['742'] = "kota bontang";
				break;
				
				case "70":
				$city['015'] = "kabupaten gorontalo";
				$city['023'] = "kabupaten boalemo";
				$city['715'] = "kota gorontalo";
				break;
				
				case "71":
				$city['026'] = "kabupaten bolaang mongondow";
				$city['034'] = "kabupaten minahasa";
				$city['042'] = "kabupaten sangihe talaud";
				$city['726'] = "kota manado";
				$city['734'] = "kota bitung";
				break;
				
				case "72":
				$city['014'] = "luwuk/banggal";
				$city['022'] = "kabupaten poso";
				$city['037'] = "kabupaten donggala";
				$city['053'] = "kabupaten banggal kepulauan";
				$city['061'] = "kabupaten banggal";
				$city['076'] = "kabupaten buol";
				$city['084'] = "kabupaten toli-toli";
				$city['092'] = "kabupaten morowali";
				$city['714'] = "kota palu, palu";
				break;
				
				case "73":
				$city['017'] = "kabupaten selayar";
				$city['025'] = "kabupaten bulukumba";
				$city['033'] = "kabupaten bantaeng";
				$city['041'] = "kabupaten jeneponto";
				$city['056'] = "kabupaten takalar";
				$city['064'] = "kabupaten gowa";
				$city['072'] = "kabupaten sinjai";
				$city['087'] = "kabupaten bone";
				$city['095'] = "kabupaten maros";
				$city['106'] = "kabupaten pangk ajene kepulauan";
				$city['114'] = "kabupaten barru";
				$city['122'] = "kabupaten soppeng";
				$city['137'] = "kabupaten wajo";
				$city['145'] = "kabupaten sidenreng rappang";
				$city['153'] = "kabupaten pinrang";
				$city['161'] = "kabupaten enrekang";
				$city['176'] = "kabupaten luwu";
				$city['184'] = "kabupaten tana toraja";
				$city['192'] = "kabupaten polewali mamasa";
				$city['203'] = "kabupaten majene";
				$city['211'] = "kabupaten mamuju";
				$city['226'] = "kabupaten luwu utara";
				$city['717'] = "kota makassar";
				$city['725'] = "kota pare-pare";
				$city['331'] = "kota palopo";
				break;
				
				case "74":
				$city['013'] = "kabupaten buton";
				$city['021'] = "kabupaten muna";
				$city['036'] = "kabupaten kendari";
				$city['044'] = "kabupaten kolaka";
				$city['713'] = "kota kendari";
				$city['721'] = "kota bau-bau";
				break;
				
				case "81":
				$city['013'] = "kabupaten maluku tenggara";
				$city['021'] = "kabupaten maluku tengah";
				$city['052'] = "kabupaten buru";
				$city['067'] = "kabupaten maluku tenggara barat";
				$city['713'] = "kota ambon";
				break;
				
				case "82":
				$city['044'] = "kabupaten maluku utara";
				$city['036'] = "kabupaten halmahera tengah";
				$city['715'] = "kota ternate";
				break;
				
				default:
				$city[''] = "Pilih Provinsi Terlebih Dahulu";
				break;
			}
		}
		else
		{
			$city[''] = "Pilih Provinsi Terlebih Dahulu";
		}
		 
		return $city;
	}
	
	function getRegional($post)
	{
		$getCountry = $this->getCountry();
		$getState = $this->getState($post['country']);
		$getCity = $this->getCity($post['country'],$post['state']);
		
		$regional = array();
		$regional['negara'] = $getCountry[$post['country']];
		if(empty($getState[$post['state']]))
		{
			$regional['provinsi'] = $post['state'];
		}
		else
		{
			$regional['provinsi'] = $getState[$post['state']];
		}
		if(empty($getCity[$post['city']]))
		{
			$regional['kota'] = $post['city'];
		}
		else
		{
			$regional['kota'] = $getCity[$post['city']];
		}
		
		return $regional;
	}
	
	//this function to get key from values option
	function getKeyRegional($post)
	{
		$getCountry = $this->getCountry();
		
		$regional = array();
		foreach($getCountry as $key => $val)
		{
			if($val == $post['country'])
			{
				$regional['country'] = $key;
				break;
			}
		}
		
		if(empty($regional['country']))
		{
			$regional['country'] == $post['country'];
		}
		
		$getState = $this->getState($regional['country']);
		foreach($getState as $key => $val)
		{
			if($val == $post['state'])
			{
				$regional['state'] = $key;
			}
		}
		
		if(empty($regional['state']))
		{
			$regional['state'] = $post['state'];
		}
		$getCity = $this->getCity($regional['country'],$regional['state']);
		foreach($getCity as $key => $val)
		{
			if($val == $post['city'])
			{
				$regional['city'] = $key;
			}
		}
		
		if(empty($regional['city']))
		{
			$regional['city'] = $post['city'];
		}

		return $regional;
		
	}
	
}

?>