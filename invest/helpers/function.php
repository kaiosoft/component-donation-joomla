<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
class investHelper{
	
/* ======================================= Recent Function Mei 2012 (Updated) ====================================== */

	function __construct()
	{
		$this->cekStatusPlan();
	}
	
	function cekStatusPlan()
	{
		$rows = $this->getDataByParam("status","='Active'","#__invest_member_plan","Array");
		foreach($rows as $row)
		{
			//range date
			$range_date = explode(";",$row->range_date);
			
			//total count
			$total = count($range_date);
			
			//finish date
			$finish_date = $range_date[$total-1];
			
			//now
			$now = date("Y-m-d");
			
			if($now >= $finish_date)
			{
				$this->updateStatusPlan($row->id);
			}
		}
	
	}
	
	function updateStatusPlan($id)
	{
		$db =& JFactory::getDBO();
		$sql = "UPDATE #__invest_member_plan SET status='Complete' WHERE id='".$id."'";
		$db->setQuery($sql);
		$db->query();
	}

	//get Config
	function getConfig($config)
	{
		$db =& JFactory::getDBO();
		$sql = "SELECT * FROM #__invest_config WHERE config='".$config."'";
		$db->setQuery($sql);
		$row = $db->loadObject();
		return $row->value;
	}
	
	//get Randomize
	function getRandomize($nilai,$hari)
	{
		$angka = $nilai/$hari;
		
		//pembagi ke pecahan sebenarnya
		$pembagi = 100 / $angka;
		
		$persen = array();
		for($i=1;$i<=$hari;$i++)
		{
			if($i%2 == 0)
			{
			$old = $persen[$i-1];
			$persen[$i] = ($angka - $old)+$angka;
			}
			else
			{
			$persen[$i] = $this->doRandomize()/$pembagi;
			}
		}
		
		return $persen;
	}
	
	//for getRandomize
	function doRandomize()
	{
		return $angkaRandom = rand(50,100);
	}
	
	//shuffle array position
	function randomizeArray($array)
	{
		$jumlah = count($array);
		$shuffle = array_rand($array,$jumlah);
		
		$newArray = array();
		for($i=0;$i<$jumlah;$i++)
		{
			$newArray[] = $array[$shuffle[$i]];
		}
		
		return $newArray;
	}
	
	//General Function for get array data or single by multiple parameter
	function getDataByParam($param,$value,$table,$show="single",$order="",$att=" && ")
	{
		$filter = $this->setFilter($param,$value,$att);
		$db =& JFactory::getDBO();
		$sql = "SELECT * FROM ".$table.$filter.$order;
		$db->setQuery($sql);
		if($show=="single")
		{
			$rows = $db->loadObject();
		}
		else
		{
			$rows = $db->loadObjectList();
		}
		return $rows;
	}
	
	function removeDataByParam($param,$value,$table)
	{
		$filter = $this->setFilter($param,$value);
		$db =& JFactory::getDBO();
		$sql = "Delete FROM ".$table.$filter;
		$db->setQuery($sql);
		$db->query();
	}
	
	//General filter with complete sign like [=,',(,)]
	/*example 
	$params = array("id","name");
	$values = array("='1'","='kaio'");
	*/
	function setFilter($params,$values,$delimiter=" && ")
	{
		$filter = " WHERE ";
		if(is_array($values))
		{
			for($i=0;$i<count($values);$i++)
			{
				if((!empty($params[$i])) && (!empty($values[$i])) && (strlen($values[$i]) > 3))
				{
					if(strlen($filter) <= 7)
					{
					$filter .= $params[$i].$values[$i];
					}
					else
					{
					$filter .= $delimiter.$params[$i].$values[$i];
					}
				}
			}
		}
		else
		{
			if((!empty($params)) && (!empty($values)) && (strlen($values) > 3))
			{
				$filter .= $params.$values;
			}
		}
		
		if(strlen($filter) <= 8)
		{
			$filter = "";
		}
		
		return $filter;
	}
	
	//unset where filter
	function unsetWhereFilter($filter)
	{
		$filter = str_replace("WHERE","&&",$filter);
		return $filter;
	}
	
	function registerUser($post,$group_id="2",$returnLink="index.php")
	{
		$db 	=& JFactory::getDBO();
		$app	=& JFactory::getApplication();
	
		// Create a new JUser object
		$user = new JUser(JRequest::getVar( 'id', 0, 'post', 'int'));
			
		if($post['id']==0){
			// default password untuk guru baru
			$getPass = $this->getRandom(6,"random");
			$post['password']	= $getPass;
			$post['password2']	= $getPass;
		} else {
			$post['password']	= JRequest::getVar('password', '', 'post', 'string', JREQUEST_ALLOWRAW);
			$post['password2']	= JRequest::getVar('password2', '', 'post', 'string', JREQUEST_ALLOWRAW);
		}

		
		if (!$user->bind($post))
		{
			$app->enqueueMessage(JText::_('CANNOT SAVE THE USER INFORMATION'), 'message');
			$app->enqueueMessage($user->getError(), 'error');
			$link = $returnLink;
			return $app->redirect($link, $msg);
			exit();
		}
			
		/*
		 * Lets save the JUser object
		*/ 
			
		if (!$user->save())
		{
			$app->enqueueMessage(JText::_('CANNOT SAVE THE USER INFORMATION'), 'message');
			$app->enqueueMessage($user->getError(), 'error'); // next version Error Handling
			$link = $returnLink;
			return $app->redirect($link, $msg);
			exit();
		}			
		
		else
		{
			// get user id
			$sql = "SELECT id FROM #__users WHERE email='".$post['email']."' && username = '".$post['username']."'";
			$db->setQuery($sql);
			$post['user_id'] = $db->loadResult();
			
			//insert user_group
			$sql = "INSERT INTO #__user_usergroup_map (`user_id`, `group_id`) VALUES ('".$post['user_id']."','".$group_id."')";
			$db->setQuery($sql);
			$RESql = $db->query();
			
			$data = array();
			$data['user_id'] = $post['user_id'];
			$data['password'] = $getPass;
			
			return $data;
		}
			
	}
	
	function updateNameUsers($name,$id)
	{
		$db =& JFactory::getDBO();
		$sql = "UPDATE #__users SET name='".$name."' WHERE id='".$id."'";
		$db->setQuery($sql);
		$db->query();
	}
	
	function br2nl($string)
	{
		$total = substr_count($string,"<br />");
		for($i=0;$i<$total;$i++)
		{
			$string = str_replace("<br />","\n",$string);
		}
		return $string;
	}
	
	//send email
	public function sendMail($recipient="",$senderEmail="",$senderName="",$subject="",$msg="",$html="false")
	{
		$mailer =& JFactory::getMailer();
		
		$sender = array($senderEmail,$senderName);
		$mailer->setSender($sender);
		$mailer->addRecipient($recipient);
		$mailer->isHTML($html);
		if(strtolower($html) == "true")
		{
			$mailer->Encoding = 'base64';
		}
		$mailer->setSubject($subject);
		$mailer->setBody($msg);
				
		$send =& $mailer->Send();
		if ( $send !== true ) 
		{
			$text ='Terjadi masalah dalam mengirim email';
		} else {
			$text ='Kami telah mengirimkan anda email pemberitahuan';
		}
		
		return $text;
	}
	
	function getRandom($length,$type="")
	{
		if($type == "number")
		{
			$first = substr(11111111111,0,$length);
			$last = substr(99999999999,0,$length);
			$result = rand($first,$last);
		}
		else
		{
			$rand = base_convert(mt_rand(60466176, 2147483647), 10, 36);
			$result = substr($rand,0,$length);
		}
		return $result;
	}
	
	//jarak waktu like 12 jam lagi, //update 28 mei 2012
	function voidTime($datetime)
	{
		
		/* prepare date void */
		$date = substr($datetime,0,10);
		$time = substr($datetime,11);
		
		//declare date
		$tgl = substr($date,8);
		$bulan = substr($date,5,2);
		$tahun = substr($date,0,4);
		
		//declare time
		$jam = substr($time,0,2);
		$menit = substr($time,3,2);
		$detik = substr($time,6,2);
		
		
		/* prepare date now */
		$nowDate = date("Y-m-d");
		$nowTime = date("H:i:s");
		
		//declare now date
		$nowTgl = substr($nowDate,8);
		$nowBulan = substr($nowDate,5,2);
		$nowTahun = substr($nowDate,0,4);
		
		//declare now time
		$nowJam = substr($nowTime,0,2);
		$nowMenit = substr($nowTime,3,2);
		$nowDetik = substr($nowTime,6,2);
		
		$vMenit = $menit - $nowMenit;
		$vJam = $jam - $nowJam;
		$vTgl = $tgl - $nowTgl;
		$vBulan = $bulan - $nowBulan;
		$vTahun = $tahun - $nowTahun;
		
		if($vMenit < 0)
		{
			$vMenit = $vMenit + 60;
			$vJam - 1;
		}
		
		if($vJam < 0)
		{
			$vJam = $vJam + 24;
			$vTgl = $vTgl - 1;
		}
		
		if($vTgl > $this->cekJumlahHari())
		{
			$vTgl = $vTgl - $this->cekJumlahHari();
			$vBulan = $vBulan + 1;
		}
		else if($vTgl >= 0)
		{
			$vTgl = $vTgl;
		}
		else
		{
			$vTgl = $this->cekJumlahHari() - $vTgl;
			$vBulan = $vBulan - 1;
		}
		
		if($vBulan > 12)
		{
			$vBulan = $vBulan - 12;
			$vTahun = $vTahun + 1;
		}
		
		$data = array();
		$mode = 0;
		
		if($vTahun > 0)
		{
			echo "tahun > 0";
			$mode = $mode + 1;
			$data['date'][] = $vTahun." Years ";
		}
		else
		{
			if($vTahun < 0)
			{
			$mode = $mode - 1;
			}
		}
		if($vBulan > 0)
		{
			$mode = $mode + 1;
			$data['date'][] = $vBulan." Months ";
		}
		else
		{
			if(vBulan < 0)
			{
				$mode = $mode - 1;
			}
		}
		if($vTgl > 0)
		{
			$mode = $mode + 1;
			$data['date'][] = $vTgl." Days ";
		}
		else
		{
			if($vTgl < 0)
			{
				$mode = $mode- 1;
			}
		}
		if($vJam > 0)
		{
			$mode = $mode + 1;
			$data['date'][] = $vJam." Hours ";
		}
		else
		{
			if($vJam < 0)
			{
				$mode = $mode -1;
			}
		}
		if($vMenit > 0)
		{
			$mode = $mode + 1;
			$data['date'][] = $vMenit." Minutes ";
		}
		else
		{
			if($vMenit < 0)
			{
				$mode = $mode -1;
			}
		}
		
		if($mode > 0)
		{
			$data['text'] = " more";
		}
		else
		{
			$data['text'] = " ago";
		}
		return $data;
	}
	
	//mengecek jumlah hari bulan ini, update 28 mei 2012
	function cekJumlahHari($nowBulan="",$nowTahun="")
	{
		if(empty($nowBulan))
		{
			$nowBulan = date('m');
		}
		
		if(empty($nowTahun))
		{
			$nowTahun = date('Y');
		}
		
		
		//calender perhitungan tgl
		$jumHari = cal_days_in_month(CAL_GREGORIAN, $nowBulan, $nowTahun);
		
		return $jumHari;
	}
	
	//mendapatkan range tanggal berdasarkan jumlah, 28 mei 2012
	function getRangeDate($jumlah)
	{
		$jmlhHari = $this->cekJumlahHari();
		$now = date("d");
		$bulan = date("m");
		$tahun = date("Y");
		$range_date = array();
		for($i=0;$i<$jumlah;$i++)
		{
			if($i == 0)
			{
				$hari = $jmlhHari - $now;
				$tgl = $now + 1;
			}
			
			if($hari == 0)
			{
				$bulan = $bulan + 01;
				if($bulan > 12)
				{
					$bulan = 01;
					$tahun = $tahun + 1;
				}
				
				if(strlen($bulan) == 1)
				{
					$bulan = "0".$bulan;
				}
				
				$hari = $this->cekJumlahHari($bulan,$tahun);
				$tgl = 1;
			}
			
			//mengecek hari apa pada tgl tersebut
			$x = mktime(0, 0, 0, $bulan, $tgl, $tahun); 
			$wHari = date("w",$x);
			
			//untuk sabtu dan minggu di skip dengan kata lain tidak di masukkan ke dalam range
			if($wHari == 6)
			{
				$tgl = $tgl + 2;
				$hari = $hari - 2;
			}
			else if($wHari == 0)
			{
				$tgl = $tgl + 1;
				$hari = $hari - 1;
			}
			else
			{
				$tgl = $tgl;
			}
			
			if(strlen($tgl) == 1)
			{
				$tgl = "0".$tgl;
			}
			
			$range_date[] = $tahun."-".$bulan."-".$tgl;
			
			$tgl++;
			$hari--;
			
		}
		
		return $range_date;
	}
	
	function getProfitInvest($data)
	{
		$persen_profit = explode(";",$data->persen_profit);
		$range_date = explode(";",$data->range_date);

		//get plan
		$plan = $this->getDataByParam("id","='".$data->plan_id."'","#__invest_plan");

		
		if($data->status != "Complete")
		{
			//show range date by date
			$showDate = array();
			$showProfit = array();
			$now = date("Y-m-d");
			$jam = date("H");
			$i = 0;
			foreach($range_date as $row)
			{
			$rowProfit =& $persen_profit[$i];
			
				if($row < $now)
				{
					$showDate[] = $row;
					$showProfit[] = $rowProfit;
				}
				else if(($row == $now) && ($jam >= 16))
				{
					$showDate[] = $row;
					$showProfit[] = $rowProfit;
				}
				else
				{
					break;
				}
				$i++;
			}
			
			$jumlahHari = count($showDate);
			$jumlahProfit = array_sum($showProfit);

			$profitMember = $data->profit;
			$profitInvest = $this->profitInvest($data->investasi,$data->persen_profit);
			$withdrawal = $profitInvest - $profitMember;
			
			$profit = $data->investasi * ($jumlahProfit / 100);
			
			if($profit < $withdrawal )
			{
				$profit = 0;
			}
			else
			{
				$profit = $profit - $withdrawal;
			}
			
			//total profit yang didapat hari itu dan dikurangi dengan profit yang tersedia karena telah di withdraw
			$profit = $data->investasi + $profit;
			//$profit = $data->investasi + ($data->profit);
			
		}
		else
		{
			$profit = $data->investasi + ($data->profit);
		}
		
		return $profit;
	}
	
	//from random persen profit / persen profit value
	function profitInvest($investasi,$persenProfit)
	{
		if(strlen($persenProfit) > 11)
		{
			$persenProfit = explode(";",$persenProfit);
			$profit = array_sum($persenProfit);
		}
		else
		{
			$profit = $persenProfit;
		}
		
		$profitInvest = $investasi * ($profit / 100);
		return $profitInvest;
	}
	
/* ================================================================================================================ */
	
	function RegisterToUser($data,$groupId="2")
	{
		$db =& JFactory::getDBO();
		$data['name']	= $data['nama'];
		$data['password']	= "invest123456";
		
		$sql = 'INSERT INTO #__users(name,username,email,password) values ("'.$data['name'].'","'.$data['email'].'","'.$data['email'].'","'.md5($data['password']).'")';
		$db->setQuery($sql);
		$db->query();
			
		// get user id
		$getUser = $this->getDataByParam("email","='".$data['email']."'","#__users");
		$data['user_id'] = $getUser->id;
		
		//insert user_group
		$sql = "INSERT INTO #__user_usergroup_map (`user_id`, `group_id`) VALUES ('".$data['user_id']."','".$groupId."')";
		$db->setQuery($sql);
		$RESql = $db->query();
		
		return $data['user_id'];
	}
	
	//update status transaction
	function updateStatusTransaction($status,$cid)
	{
		$db =& JFactory::getDBO();
		$cid = implode(',', $cid);		
		
		$q = 'UPDATE #__invest_transaction SET status="'.$status.'" WHERE id IN ('.$cid.') && status != "Success"';
		$db->setQuery( $q );
	
		if (!$db->query()) {
			$text = "Maaf terjadi kesalahan pada sistem";
			$mode = 'error';		
		}
		else
		{
			$text = "Berhasil Mengupdate status";
		}
		
		$data = array();
		$data['text'] = $text;
		$data['mode'] = $mode;
		
		return $data;
	}
	
}

?>