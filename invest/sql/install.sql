-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jun 08, 2012 at 09:26 PM
-- Server version: 5.0.51
-- PHP Version: 5.4.0

-- 
-- Database: `nagadana_2012`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `#__invest_bank`
-- 

CREATE TABLE `#__invest_bank` (
  `id` int(11) NOT NULL auto_increment,
  `bank` varchar(50) NOT NULL,
  `rekening` int(11) NOT NULL,
  `an` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `#__invest_bank`
-- 

INSERT INTO `#__invest_bank` VALUES (1, 'BCA', 1231321321, 'Naga Dana');
INSERT INTO `#__invest_bank` VALUES (3, 'DANAMON', 2147483647, 'Lorem Ipsum Dolor');
INSERT INTO `#__invest_bank` VALUES (5, 'BNI', 2147483647, 'Lorem Ipsum Dolor');

-- --------------------------------------------------------

-- 
-- Table structure for table `#__invest_config`
-- 

CREATE TABLE `#__invest_config` (
  `id` int(11) NOT NULL auto_increment,
  `config` varchar(50) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `category` varchar(50) NOT NULL,
  `order` int(2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

-- 
-- Dumping data for table `#__invest_config`
-- 

INSERT INTO `#__invest_config` VALUES (1, 'registerMemberInfo', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa.\r\n\r\nSed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh. Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris.\r\n\r\nMaecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.', 'Informasi yang akan ditampilkan pada register member', 'Info', 0);
INSERT INTO `#__invest_config` VALUES (2, 'emailAdmin', 'temals_mulyadi@yahoo.com', 'email yang digunakan untuk pemberitahuan aktifitas member, atau sebagai pengirim email pada saat mengirim email ke member', 'investConfig', 0);
INSERT INTO `#__invest_config` VALUES (3, 'sendEmail', 'true', 'jika true, member maupun admin akan mendapatkan email dari setiap aktifitas transaksi', 'investConfig', 0);
INSERT INTO `#__invest_config` VALUES (4, 'successDeposit', 'Saudara [namaMember], Deposit anda telah kami terima sebesar [nominalRupiah] dan akan segera kami tambahkan sebesar [nominalDollar] pada akun [exchangeMember]. terima kasih', 'Pesan yang dikirmkan ke email member saat status deposit telah di update menjadi success', 'investConfig', 0);
INSERT INTO `#__invest_config` VALUES (5, 'successWithdraw', 'Saudara [namaMember], Withdraw anda telah kami proses sebesar [nominalDollar] dan akan segera kami kirimkan sebesar [nominalRupiah] pada rekening [rekening] anda. terima kasih', 'Pesan yang dikirmkan ke email member saat status withdraw telah di update menjadi success', 'investConfig', 0);
INSERT INTO `#__invest_config` VALUES (6, 'rejectDeposit', 'Saudara [namaMember], Deposit anda tidak dapat kami proses, silahkan anda coba kembali, terima kasih', 'pesan yang akan dikirimkan ke email member bersangkutan saat status depositnya di reject', 'investConfig', 0);
INSERT INTO `#__invest_config` VALUES (7, 'rejectWithdraw', 'Saudara [namaMember], Withdraw anda tidak dapat kami proses, silahkan anda coba kembali, terima kasih', 'pesan yang akan dikirimkan ke email member bersangkutan saat status withdrawnya di reject', 'investConfig', 0);
INSERT INTO `#__invest_config` VALUES (8, 'uploadDir', 'administrator/components/com_invest/assets/images/legalitas/', 'Direktori upload file', 'global Configuration', 0);
INSERT INTO `#__invest_config` VALUES (9, 'maxSizeUpload', '500000', 'maksimal file upload', 'global Configuration', 0);
INSERT INTO `#__invest_config` VALUES (10, 'registerMember', 'Dear [email],\r\n\r\nAnda telah berhasil menjadi member Naga Dana, berikut detail keanggotaan anda :\r\n\r\nusername : [email]\r\npassword : [password]\r\n\r\nkami akan memberikan informasi jika admin telah mengaktifkan keanggotaan anda \r\n\r\n\r\nTerima kasih\r\n\r\nNaga Dana', 'email notifikasi saat member terdaftar pada website', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (11, 'addedPlan', 'Dear [nama],\r\n\r\nkami telah menerima permintaan anda untuk melakukan penambahkan plan investasi kedalam account anda dengan detail sebagai berikut :\r\n\r\nPlan : [plan]\r\nAccount / Nama Plan Anda : [memberplan]\r\nBesar Investasi : [memberinvestasi]\r\n\r\nBerikut adalah syarat dan ketentuan yang mungkin dapat anda pelajari :\r\n[term]\r\n\r\nlakukan pembayaran untuk pembelian plan anda tersebut sebesar [memberinvestasi] pada salah satu rekening kami\r\n\r\n[rekening]\r\n\r\n\r\nLalu, untuk mengaktifkan plan anda tersebut diatas kirimkan konfirmasi pembayaran setelah anda melakukan pembayaran pada form transaction di website kami.\r\n\r\n\r\n\r\nTerima kasih\r\n\r\nNagadana', 'email notifikasi saat member menambahkan plan', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (12, 'activedPlan', 'Dear [nama],\r\n\r\n', 'email notifikasi saat plan investasi member telah di aktifkan', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (13, 'completedPlan', 'plan anda telah mencapai batasan waktu yang ditetapkan, dengan begitu anda dapat menarik investasi dan profit anda', 'email notifikasi saat plan investasi member telah di aktifkan', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (14, 'activedMember', 'selamat keanggotaan anda telah diaktifkan, sekarang anda dapat melakukan aktifitas pada naga dana invest ini', 'email notifikasi saat keanggotaan member telah diaktifkan', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (15, 'closedPlan', 'Plan investasi anda telah di tutup, dan profit telah / akan segera kami kirimkan pada rekening anda terima kasih', 'email notifikasi saat plan investasi telah selesai dan profit telah dikirim', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (16, 'memberConfirmInvest', 'Member melakukan konfirmasi untuk pembayaran investasi plannya silakan di lakukan pengecekan pada bagian administrator website', 'email notifikasi kepada admin jika member telah melakukan konfirmasi untuk investasi plannya', 'admin notification', 0);
INSERT INTO `#__invest_config` VALUES (17, 'memberAddedPlan', 'Dear Admin,\r\n\r\nmember dengan email [email] melakukan permintaan penambahan plan investasi untuk accountnya dengan detail :\r\n\r\nPlan : [plan]\r\nAccount / Nama Plan : [memberplan]\r\nInvestasi : [memberinvestasi]\r\n\r\nterima kasih', 'email notifikasi kepada admin jika member melakukan penambahan / order plan investasi', 'admin notification', 0);
INSERT INTO `#__invest_config` VALUES (18, 'memberRegister', 'Dear Admin,\r\n\r\nSeseorang dengan email [email] telah melakukan pendaftaran pada website, silakan untuk mengaktifkan keanggotaan pada halaman admin website\r\n\r\nterima kasih', 'email notifikasi saat terdapat member baru di website', 'admin notification', 0);
INSERT INTO `#__invest_config` VALUES (19, 'memberWithdrawInvest', 'member ini telah meminta untuk penarikan investasinya terima kasih', 'email notifikasi saat member melakukan withdraw', 'admin notification', 0);
INSERT INTO `#__invest_config` VALUES (20, 'senderMail', 'info@kaiogroup.com', 'email yang digunakan sebagai pengirim notifikasi ke member', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (21, 'senderName', 'Naga Dana Invest', 'nama yang digunakan sebagai pengirim notifikasi ke member', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (22, 'adminMail', 'info@kaiogroup.com', 'email yang menjadi penerima notifikasi saat terjadi aktifitas member', 'admin notification', 0);
INSERT INTO `#__invest_config` VALUES (23, 'withdrawInvest', 'Saudara, anda telah melakukan permintaan withdraw. kami akan segera memproses withdraw anda', 'email notifikasi kepada member saat member melakukan permintaan withdraw', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (24, 'confirmInvest', 'Saudara, kami telah menerima konfirmasi pembayaran untuk plan investasi anda, dengan segera kami akan mengaktifkan plan investasi anda tersebut terima kasih', 'email notifikasi kepada member saat member melakukan permintaan withdraw', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (25, 'memberMailTemplate', '<table border="0">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p><img src="http://192.168.1.101/project/nagadana/images/chatwithus.png" border="0" alt="" /></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>[textEmail]<img src="http://192.168.1.101/project/nagadana/images/tentang-nagadana.png" border="0" alt="" /></td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Design Templete Mail Member, gunakan tag [textEmail] untuk meletakkan pesan atau email anda dalam template', 'member notification', 0);
INSERT INTO `#__invest_config` VALUES (26, 'adminMailTemplate', '<table border="0">\r\n<tbody>\r\n<tr>\r\n<td colspan="2"><img src="images/contact-us.png" border="0" alt="" width="380" height="130" /></td>\r\n</tr>\r\n<tr>\r\n<td>Hello...</td>\r\n<td>[textEmail]</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Design Template Mail Admin, gunakan tag [textEmail] untuk meletakkan pesan atau email anda dalam template', 'admin notification', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `#__invest_member`
-- 

CREATE TABLE `#__invest_member` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `identitas` varchar(50) NOT NULL,
  `no_identitas` varchar(50) NOT NULL,
  `type` enum('Personal','Lembaga') NOT NULL,
  `penanggung_jawab` varchar(50) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(225) NOT NULL,
  `provinsi` varchar(225) NOT NULL,
  `negara` varchar(100) NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `verified` tinyint(4) NOT NULL,
  `status` enum('InActive','Active') NOT NULL,
  `balance` int(11) NOT NULL,
  `legalitas` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `#__invest_member_bank`
-- 

CREATE TABLE `#__invest_member_bank` (
  `id` int(11) NOT NULL auto_increment,
  `member_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `rekening` int(11) NOT NULL,
  `an` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `#__invest_member_plan`
-- 

CREATE TABLE `#__invest_member_plan` (
  `id` int(11) NOT NULL auto_increment,
  `member_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `account` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `investasi` int(11) NOT NULL,
  `profit` int(11) NOT NULL,
  `persen_profit` text NOT NULL,
  `range_date` text NOT NULL,
  `date` datetime NOT NULL,
  `status` enum('InActive','Confirm','Active','Withdraw','Complete','Closed') NOT NULL default 'InActive',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `#__invest_plan`
-- 

CREATE TABLE `#__invest_plan` (
  `id` int(11) NOT NULL auto_increment,
  `plan` varchar(50) NOT NULL,
  `investasi` int(11) NOT NULL,
  `profit` int(11) NOT NULL,
  `waktu` int(11) NOT NULL,
  `penalty` int(11) NOT NULL,
  `description` text NOT NULL,
  `term` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `#__invest_plan`
-- 

INSERT INTO `#__invest_plan` VALUES (1, 'Bronze', 100000, 2, 20, 2, '<p>DESCRIPTION : Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh.</p>\r\n<p>Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.</p>', '<p>SYARAT DAN KETENTUAN : Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh.</p>\r\n<p>Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.</p>');
INSERT INTO `#__invest_plan` VALUES (2, 'Silver', 300000, 4, 40, 4, '<p>Description :</p>\r\n<p>Mauris vel lacus vitae felis vestibulum volutpat. Etiam est nunc, venenatis in, tristique eu, imperdiet ac, nisl. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In iaculis facilisis massa. Etiam eu urna. Sed porta. Suspendisse quam leo, molestie sed, luctus quis, feugiat in, pede. Fusce tellus. Sed metus augue, convallis et, vehicula ut, pulvinar eu, ante. Integer orci tellus, tristique vitae, consequat nec, porta vel, lectus. Nulla sit amet diam. Duis non nunc. Nulla rhoncus dictum metus. Curabitur tristique mi condimentum orci. Phasellus pellentesque aliquam enim. Proin dui lectus, cursus eu, mattis laoreet, viverra sit amet, quam. Curabitur vel dolor ultrices ipsum dictum tristique. Praesent vitae lacus. Ut velit enim, vestibulum non, fermentum nec, hendrerit quis, leo. Pellentesque rutrum malesuada neque.</p>\r\n<p>Nunc tempus felis vitae urna. Vivamus porttitor, neque at volutpat rutrum, purus nisi eleifend libero, a tempus libero lectus feugiat felis. Morbi diam mauris, viverra in, gravida eu, mattis in, ante. Morbi eget arcu. Morbi porta, libero id ullamcorper nonummy, nibh ligula pulvinar metus, eget consectetuer augue nisi quis lacus. Ut ac mi quis lacus mollis aliquam. Curabitur iaculis tempus eros. Curabitur vel mi sit amet magna malesuada ultrices. Ut nisi erat, fermentum vel, congue id, euismod in, elit. Fusce ultricies, orci ac feugiat suscipit, leo massa sodales velit, et scelerisque mi tortor at ipsum. Proin orci odio, commodo ac, gravida non, tristique vel, tellus. Pellentesque nibh libero, ultricies eu, sagittis non, mollis sed, justo. Praesent metus ipsum, pulvinar pulvinar, porta id, fringilla at, est.</p>', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit placerat molestie. Donec dictum lectus non odio. Cras a ante vitae enim iaculis aliquam. Mauris nunc quam, venenatis nec, euismod sit amet, egestas placerat, est.</p>\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras id elit. Integer quis urna. Ut ante enim, dapibus malesuada, fringilla eu, condimentum quis, tellus. Aenean porttitor eros vel dolor. Donec convallis pede venenatis nibh. Duis quam. Nam eget lacus. Aliquam erat volutpat. Quisque dignissim congue leo.</p>');
INSERT INTO `#__invest_plan` VALUES (3, 'Gold', 500000, 6, 60, 6, '<p>Description : <br />Phasellus felis dolor, scelerisque a, tempus eget, lobortis id, libero. Donec scelerisque leo ac risus. Praesent sit amet est. In dictum, dolor eu dictum porttitor, enim felis viverra mi, eget luctus massa purus quis odio. Etiam nulla massa, pharetra facilisis, volutpat in, imperdiet sit amet, sem. Aliquam nec erat at purus cursus interdum. Vestibulum ligula augue, bibendum accumsan, vestibulum ut, commodo a, mi. Morbi ornare gravida elit. Integer congue, augue et malesuada iaculis, ipsum dui aliquet felis, at cursus magna nisl nec elit. Donec iaculis diam a nisi accumsan viverra. Duis sed tellus et tortor vestibulum gravida. Praesent elementum elit at tellus. Curabitur metus ipsum, luctus eu, malesuada ut, tincidunt sed, diam. Donec quis mi sed magna hendrerit accumsan. Suspendisse risus nibh, ultricies eu, volutpat non, condimentum hendrerit, augue. Etiam eleifend, metus vitae adipiscing semper, mauris ipsum iaculis elit, congue gravida elit mi egestas orci. Curabitur pede.</p>\r\n<p>Maecenas aliquet velit vel turpis. Mauris neque metus, malesuada nec, ultricies sit amet, porttitor mattis, enim. In massa libero, interdum nec, interdum vel, blandit sed, nulla. In ullamcorper, est eget tempor cursus, neque mi consectetuer mi, a ultricies massa est sed nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Proin nulla arcu, nonummy luctus, dictum eget, fermentum et, lorem. Nunc porta convallis pede.</p>', '<p>Mauris vel lacus vitae felis vestibulum volutpat. Etiam est nunc, venenatis in, tristique eu, imperdiet ac, nisl. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In iaculis facilisis massa. Etiam eu urna. Sed porta. Suspendisse quam leo, molestie sed, luctus quis, feugiat in, pede. Fusce tellus. Sed metus augue, convallis et, vehicula ut, pulvinar eu, ante. Integer orci tellus, tristique vitae, consequat nec, porta vel, lectus.</p>\r\n<p>Nulla sit amet diam. Duis non nunc. Nulla rhoncus dictum metus. Curabitur tristique mi condimentum orci. Phasellus pellentesque aliquam enim. Proin dui lectus, cursus eu, mattis laoreet, viverra sit amet, quam. Curabitur vel dolor ultrices ipsum dictum tristique. Praesent vitae lacus. Ut velit enim, vestibulum non, fermentum nec, hendrerit quis, leo. Pellentesque rutrum malesuada neque.</p>');
INSERT INTO `#__invest_plan` VALUES (4, 'Platinum', 1000000, 10, 240, 10, '<h2 align="left">Description </h2>\r\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh. Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.</p>\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit placerat molestie. Donec dictum lectus non odio. Cras a ante vitae enim iaculis aliquam. Mauris nunc quam, venenatis nec, euismod sit amet, egestas placerat, est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras id elit. Integer quis urna. Ut ante enim, dapibus malesuada, fringilla eu, condimentum quis, tellus. Aenean porttitor eros vel dolor. Donec convallis pede venenatis nibh. Duis quam. Nam eget lacus. Aliquam erat volutpat. Quisque dignissim congue leo.</p>', '<h2 align="left">Syarat Dan Ketentuan</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh. Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.</p>\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit placerat molestie. Donec dictum lectus non odio. Cras a ante vitae enim iaculis aliquam. Mauris nunc quam, venenatis nec, euismod sit amet, egestas placerat, est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras id elit. Integer quis urna. Ut ante enim, dapibus malesuada, fringilla eu, condimentum quis, tellus. Aenean porttitor eros vel dolor. Donec convallis pede venenatis nibh. Duis quam. Nam eget lacus. Aliquam erat volutpat. Quisque dignissim congue leo.</p>');

-- --------------------------------------------------------

-- 
-- Table structure for table `#__invest_transaction`
-- 

CREATE TABLE `#__invest_transaction` (
  `id` int(11) NOT NULL auto_increment,
  `member_id` int(11) NOT NULL,
  `member_plan_id` int(11) NOT NULL,
  `type` enum('Deposit','Withdraw Profit','Withdraw Invest') NOT NULL,
  `tiket` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `member_bank_id` int(11) NOT NULL,
  `status` enum('Pending','Success','Reject') NOT NULL default 'Pending',
  `date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;