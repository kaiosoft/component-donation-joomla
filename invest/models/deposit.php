<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class InvestModelDeposit extends JModel{

	function getAllDeposit($filter, $pageNav){
		$sql = "SELECT a.*,b.bank,b.rekening,b.an,c.account,d.plan,e.nama FROM #__invest_transaction AS a 
		LEFT JOIN #__invest_bank AS b ON (b.id=a.bank_id) 
		LEFT JOIN #__invest_member_plan AS c ON (c.id=a.member_plan_id)
		LEFT JOIN #__invest_plan AS d ON (d.id = c.plan_id)
		LEFT JOIN #__invest_member AS e ON (e.id=c.member_id) WHERE a.type='Deposit'".$filter;
		$this->_db->setQuery($sql, $pageNav->limitstart, $pageNav->limit);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getDepositById($id)
	{
		$sql = "SELECT * FROM #__invest_deposit WHERE id = '".$id."'";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getTotal($filter)
	{
		$sql = "SELECT COUNT(*) FROM #__invest_transaction AS a 
		LEFT JOIN #__invest_bank AS b ON (b.id=a.bank_id) 
		LEFT JOIN #__invest_member_plan AS c ON (c.id=a.member_plan_id)
		LEFT JOIN #__invest_plan AS d ON (d.id = c.plan_id)
		LEFT JOIN #__invest_member AS e ON (e.id=c.member_id) WHERE a.type='Deposit'".$filter;
		$this->_db->setQuery( $sql );
		$total = $this->_db->loadResult();
		return $total;
	}
	
	
}

?>