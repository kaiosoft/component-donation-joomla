<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class InvestModelWithdraw extends JModel{

	function getAllWithdraw($filter, $pageNav){
		$sql = "SELECT a.*,c.bank,b.rekening,b.an,d.account,e.plan,f.nama FROM #__invest_transaction AS a 
		LEFT JOIN #__invest_member_bank AS b ON (b.id=a.member_bank_id) 
		LEFT JOIN #__invest_bank AS c ON (c.id=b.bank_id)
		LEFT JOIN #__invest_member_plan AS d ON (d.id=a.member_plan_id)
		LEFT JOIN #__invest_plan AS e ON (e.id = d.plan_id)
		LEFT JOIN #__invest_member AS f ON (f.id=d.member_id) WHERE a.type!='Deposit'".$filter;
		$this->_db->setQuery($sql, $pageNav->limitstart, $pageNav->limit);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getWithdrawById($id)
	{
		$sql = "SELECT * FROM #__invest_withdraw WHERE id = '".$id."'";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getTotal($filter)
	{
		$query = "SELECT COUNT(*) FROM #__invest_transaction AS a 
		LEFT JOIN #__invest_member_bank AS b ON (b.id=a.member_bank_id) 
		LEFT JOIN #__invest_bank AS c ON (c.id=b.bank_id)
		LEFT JOIN #__invest_member_plan AS d ON (d.id=a.member_plan_id)
		LEFT JOIN #__invest_plan AS e ON (e.id = d.plan_id)
		LEFT JOIN #__invest_member AS f ON (f.id=d.member_id) WHERE a.type!='Deposit'".$filter;
		$this->_db->setQuery( $query );
		$total = $this->_db->loadResult();
		return $total;
	}
	
	
}

?>