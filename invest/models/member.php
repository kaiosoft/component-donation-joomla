<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class InvestModelMember extends JModel{

	function getAllMember($filter, $pageNav){
		$sql = "SELECT a.*,b.username,b.email FROM #__invest_member AS a LEFT JOIN #__users AS b ON (b.id=a.user_id) ".$filter;
		$this->_db->setQuery($sql, $pageNav->limitstart, $pageNav->limit);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getMemberById($id)
	{
		$sql = "SELECT * FROM #__invest_member WHERE id = '".$id."'";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getTotal($filter)
	{
		$query = "SELECT COUNT(*) FROM #__invest_member ".$filter;
		$this->_db->setQuery( $query );
		$total = $this->_db->loadResult();
		return $total;
	}
	
	function getListMember()
	{
		$query = "SELECT id,nama FROM #__invest_member";
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;	
	}
	
	
}

?>