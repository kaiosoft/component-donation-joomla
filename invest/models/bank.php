<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class InvestModelBank extends JModel{

	function getAllBank($filter, $pageNav){
		$sql = "SELECT * FROM #__invest_bank ".$filter;
		$this->_db->setQuery($sql, $pageNav->limitstart, $pageNav->limit);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getBankById($id)
	{
		$sql = "SELECT * FROM #__invest_bank WHERE id = '".$id."'";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getTotal($filter)
	{
		$query = "SELECT COUNT(*) FROM #__invest_bank ".$filter;
		$this->_db->setQuery( $query );
		$total = $this->_db->loadResult();
		return $total;
	}
	
	function getListBank()
	{
		$query = "SELECT * FROM #__invest_bank";
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;	
	}
	
	
}

?>