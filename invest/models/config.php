<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class InvestModelConfig extends JModel{

	function getConfig(){
		$sql = "SELECT * FROM #__invest_config ";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getConfigCategory(){
		$sql = "SELECT DISTINCT category FROM #__invest_config ";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getConfigByCategory($cat){
		$sql = "SELECT * FROM #__invest_config WHERE category='".$cat."' ORDER BY `order` ASC";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
}

?>