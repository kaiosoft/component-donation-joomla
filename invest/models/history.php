<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */
 
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class InvestModelHistory extends JModel{

	function getAllHistory($filter, $pageNav){
		$sql = "SELECT a.*,b.bank,b.rekening,b.an,c.rekening AS rekening_member,c.an AS an_member,d.account,e.exchange,f.nama,g.bank AS bank_member FROM #__invest_transaction AS a 
		LEFT JOIN #__invest_bank AS b ON (b.id=a.bank_id) 
		LEFT JOIN #__invest_member_bank AS c ON (c.id=a.member_bank_id)
		LEFT JOIN #__invest_member_exchange AS d ON (d.id=a.member_exchange_id)
		LEFT JOIN #__invest_exchange AS e ON (e.id = d.exchange_id)
		LEFT JOIN #__invest_member AS f ON (f.id=d.member_id)
		LEFT JOIN #__invest_bank AS g ON (g.id=c.bank_id) ".$filter;
		$this->_db->setQuery($sql, $pageNav->limitstart, $pageNav->limit);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getHistoryById($id)
	{
		$sql = "SELECT * FROM #__invest_history WHERE id = '".$id."'";
		$this->_db->setQuery($sql);
		$rows = $this->_db->loadObjectList();
		return $rows;
	}
	
	function getTotal($filter)
	{
		$query = "SELECT COUNT(*) FROM #__invest_history ".$filter;
		$this->_db->setQuery( $query );
		$total = $this->_db->loadResult();
		return $total;
	}
	
}

?>