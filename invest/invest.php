 <?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_invest')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

//include stylesheet
$doc =& JFactory::getDocument();
$doc->addStyleSheet('components/com_invest/assets/stylesheet/style.css');


// Include dependancies
jimport('joomla.application.component.controller');
require_once(JPATH_COMPONENT.DS.'controller.php');

// Set the table directory
JTable::addIncludePath(JPATH_COMPONENT.DS.'tables');

// include Helper
require_once(JPATH_COMPONENT.DS.'helpers'.DS.'function.php');
require_once(JPATH_COMPONENT.DS.'helpers'.DS.'notification.php');
?><script src="components/com_invest/assets/js/jquery.js"></script><?php
$controller = JRequest::getWord('c');

// Require specific controller if requested
if($controller==''){
	$controller = "Dashboard";
}

$controller_path = strtolower($controller);
$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller_path.'.php';
require_once $path;

// Create the controller
$classname    = 'InvestController'.ucfirst($controller);
$controller   = new $classname( );

$controller->execute(JRequest::getCmd('task'));
$controller->redirect();

?>