<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

class InvestControllerWithdraw extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		$task = JRequest::getVar('task');
		JRequest::setVar('task',$task);
		
		switch($task){
			case "withdraw.save":
			$this->save();
			break;
			
			case "withdraw.cancel":
			$this->cancel();
			break;
			
			case "withdraw.delete":
			$this->remove();
			break;
			
			default:
			JRequest::setVar('layout','default');
			break;
			
		}
		parent::__construct( $config );
	}

	/**
	 * Display the list of withdraw
	 */
	function display()
	{
		JRequest::setVar('view', 'withdraw' );	
		parent::display(); 
	}
	
	function save()
	{ 
		// Check for request forgeries
		$app=& JFactory::getApplication();
		$investHelper = new investHelper;
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$row =& JTable::getInstance('withdraw', 'Table');
		$post = JRequest::get('post');
		$id	= JRequest::getVar( 'id', array(0), 'post', 'array' );		
		$post['id'] = (int) $id[0];
		
		$axistWithdraw = $investHelper->getWithdrawByParam('withdraw',$post['withdraw']);
		
		if((!empty($axistWithdraw)) && ($axistWithdraw->id != $post['id']))
		{
			echo "<script>
				alert('withdraw tersebut telah ditambahkan sebelumnya');
				history.back(-1);
			</script>";
			exit();
		}
		
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		
		if ($row->store($post)) {
			$msg = JText::_( 'Data withdraw Telah Di Simpan' );
		} else {
			$msg = JText::_( 'Ada Kesalahan Dalam Menyimpan Data' );
		} 
		
		// Check the table in so it can be edited.... we are done with it anyway
		$row->checkin();
		$link = 'index.php?option=com_invest&c=withdraw';
		$app->redirect($link, $msg); 
		
	}

	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_invest&c=withdraw', $msg );
	}

	function remove()
	{
		global $mainframe;
		
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		// Initialize variables
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), 'method', 'array' );
		$n		= count( $cid ); 
		 
		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select a section to delete', true ) );
		}
		
		JArrayHelper::toInteger( $cid );

		$cid = implode(',', $cid);		
		$q = 'DELETE FROM #__invest_withdraw WHERE id IN ('.$cid.')';
		$db->setQuery( $q );
	
		if (!$db->query()) {
			JError::raiseWarning( 500, $db->getError() );
		}

		$text = "withdraw telah di hapus";
		$this->setMessage( JText::sprintf( $text, $n ) ); 
		$this->setRedirect( 'index.php?option=com_invest&c=withdraw' ); 
		
	}
		
}

?>