<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

class InvestControllerPlan extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		$task = JRequest::getVar('task');
		JRequest::setVar('task',$task);
		
		switch($task){
			case "plan.add":
			case "plan.edit":
			JRequest::setVar('layout','add');
			break;
			
			case "plan.save":
			$this->save();
			break;
			
			case "plan.cancel":
			$this->cancel();
			break;
			
			case "plan.delete":
			$this->remove();
			break;
			
			default:
			JRequest::setVar('layout','default');
			break;
			
		}
		parent::__construct( $config );
	}

	/**
	 * Display the list of plan
	 */
	function display()
	{
		JRequest::setVar('view', 'plan' );	
		parent::display(); 
	}
	
	function save()
	{ 
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$app =& JFactory::getApplication();

		$investHelper = new investHelper;
		$row =& JTable::getInstance('plan', 'Table');
		$post = JRequest::get('post');
		$data['id'] = $post['id'];
		$data['plan'] = $post['main']['plan'];
		$data['investasi'] = $post['main']['investasi'];
		$data['waktu'] = $post['main']['waktu'];
		$data['description'] = $post['main']['description'];
		$data['term'] = $post['main']['term'];
		$data['profit'] = $post['extra']['profit'];
		$data['penalty'] = $post['extra']['penalty'];
		
		$axistPlan = $investHelper->getDataByParam('plan',"='".$data['plan']."'","#__invest_plan");
		
		if($axistPlan->id != $data['id'])
		{
			echo "<script>
				alert('plan tersebut telah ditambahkan sebelumnya');
				history.back(-1);
			</script>";
			exit();
		}
		
		if (!$row->bind( $data )) {
			JError::raiseError(500, $row->getError() );
		}
		
		if ($row->store($data)) {
			$msg = JText::_( 'Data plan Telah Di Simpan' );
		} else {
			$msg = JText::_( 'Ada Kesalahan Dalam Menyimpan Data' );
		} 
		
		// Check the table in so it can be edited.... we are done with it anyway
		$row->checkin();
		$link = 'index.php?option=com_invest&c=plan';
		$app->redirect($link, $msg); 
		
	}

	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_invest&c=plan', $msg );
	}

	function remove()
	{
		global $mainframe;
		
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );
		
		// Initialize variables
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), 'method', 'array' );
		$n		= count( $cid ); 
		 
		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select a section to delete', true ) );
		}
		
		JArrayHelper::toInteger( $cid );

		$cid = implode(',', $cid);		
		$q = 'DELETE FROM #__invest_plan WHERE id IN ('.$cid.')';
		$db->setQuery( $q );
	
		if (!$db->query()) {
			JError::raiseWarning( 500, $db->getError() );
		}

		$text = "plan telah di hapus";
		$this->setMessage( JText::sprintf( $text, $n ) ); 
		$this->setRedirect( 'index.php?option=com_invest&c=plan' ); 
		
	}
		
}

?>