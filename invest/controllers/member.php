<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
//defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

class InvestControllerMember extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		parent::__construct( $config );
		
		
	}

	/**
	 * Display the list of Member
	 */
	function display()
	{
		
		JRequest::setVar('view', 'member' );	
		$task = JRequest::getVar('task');
		JRequest::setVar('task',$task);
		
		switch($task){
	
			case "member.add":
			case "member.edit":
			JRequest::setVar('layout','add');
			break;
			
			case "member.save":
			$this->save();
			break;
			
			case "member.cancel":
			$this->cancel();
			break;
			
			case "member.delete":
			$this->remove();
			break;
			
			case "forceDelete":
			$this->forceDelete();
			break;
			
			case "ajaxKab":
			$this->ajaxKab();
			break;
			
			case "removeExchange":
			$this->removeExchange();
			break;
			
			default:
			JRequest::setVar('layout','default');
			break;
			
		}
				
		parent::display(); 
	}
	
	/**
	 * Save method
	 */
	function save()
	{ 
		
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$app = JFactory::getApplication();
		$db	=& JFactory::getDBO();
		$row =& JTable::getInstance('member', 'Table');
		$post = JRequest::get('post');
		$data['id'] = $post['id'];
		$data['user_id'] = $post['user_id'];
		$data['email'] = $post['main']['email'];
		$data['nama'] = $post['main']['nama'];
		$data['identitas'] = $post['main']['identitas'];
		$data['no_identitas'] = $post['main']['no_identitas'];
		$data['alamat'] = $post['main']['alamat'];
		$data['kode_pos'] = $post['main']['kode_pos'];
		$data['type'] = $post['main']['type'];
		$data['penanggung_jawab'] = $post['main']['penanggung_jawab'];
		$data['hp'] = $post['main']['hp'];
		$data['status'] = $post['extra']['status'];
		$data['date'] = $post['date'];
		$data['legalitas'] = $post['legalitas'];
		$data['balance'] = $post['balance'];
		
		$investHelper = new investHelper;
		require_once('components/com_invest/helpers/regional.php');
		$regionalHelper = new regionalHelper;

		$regional = $regionalHelper->getRegional($post);
		
		$data['kota'] = $regional['kota'];
		$data['provinsi'] = $regional['provinsi'];
		$data['negara'] = $regional['negara'];
		
		if($data['id'] == 0)
		{
			//cecking user
			$isMember = $investHelper->getDataByParam("email","='".$data['email']."'","#__users");
					
			if(!empty($isMember->id))
			{
				echo "<script>alert('Maaf Username / Email sudah terdaftar');history.back(-1);</script>";
				exit();
			}
			
			//prepare data
			$post = array();
			$post['email'] = $data['email'];
			$post['username'] = $data['email'];
			$post['name'] = $data['nama'];
			
			//insert user and geting user_id
			$register = $investHelper->registerUser($post,2,"index.php?option=com_invest&c=member");
			$data['user_id'] = $register['user_id'];
			
			$data['date'] = date('Y-m-d H:i:s');
		}
				
			//update name to users	
		if($data['id'] != 0)
		{
			$updateName = "UPDATE #__users SET name='".$data['nama']."' WHERE id='".$data['user_id']."'";
			$db->setQuery($updateName);
			$db->query();
		}
		
		if (!$row->bind( $data )) {
			JError::raiseError(500, $row->getError() );
		}
				
		if ($row->store($data)) {
			$msg = JText::_( 'Berhasil Menyimpan Data' );
			
			$sql = "SELECT a.*,b.email FROM #__invest_member AS a LEFT JOIN #__users AS b ON (b.id=a.user_id) WHERE a.id='".$data['id']."'";
			$db->setQuery($sql);
			$getUser = $db->loadObject();
			
			
			
			if(($data['status'] == "Active") && (!empty($post['sendEmail'])))
			{
				//send mail to member
				$notifMember = $investHelper->getConfig('activedMember');
				$templateMember = $investHelper->getConfig('memberMailTemplate');
				$templateMember = str_replace("[textEmail]",$notifMember,$templateMember);
				$senderMail = $investHelper->getConfig('senderMail');
				$senderName = $investHelper->getConfig('senderName');
				$investHelper->sendMail($getUser->email,$senderMail,$senderName,"Pemberitahuan Pendaftaran Member",$templateMember);
			}
			
			if(($data['id'] == 0) && ($data['status'] == "InActive"))
			{
				//send mail to member
				$notifMember = $investHelper->getConfig('registerMember');
				$templateMember = $investHelper->getConfig('memberMailTemplate');
				$templateMember = str_replace("[textEmail]",$notifMember,$templateMember);
				$senderMail = $investHelper->getConfig('senderMail');
				$senderName = $investHelper->getConfig('senderName');
				$investHelper->sendMail($getUser->email,$senderMail,$senderName,"Pemberitahuan Pendaftaran Member",$templateMember);
					
				//send mail to admin
				$notifAdmin = $investHelper->getConfig('memberRegister');
				$adminMail = $investHelper->getConfig('adminMail');
				$investHelper->sendMail($adminMail,'info@kaiogroup.com','Administrator Website',"Pemberitahuan Pendaftaran Member",$notifAdmin);
			}
		}
		else
		{
			$msg = JText::_( 'Ada kesalahan dalam melakukan penyimpanan data' );
		} 
			
		$row->checkin();
		$this->setRedirect( 'index.php?option=com_invest&c=member',$msg."<br />".$textMsg );
				 
	}

	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_invest&c=member', $msg );
	}

	function remove()
	{
		global $mainframe;
		
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		// Initialize variables
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), 'method', 'array' );
		$n		= count( $cid ); 
		 
		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select a section to delete', true ) );
		}
		
		JArrayHelper::toInteger( $cid );

		$cid = implode(',', $cid);		
		
		$getUser = "SELECT a.id FROM #__users AS a LEFT JOIN #__invest_member AS b ON (b.user_id=a.id) WHERE b.id IN (".$cid.") && b.status!='Active'";
		$db->setQuery($getUser);
		$RgetUser = $db->loadObjectList();
		
		if(empty($RgetUser))
		{
			$exMsg = "Member berstatus aktif tidak dapat dihapus";
		}
		
		$deleteMember = "DELETE FROM #__invest_member WHERE id IN (".$cid.") && status!='Active'";
		$db->setQuery($deleteMember);
		$RdeleteMember = $db->query();
		
		if (!$RdeleteMember) {
			JError::raiseWarning( 500, $db->getError() );
		}
		
		$ids = array();
		foreach($RgetUser as $id)
		{
			$ids[] = $id->id;
			
		}
		
		$pilihan = implode(',',array_values($ids));
		$deleteUser = "DELETE FROM #__users WHERE id IN (".$pilihan.")";
		$db->setQuery( $deleteUser );
		$RdeleteUser = $db->query();
		
		$deleteUserMap = "DELETE FROM #__user_usergroup_map WHERE user_id IN (".$pilihan.")";
		$db->setQuery( $deleteUserMap );
		$RdeleteUserMap = $db->query();

		if(!empty($exMsg))
		{
			$text = $exMsg;
		}
		else
		{
			$text = "Member telah di hapus";
		}
		$this->setMessage( JText::sprintf( $text, $n ) ); 
		$this->setRedirect( 'index.php?option=com_invest&c=member' ); 
		
	}
	
	function forceDelete()
	{
		$id = JRequest::getVar('id');
		$db =& JFactory::getDBO();
		$app =& JFactory::getApplication();
		
		echo $sql = "DELETE FROM #__invest_member WHERE id='".$id."'";
		$db->setQuery($sql);
		if(!$db->query())
		{
			$msg = "Gagal menghapus data";
			$mode = "error";
		}
		else
		{
			$msg = "Data telah terhapus";
		}
		
		$app->redirect('index.php?option=com_invest&c=member',$msg,$mode);
	}
	
	public function ajaxKab()
	{

		require_once('components/com_ajib/helpers/function.php');
		$ajibHelper = new AjibHelper;
	  	$provinsi = JRequest::getVar('provinsi_id', '', 'ger', 'provinsi_id');
  	  	$kabupaten = $ajibHelper->getKabArray($provinsi);
		$kablist = $kabupaten;
		echo $lists['kab']	= JHTML::_('select.genericlist', $kablist, 'kab_id', 'onchange="" class="inputbox" size="1"', 'id', 'nama_kabupaten', $this->data['member'][0]->kab_id );
	}
	
	public function removeExchange()
	{ 
		$id = JRequest::getVar('id');
		echo $msg = "Berhasil menghapus data".$id;
	}
	
	function getRegional()
	{
		error_reporting(0);
		require_once('components/com_invest/helpers/regional.php');
		$helper = new regionalHelper;
		
		$regional = JRequest::setVar('regional');
		$country = JRequest::getVar('country');
		$state = JRequest::getVar('state');
		
		if($regional == "state")
		{
			$listState 		= $helper->getState($country);
			$jumlah = count($listState);
			if($jumlah > 1)
			{
				$statelist		= $listState;
				echo $lists['state'] = JHTML::_('select.genericlist', $statelist, 'state', 'class="inputbox" size="1"', 'value', 'text' );
			}
			else
			{
				echo "<input type='text' id='state' name='state' placeHolder='Masukkan Provinsi' />";
			}
		}
		else
		{
			$listCity 		= $helper->getCity($country,$state);
			$jumlah			= count($listCity);
			if($jumlah > 1)
			{
				$citylist		= $listCity;
				echo $lists['city'] 	= JHTML::_('select.genericlist', $citylist, 'city', 'class="inputbox" size="1"', 'value', 'text' );
			}
			else
			{
				echo "<input type='text' name='city' id='city' placeHolder='Masukkan Kota' />";
			}
		}
		break;
	}
		
}

?>