<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

class InvestControllerMemberplan extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		$task = JRequest::getVar('task');
		JRequest::setVar('task',$task);
		
		switch($task){
			case "plan.add":
			case "plan.edit":
			JRequest::setVar('layout','add');
			break;
			
			case "plan.save":
			$this->save();
			break;
			
			case "plan.cancel":
			$this->cancel();
			break;
			
			case "plan.delete":
			$this->remove();
			break;
			
			case "doTransaksi":
			$this->doTransaksi();
			break;
			
			default:
			JRequest::setVar('layout','default');
			break;
			
		}
		parent::__construct( $config );
	}

	/**
	 * Display the list of plan
	 */
	function display()
	{
		JRequest::setVar('view', 'memberplan' );	
		parent::display(); 
	}
	
	function save()
	{ 
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$app =& JFactory::getApplication();
		$investHelper = new investHelper;
		$db =& JFactory::getDBO();

		$investHelper = new investHelper;
		$row =& JTable::getInstance('memberplan', 'Table');
		$post = JRequest::get('post');
		$data['id'] = $post['id'];
		$data['plan_id'] = $post['plan_id'];
		$data['member_id'] = $post['member_id'];
		$data['nama'] = $post['main']['nama'];
		$data['investasi'] = $post['main']['investasi'];
		$data['status'] = $post['main']['status'];
		$data['persen_profit'] = $post['persen_profit'];
		$data['account'] = $post['account'];
		$data['date'] = $post['date'];
		$data['range_date'] = $post['range_date'];
		$data['profit'] = $post['profit'];
		
		
		$plan = $investHelper->getDataByParam("id","='".$data['plan_id']."'","#__invest_plan");
		
		if($data['id'] == 0)
		{
			$data['account'] = $plan->plan." - ".$data['member_id'].$data['plan_id'].rand(111,999);
			$data['date'] = date("Y-m-d h:i:s");
			$data['profit'] = $data['investasi']*($plan->profit/100);
		}
		
		if($data['persen_profit'] == "")
		{
			$persen_profit = $investHelper->getRandomize($plan->profit,$plan->waktu);
			$data['persen_profit'] = implode(";",$investHelper->randomizeArray($persen_profit));
		}
		
		if(($data['status'] == "Active") && ($data['range_date'] == ""))
		{
			//print_r($investHelper->getRangeDate($plan->waktu));
			$data['range_date'] = implode($investHelper->getRangeDate($plan->waktu),";");
		}
		
		if($data['investasi'] < $plan->investasi)
		{
			echo "<script>
				alert('Investasi untuk plan ini minimal ".$plan->investasi."');
				history.back(-1);
			</script>";
			exit();
		}
		
		$axistPlan = $investHelper->getDataByParam(array("plan_id","nama"),array("='".$data['plan_id']."'","='".$data['nama']."'"),"#__invest_member_plan");
		
		if((!empty($axistPlan)) && ($axistPlan->id != $data['id']))
		{
			$link = 'index.php?option=com_invest&c=memberplan';
			$msg = 'Tidak dapat menggunakan nama yang sama untuk setiap member dan plan yang sama';
			$app->redirect($link, $msg, "error"); 
			exit();
		}
		
		$activeMember = $investHelper->getDataByParam(array("id","status"),array("='".$data['member_id']."'","='Active'"),"#__invest_member");
		
		if(empty($activeMember->id))
		{
			$link = 'index.php?option=com_invest&c=memberplan';
			$msg = 'Maaf, anda tidak bisa menambahkan atau mengaktifkan plan untuk member ini sampai keanggotaannya ini diaktifkan';
			$app->redirect($link, $msg, "error"); 
			exit();
		}
		
		
		if (!$row->bind( $data )) {
			JError::raiseError(500, $row->getError() );
		}
		
		if ($row->store($data)) {
			$msg = JText::_( 'Data plan Telah Di Simpan' );
			
			$sql = "SELECT * FROM #__invest_member AS a LEFT JOIN #__users AS b ON (b.id=a.user_id) WHERE a.id='".$data['member_id']."'";
			$db->setQuery($sql);
			$getUser = $db->loadObject();
			
			if(($data['status'] == "Active") && (!empty($post['sendEmail'])))
			{
				//send mail to member
				$notifMember = $investHelper->getConfig('activedPlan');
				$templateMember = $helper->getConfig('memberMailTemplate');
				echo $templateMember = str_replace("[textEmail]",$notifMember,$templateMember);
				$senderMail = $investHelper->getConfig('senderMail');
				$senderName = $investHelper->getConfig('senderName');
				$investHelper->sendMail($getUser->email,$senderMail,$senderName,"Pemberitahuan Permintaan Penarikan Invesatasi",$templateMember);
			}
			
			if(($data['id'] == 0) && ($data['status'] == "InActive"))
			{
				//send mail to member
				$notifMember = $investHelper->getConfig('addedPlan');
				$templateMember = $helper->getConfig('memberMailTemplate');
				echo $templateMember = str_replace("[textEmail]",$notifMember,$templateMember);
				$senderMail = $investHelper->getConfig('senderMail');
				$senderName = $investHelper->getConfig('senderName');
				$investHelper->sendMail($getUser->email,$senderMail,$senderName,"Pemberitahuan Permintaan Penarikan Invesatasi",$templateMember);
					
				//send mail to admin
				$notifAdmin = $investHelper->getConfig('memberAddedPlan');
				$adminMail = $investHelper->getConfig('adminMail');
				$investHelper->sendMail($adminMail,'info@kaiogroup.com','Administrator Website',"Pemberitahuan Permintaan Penarikan Invesatasi",$notifAdmin);
			}
			
		} else {
			$msg = JText::_( 'Ada Kesalahan Dalam Menyimpan Data' );
		} 
		
		// Check the table in so it can be edited.... we are done with it anyway
		$row->checkin();
		$link = 'index.php?option=com_invest&c=memberplan';
		$app->redirect($link, $msg); 
		
	}

	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_invest&c=memberplan', $msg );
	}

	function remove()
	{
		global $mainframe;
		
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );
		
		// Initialize variables
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), 'method', 'array' );
		$n		= count( $cid ); 
		 
		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select a section to delete', true ) );
		}
		
		JArrayHelper::toInteger( $cid );

		$cid = implode(',', $cid);		
		$q = 'DELETE FROM #__invest_member_plan WHERE id IN ('.$cid.')';
		$db->setQuery( $q );
	
		if (!$db->query()) {
			JError::raiseWarning( 500, $db->getError() );
		}

		$text = "data telah di hapus";
		$this->setMessage( JText::sprintf( $text, $n ) ); 
		$this->setRedirect( 'index.php?option=com_invest&c=memberplan' ); 
		
	}
	
	function doTransaksi()
	{
		error_reporting(0);
		
		$helper = new investHelper;
		$db =& JFactory::getDBO();
		
		$id = $_POST['id'];
		$status = $_POST['status'];
		
		
		$transaksi = $helper->getDataByParam("id","='".$id."'","#__invest_transaction");
		
		$sql = "UPDATE #__invest_transaction SET status='".$status."' WHERE id='".$id."'";
		$db->setQuery($sql);
		if(!$db->query())
		{
			echo "Terjadi kesalahan dalam menyimpan data";
		}
		else
		{
			if($status == "Success")
			{
				$cekAktifMember = $helper->getDataByParam(array("id","status"),array("='".$member_id."'","='Active'"),"#__invest_member");
				if(!empty($cekAktifMember->id))
				{
					if($transaksi->type == "Withdraw Profit")
					{
						echo $sql = "UPDATE #__invest_member_plan SET profit=profit-'".$transaksi->nominal."',status='Active' WHERE id='".$transaksi->member_plan_id."'";
						$db->setQuery($sql);
						if(!$db->query())
						{
							echo "Terjadi kesalahan dalam melakukan update profit";
						}
					}
					else if($transaksi->type == "Withdraw Invest")
					{
						 $sql = "UPDATE #__invest_member_plan SET status='Closed' WHERE id='".$transaksi->member_plan_id."'";
						$db->setQuery($sql);
						if(!$db->query())
						{
							echo "Terjadi kesalahan dalam melakukan update profit";
						}
					}
					else
					{
						$sql = "UPDATE #__invest_member_plan SET status='Confirmed' WHERE id='".$transaksi->member_plan_id."'";
						$db->setQuery($sql);
						if(!$db->query())
						{
							echo "Terjadi kesalahan dalam melakukan update profit";
						}
					}
				}
				else
				{
					echo "Member tersebut belum aktif";
				}
			}
			else
			{
					if($transaksi->type == "Deposit")
					{
						$rejectStatus = "InActive";
					}
					else
					{
						$rejectStatus = "Active";
					}
					$sql = "UPDATE #__invest_member_plan SET status='".$rejectStatus."' WHERE id='".$transaksi->member_plan_id."'";
					$db->setQuery($sql);
					if(!$db->query())
					{
						echo "Terjadi kesalahan dalam melakukan update profit";
					}
			}
			
			echo "Berhasil Mengupdate Status";
			
		}

		break;
	}
		
}

?>