<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

class InvestControllerConfig extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		parent::__construct( $config );
	}

	/**
	 * Display the list of Token
	 */
	function display()
	{
		$task = JRequest::getVar('task');	
		JRequest::setVar('task',$task);	
		
		switch($task){
			case "config.edit":
			JRequest::setVar('layout','add');
			break;
			
			case "config.save":
			$this->save();
			break;
		
			default:
			JRequest::setVar('layout', 'default' );
			break;
		}
		
		JRequest::setVar('view', 'config' );	
		parent::display(); 
	}	
	
	function save()
	{ 
		$investHelper = new investHelper;
		$db =& JFactory::getDBO();
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$post = JRequest::get('post');
		
		foreach($post['value'] as $key=>$val)
		{
			$uriBase = JURI::Base();
			$uriBase = substr($uriBase,0,strpos($uriBase,"administrator"));
			$val = str_replace('<img src="','<img src="'.$uriBase,$val);
			echo $sql = "UPDATE #__invest_config SET value='".$val."' WHERE id='".$key."'";
			$db->setQuery($sql);
			if(!$db->query())
			{
				$msg = "".JText::_('COM_INVEST_NOTIFICATION_ERROR_SAVING_CONFIG')."";
				$mode = "error";
			}
			else
			{
				$msg = "".JText::_('COM_INVEST_NOTIFICATION_SUCCESS_SAVING_CONFIG')."";
			}
			
		}
		
		$msg = "Konfigurasi tersimpan";
		
		$link = 'index.php?option=com_invest&c=config';
		$this->setRedirect($link, $msg); 
		
	}

}

?>