<?php

 /*
 *	Copyright (C) Kaio Piranti Lunak
 *  copyright statements are left intact.
 *
 *	Developer : Fatah Iskandar Akbar 
 *  Email : info@kaiogroup.com
 *	Date: Jan 2012
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

class InvestControllerMemberbank extends JController
{
	/**
	 * Constructor
	 */
	function __construct( $config = array() )
	{
		$task = JRequest::getVar('task');
		JRequest::setVar('task',$task);
		
		switch($task){
			case "bank.add":
			case "bank.edit":
			JRequest::setVar('layout','add');
			break;
			
			case "bank.save":
			$this->save();
			break;
			
			case "bank.cancel":
			$this->cancel();
			break;
			
			case "bank.delete":
			$this->remove();
			break;
			
			default:
			JRequest::setVar('layout','default');
			break;
			
		}
		parent::__construct( $config );
	}

	/**
	 * Display the list of member bank
	 */
	function display()
	{
		JRequest::setVar('view', 'memberbank' );	
		parent::display(); 
	}
	
	function save()
	{ 
		// Check for request forgeries
		$app=& JFactory::getApplication();
		$investHelper = new investHelper;
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$row =& JTable::getInstance('memberbank', 'Table');
		$post = JRequest::get('post');
		$data['id'] = $post['id'];
		$data['bank_id'] = $post['bank_id'];
		$data['member_id'] = $post['member_id'];
		$data['rekening'] = $post['main']['rekening'];
		$data['an'] = $post['main']['an'];
		
		$axistBank = $investHelper->getDataByParam(array("bank_id","rekening"),array("='".$data['bank_id']."'","='".$data['rekening']."'"),"#__invest_member_bank");
		
		if((!empty($axistBank)) && ($axistBank->id != $data['id']))
		{
			echo "<script>
				alert('Bank tersebut telah ditambahkan sebelumnya');
				history.back(-1);
			</script>";
			exit();
		}
		
		if (!$row->bind( $data )) {
			JError::raiseError(500, $row->getError() );
		}
		
		if ($row->store($data)) {
			$msg = JText::_( 'Data bank Telah Di Simpan' );
		} else {
			$msg = JText::_( 'Ada Kesalahan Dalam Menyimpan Data' );
		} 
		
		// Check the table in so it can be edited.... we are done with it anyway
		$row->checkin();
		$link = 'index.php?option=com_invest&c=memberbank';
		$app->redirect($link, $msg); 
		
	}

	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_invest&c=memberbank', $msg );
	}

	function remove()
	{
		global $mainframe;
		
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		// Initialize variables
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), 'method', 'array' );
		$n		= count( $cid ); 
		 
		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select a section to delete', true ) );
		}
		
		JArrayHelper::toInteger( $cid );

		$cid = implode(',', $cid);		
		$q = 'DELETE FROM #__invest_member_bank WHERE id IN ('.$cid.')';
		$db->setQuery( $q );
	
		if (!$db->query()) {
			JError::raiseWarning( 500, $db->getError() );
		}

		$text = "bank telah di hapus";
		$this->setMessage( JText::sprintf( $text, $n ) ); 
		$this->setRedirect( 'index.php?option=com_invest&c=bank' ); 
		
	}
		
}

?>